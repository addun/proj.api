﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using justfinditApi.Core.ConstantValues;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.Server.Kestrel.Https.Internal;

namespace justfinditApi.Core.Domain
{
    public class Employee
    {
        public Guid EmployeeId { get; private set; }
        public User User { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string About { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Sex { get; private set; }
        public string Phone { get; private set; }
        public string Country { get; private set; }
        public string City { get; private set; }
        public string WebsiteUrl { get; private set; }
        
        public ICollection<ProfessionalExperience> ProfessionalExperience { get; private set; }     
        public ICollection<Education> Education { get; private set; }        
        public ICollection<Course> Courses { get; private set; }
        public ICollection<EmployeeLanguage> EmployeeLanguages { get; private set; }
        public ICollection<EmployeeSkill> EmployeeSkills { get; private set; }
        public ICollection<JobApplication> JobApplications { get; private set; }
        public ICollection<EmployeeFavouriteJobOffer> Favourites { get; private set; }

        protected Employee()
        {
            Courses = new List<Course>();
            Education = new List<Education>();
            EmployeeLanguages = new List<EmployeeLanguage>();
            EmployeeSkills = new List<EmployeeSkill>();
            ProfessionalExperience = new List<ProfessionalExperience>();
            JobApplications = new List<JobApplication>();
            Favourites = new List<EmployeeFavouriteJobOffer>();
            EmployeeId = CreateId();
        }
        
        public Employee(string name, string surname, string sex, string birthDate, string about,
            string phone, string websiteUrl, string country, string city, User user): this()
        {
            if (!IsCityValid(city))
                throw new ArgumentException("City is invalid");
            
            if (!IsCountryValid(country))
                throw new ArgumentException("Country is invalid");
            
            if (!IsNameValid(name))
                throw new ArgumentException("Name is invalid");
            
            if (!IsSexValid(sex))
                throw new ArgumentException("Sex is invalid");
            
            if (!IsSurnameValid(surname))
                throw new ArgumentException("Surname is invalid");
            
            if (!IsBirthDateValid(birthDate))
                throw new ArgumentException("Birthdate is invalid");
            
            if(user == null)
                throw new ArgumentException("User is null");
            
            Name = name;
            Surname = surname;
            About = about;
            Sex = sex;
            BirthDate = DateTime.Parse(birthDate);
            Country = country;
            City = city;
            Phone = phone;
            WebsiteUrl = websiteUrl; 
            User = user;
        }

        public void AddCourse(Course course)
        {
            if (course == null)
                return;
            
            Courses.Add(course);
        }
        
        public void AddEducation(Education education)
        {
            if (education == null)
                return;
            
            Education.Add(education);
        }
        
        public void AddJobOfferToFavourites(JobOffer jobOffer)
        {
            if (jobOffer == null)
                return;
            
            Favourites.Add(EmployeeFavouriteJobOffer.Create(this, jobOffer));
        }

        public void AddLanguage(Language language, int level)
        {
            if (language == null)
                return;

            if (EmployeeLanguages.Any(x => x.LanguageId == language.LanguageId))
                return;

            EmployeeLanguages.Add(Domain.EmployeeLanguage.Create(language, level));
        }

        public void AddProfessionalExperience(ProfessionalExperience professionalExperience)
        {
            if (professionalExperience == null)
                return;
            
            ProfessionalExperience.Add(professionalExperience);
        }

        public void AddSkill(Technology technology, int level)
        {
            if (technology == null)
                return;

            if (EmployeeSkills.Any(x => x.TechnologyId == technology.TechnologyId))
                return;

            EmployeeSkills.Add(Domain.EmployeeSkill.Create(technology, level));
        }
        
        public void ApplyForJob(JobOffer jobOffer, byte[] resume)
        {
            if (jobOffer == null)
                return;
            
            if (resume == null)
                return;
            
            JobApplications.Add(JobApplication.Create(this,jobOffer,resume));
        }
        
        public void DeleteCertificate(Guid certificateId)
        {
            var certificateToDelete = Courses
                .SingleOrDefault(x => x.CourseId == certificateId);
            
            if(certificateToDelete == null)
                throw new ArgumentException("Certificate entry doesn't exist.");

            Courses.Remove(certificateToDelete);
        }
        
        public void DeleteEducation(Guid educationId)
        {
            var educationToDelete = Education
                .SingleOrDefault(x => x.EducationId == educationId);
            
            if(educationToDelete == null)
                throw new ArgumentException("Education entry doesn't exist.");

            Education.Remove(educationToDelete);
        }
        
        public void DeleteJobOfferFromFavourites(Guid offerId)
        {
            var jobOffer = Favourites
                .SingleOrDefault(x => x.JobOfferId == offerId);
            
            if(jobOffer == null)
                throw new ArgumentException("Job offer entry doesn't exist.");
            
            Favourites.Remove(jobOffer);
        }
        
        public void DeleteLanguage(Guid languageId)
        {
            var employeeLanguageToDelete = EmployeeLanguages.SingleOrDefault(x => x.Language.LanguageId == languageId);
            if(employeeLanguageToDelete == null)   
                throw new ArgumentException("Employee language entry doesn't exist.");

            EmployeeLanguages.Remove(employeeLanguageToDelete);
        }
        
        public void DeleteProfessionalExperience(Guid professionalExperienceId)
        {
            var professionalExperienceToDelete = ProfessionalExperience
                .SingleOrDefault(x => x.ProfessionalExperienceId == professionalExperienceId);
            
            if(professionalExperienceToDelete == null)
                throw new ArgumentException("Professional experience entry doesn't exist.");

            ProfessionalExperience.Remove(professionalExperienceToDelete);
        }
        
        public void DeleteSkill(Guid skillId)
        {
            var employeeSkillToDelete = EmployeeSkills.SingleOrDefault(x => x.Technology.TechnologyId == skillId);
            if(employeeSkillToDelete == null)   
                throw new ArgumentException("Employee skill entry doesn't exist.");

            EmployeeSkills.Remove(employeeSkillToDelete);
        }
        
        public void EditCertificate(Guid certificateEntryId, Course course)
        {
            if (course == null)
                throw new ArgumentException("Edited course parameter equal null");
            
            var certificateEntryToEdit = Courses
                .SingleOrDefault(x => x.CourseId == certificateEntryId);
            
            if(certificateEntryToEdit == null)
                throw new ArgumentException("Course entry doesn't exist.");
            
            certificateEntryToEdit.EditFrom(course);
        }

        public void EditEducation(Guid educationEntryId, Education education)
        {
            if (education == null)
                throw new ArgumentException("Education parameter is null");
            
            var educationEntryToEdit = Education
                .SingleOrDefault(x => x.EducationId == educationEntryId);
            
            if(educationEntryToEdit == null)
                throw new ArgumentException("Education entry doesn't exist.");
            
            educationEntryToEdit.EditFrom(education);
        }
        
        public void EditLanguage(Guid languageId, int level)
        {
            var employeeLanguageToEdit = EmployeeLanguages.SingleOrDefault(x => x.Language.LanguageId == languageId);
            if(employeeLanguageToEdit == null)
                throw new ArgumentException("Employee language entry doesn't exist.");
            
            employeeLanguageToEdit.SetLanguageLevel(level);
        }

        public void EditPersonalData(string name, string surname, string about, string sex, string birthdate,
             string country, string city, string phone, string websiteUrl)
        {
            if (!IsCityValid(city))
                throw new ArgumentException("City is invalid");
            
            if (!IsCountryValid(country))
                throw new ArgumentException("Country is invalid");
            
            if (!IsNameValid(name))
                throw new ArgumentException("Name is invalid");
            
            if (!IsSexValid(sex))
                throw new ArgumentException("Sex is invalid");
            
            if (!IsSurnameValid(surname))
                throw new ArgumentException("Surname is invalid");
            
            if (!IsBirthDateValid(birthdate))
                throw new ArgumentException("Birthdate is invalid");

            Name = name;
            Surname = surname;
            About = about;
            Sex = sex;
            BirthDate = DateTime.Parse(birthdate);
            Country = country;
            City = city;
            Phone = phone;
            WebsiteUrl = websiteUrl;
        }
        
                       
        public void EditProfessionalExperience(Guid professionalExperienceId, ProfessionalExperience professionalExperience)
        {
            if (professionalExperience == null)
                throw new ArgumentException("ProfessionalExperience parameter is null");

            var professionalExperienceToEdit = ProfessionalExperience
                .SingleOrDefault(x => x.ProfessionalExperienceId
                                      == professionalExperienceId);
            
            if(professionalExperienceToEdit == null)
                throw new ArgumentException("Professional experience entry doesn't exist.");

            professionalExperienceToEdit.EditFrom(professionalExperience);
        }
        
        public void EditSkill(Guid skillId, int level)
        {
            var employeeSkillToEdit = EmployeeSkills.SingleOrDefault(x => x.Technology.TechnologyId == skillId);
            if(employeeSkillToEdit == null)
                throw new ArgumentException("Employee skill entry doesn't exist.");

            employeeSkillToEdit.SetSkillLevel(level);
        }
       
        
        //PRIVATE 
        private Guid CreateId()
        {
            return Guid.NewGuid();
        }
        
        private bool IsNameValid(string name)
        {
            return !string.IsNullOrWhiteSpace(name);
        }
        
        private bool IsSurnameValid(string surname)
        {
            return !string.IsNullOrWhiteSpace(surname);
        }
        
        private bool IsSexValid(string sex)
        {
            return ApplicationGenderValues.IsValidGender(sex);
        }
        
        private bool IsBirthDateValid(string birthDate)
        {
            return DateTime.TryParse(birthDate, out var parsedBirthdate);
        }     
        
        private bool IsCityValid(string city)
        {
            return !string.IsNullOrWhiteSpace(city);
        }
        
        private bool IsCountryValid(string country)
        {
            return !string.IsNullOrWhiteSpace(country);
        }

    }
}
