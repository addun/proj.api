using System;
using System.Collections.Generic;

namespace justfinditApi.Core.Domain
{
    public class Course
    {
        public Guid CourseId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public string CourseName { get; private set; }
        public DateTime DateOfObtain { get; private set; }
        public string OrganizerName { get; private set; }
        public Employee Employee { get; private set; }

        protected Course()
        {
            CourseId = Guid.NewGuid();
        }
        
        protected Course(string courseName, string organizerName, string monthOfObtain, string yearOfObtain): this()
        {
            if (string.IsNullOrWhiteSpace(courseName))
            {
                throw new ArgumentException("Course name can not be empty.", nameof(courseName));
            }
            if (string.IsNullOrWhiteSpace(organizerName))
            {
                throw new ArgumentException("Organizer name name can not be empty.", nameof(organizerName));
            }  
            if (!DateTime.TryParse($"{yearOfObtain}-{monthOfObtain}-01", out var parsedDateOfObtain))
            {
                throw new ArgumentException("Obtain date is invalid.");
            }

            CourseName = courseName;
            OrganizerName = organizerName;
            DateOfObtain = parsedDateOfObtain;
        }

        public void EditFrom(Course course)
        {
            CourseName = course.CourseName;
            OrganizerName = course.OrganizerName;
            DateOfObtain = course.DateOfObtain;
        }
        
        public static Course Create(string courseName, string organizerName, string monthOfObtain, string yearOfObtain)
        {
            return new Course(courseName, organizerName, monthOfObtain, yearOfObtain);
        }
    }
}