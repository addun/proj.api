using System;

namespace justfinditApi.Core.Domain
{
    public class EmployeeFavouriteJobOffer
    {
        public Guid EmployeeId { get; private set; }
        public Employee Employee { get; private set; }
        public Guid JobOfferId { get; private set; }
        public JobOffer JobOffer { get; private set; }

        protected EmployeeFavouriteJobOffer()
        {}

        protected EmployeeFavouriteJobOffer(Employee employee, JobOffer jobOffer)
        {
            if (employee == null)
            {
                throw new ArgumentException("Employee name can not be null.", nameof(employee));
            }
            
            if (jobOffer == null)
            {
                throw new ArgumentException("Job offer name can not be null.", nameof(jobOffer));
            }

            Employee = employee;
            EmployeeId = employee.EmployeeId;
            JobOffer = jobOffer;
            JobOfferId = jobOffer.OfferId;
        }
        
        public static EmployeeFavouriteJobOffer Create(Employee employee, JobOffer jobOffer)
        {
            return new EmployeeFavouriteJobOffer(employee, jobOffer);
        }
    }
}