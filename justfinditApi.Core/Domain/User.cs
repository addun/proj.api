﻿using System;
using Microsoft.AspNetCore.Identity;

namespace justfinditApi.Core.Domain
{
    public class User: IdentityUser<Guid>
    {
        public string VerificationCode { get; set; }
        public string LastGeneratedSecurityToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
