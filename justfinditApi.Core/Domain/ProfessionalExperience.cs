using System;

namespace justfinditApi.Core.Domain
{
    public class ProfessionalExperience
    {
        public Guid ProfessionalExperienceId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public string Position { get; private set; }
        public string CompanyName { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }
        public string Country { get; private set; }
        public string City { get; private set; }
        public string JobDescription { get; private set; }
        public Employee Employee { get; private set; }

        protected ProfessionalExperience()
        {
            ProfessionalExperienceId = Guid.NewGuid();
        }

        protected ProfessionalExperience(string position, string companyName, string startMonth, string startYear, 
            string endMonth, string endYear, string country, string city, string jobDescription = ""):this()
        {
            SetPosition(position);
            SetCompanyName(companyName);
            SetStartDate(startMonth,startYear);
            SetEndDate(endMonth,endYear);
            SetCountry(country);
            SetCity(city);
            SetJobDescription(jobDescription);
        }

        public static ProfessionalExperience Create(string position, string companyName, string startMonth,
            string startYear, string endMonth, string endYear, string country, string city, string jobDescription = "")
        {
            return new ProfessionalExperience(position, companyName, startMonth, startYear, 
                endMonth, endYear, country, city, jobDescription);
        }

        public void EditFrom(ProfessionalExperience professionalExperience)
        {
            Position = professionalExperience.Position;
            CompanyName = professionalExperience.CompanyName;
            StartDate = professionalExperience.StartDate;
            EndDate = professionalExperience.EndDate;
            Country = professionalExperience.Country;
            City = professionalExperience.City;
            JobDescription = professionalExperience.JobDescription;
        }

        private void SetPosition(string position)
        {
            if (string.IsNullOrWhiteSpace(position))
            {
                throw new ArgumentException("Position name can not be empty.", nameof(position));
            }

            Position = position;
        }
        
        private void SetCompanyName(string companyName)
        {
            if (string.IsNullOrWhiteSpace(companyName))
            {
                throw new ArgumentException("Company name can not be empty.", nameof(companyName));
            }

            CompanyName = companyName;
        }
        
        private void SetCountry(string country)
        {
            if (string.IsNullOrWhiteSpace(country))
            {
                throw new ArgumentException("Country can not be empty.", nameof(country));
            }

            Country = country;
        }
        
        private void SetCity(string city)
        {
            if (string.IsNullOrWhiteSpace(city))
            {
                throw new ArgumentException("City name can not be empty.", nameof(city));
            }

            City = city;
        }
        
        private void SetStartDate(string month, string year)
        {
            if (!DateTime.TryParse($"{year}-{month}-01", out var parsedStartDate))
            {
                throw new ArgumentException("Start date is invalid.");
            }

            StartDate = parsedStartDate;
        }
        
        private void SetEndDate(string month, string year)
        {
            if (DateTime.TryParse($"{year}-{month}-01", out var parsedEndDate))
            {
                EndDate = parsedEndDate;
            }
            else if (string.IsNullOrWhiteSpace(month) && string.IsNullOrWhiteSpace(year))
            {
                EndDate = null;
            }
            else
            {
                throw new ArgumentException("End date is invalid.");
            }
        }

        private void SetJobDescription(string jobDescription = "")
        {
            JobDescription = jobDescription;
        }
    }
    
}