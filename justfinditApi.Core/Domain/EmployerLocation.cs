using System;

namespace justfinditApi.Core.Domain
{
    public class EmployerLocation
    {
        public string ApartmentNumber { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public string FlatNumber { get; private set; }
        public string PostalCode { get; private set; }
        public string Street { get; private set; }

        public Guid EmployerLocationId { get; private set; }
        public Employer Employer { get; private set; }
        public Guid EmployerId { get; private set; }

        protected EmployerLocation()
        {
             EmployerLocationId = Guid.NewGuid();
        }
        
        protected EmployerLocation(string country, string city, string street, string postalCode,
            string apartmentNumber, string flatNumber): this()
        {
            if (string.IsNullOrWhiteSpace(country))
            {
                throw new ArgumentException("Country name can not be empty.", nameof(country));
            }
            if (string.IsNullOrWhiteSpace(city))
            {
                throw new ArgumentException("City name can not be empty.", nameof(city));
            } 
            if (string.IsNullOrWhiteSpace(street))
            {
                throw new ArgumentException("Street name can not be empty.", nameof(street));
            }  
            if (string.IsNullOrWhiteSpace(postalCode))
            {
                throw new ArgumentException("Postal code can not be empty.", nameof(postalCode));
            }  
            if (string.IsNullOrWhiteSpace(apartmentNumber))
            {
                throw new ArgumentException("Apartment number can not be empty.", nameof(apartmentNumber));
            }

            ApartmentNumber = apartmentNumber;
            City = city;
            Country = country;
            FlatNumber = flatNumber;
            PostalCode = postalCode;
            Street = street;
        }
        
        public static EmployerLocation Create(string country, string city, string street, string postalCode,
            string apartmentNumber, string flatNumber = null)
        {
            return new EmployerLocation(country, city, street, postalCode, apartmentNumber, flatNumber);
        }
        
        public void EditFrom(EmployerLocation location)
        {
            Country = location.Country;
            City = location.City;
            Street = location.Street;
            ApartmentNumber = location.ApartmentNumber;
            FlatNumber = location.FlatNumber;
            PostalCode = location.PostalCode;   
        }
    }
}