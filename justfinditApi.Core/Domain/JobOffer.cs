using System;
using System.Collections.Generic;
using System.Linq;
using justfinditApi.Core.ConstantValues;

namespace justfinditApi.Core.Domain
{
    public class JobOffer
    {
        public Guid OfferId { get; private set; }
        public string Title { get; private set; }
        public string Country { get; private set; }
        public string City { get; private set; }
        public string Address { get; private set; }
        public DateTime CreationDate { get; private set; }
        public long SalaryFrom { get; private set; }
        public long SalaryTo { get; private set; }
        public string Currency { get; private set; }
        public string ExperienceLevel { get; private set; }
        public string InvoiceType { get; private set; }
        public string WorkTime { get; private set; }
        public string OfferDescription { get; private set; }
        public string Remote { get; private set; }
        
        public Guid EmployerId { get; private set; }
        public Employer Employer { get; private set; }
        public ICollection<JobApplication> JobApplication { get; private set; }
        public ICollection<EmployeeFavouriteJobOffer> Favourites { get; private set; }
        public List<JobOfferTechnology> Skills { get; private set; }

        protected JobOffer()
        {
            OfferId = Guid.NewGuid();
            Skills = new List<JobOfferTechnology>();
            JobApplication = new List<JobApplication>();
            Favourites = new List<EmployeeFavouriteJobOffer>();
        }
        
        protected JobOffer(string title, string country, string city, string address, string salaryFrom,
            string salaryTo, string currency, string workTime, string invoiceType, string experienceLevel,
            string offerDescription, string remote): this()
        {
            if (!IsTitleValid(title))
                throw new ArgumentException("Title is invalid");
            
            if (!IsCountryValid(country))
                throw new ArgumentException("Country is invalid");
            
            if (!IsCityValid(city))
                throw new ArgumentException("City is invalid");
            
            if (!IsAddressValid(address))
                throw new ArgumentException("Address is invalid");
            
            if (!IsSalaryFromValid(salaryFrom))
                throw new ArgumentException("SalaryFrom is invalid");
            
            if (!IsSalaryToValid(salaryTo))
                throw new ArgumentException("SalaryTo is invalid");
           
            if (!IsCurrencyValid(currency))
                throw new ArgumentException("Currency is invalid");
            
            if (!IsWorkTimeValid(workTime))
                throw new ArgumentException("Work time is invalid");
            
            if (!IsInvoiceTypeValid(invoiceType))
                throw new ArgumentException("InvoiceType is invalid");
            
            if (!IsRemoteValid(remote))
                throw new ArgumentException("Remote is invalid");
            
            if (!IsExperienceLevelValid(experienceLevel))
                throw new ArgumentException("Experience Level is invalid");
            
            if (!IsOfferDescriptionValid(offerDescription))
                throw new ArgumentException("Offer Description is invalid");
            
            if (!IsRemoteValid(remote))
                throw new ArgumentException("Remote is invalid");
            
            
            Title = title;
            Country = country;
            City = city;
            Address = address;
            SalaryFrom = long.Parse(salaryFrom);
            SalaryTo = long.Parse(salaryTo);
            Currency = currency;
            WorkTime = workTime;
            InvoiceType = invoiceType;
            ExperienceLevel = experienceLevel;
            OfferDescription = offerDescription;
            Remote = remote;
            CreationDate = DateTime.Now;
        }
        
        public static JobOffer Create(string title, string country, string city, string address, string salaryFrom,
            string salaryTo, string currency,string workTime, string invoiceType, string experienceLevel,
            string offerDescription, string remote)
        {
            return new JobOffer(title, country, city, address, salaryFrom, salaryTo, currency, workTime,
                invoiceType,experienceLevel, offerDescription, remote);
        }

        public void AddSkill(Technology technology)
        {
            if (technology == null)
                return;
            
            Skills.Add(JobOfferTechnology.Create(technology));    
        }
        
        public void AddSkills(IEnumerable<Technology> technologies)
        {
            if (technologies == null)
                return;

            foreach (var technology in technologies)
            {
                AddSkill(technology);
            }
        }

        public void EditFrom(JobOffer jobOffer)
        {
            Title = jobOffer.Title;
            Country = jobOffer.Country;
            City = jobOffer.City;
            Address = jobOffer.Address;
            CreationDate = jobOffer.CreationDate;
            SalaryFrom = jobOffer.SalaryFrom;
            SalaryTo = jobOffer.SalaryTo;
            Currency = jobOffer.Currency;
            WorkTime = jobOffer.WorkTime;
            InvoiceType = jobOffer.InvoiceType;
            ExperienceLevel = jobOffer.ExperienceLevel;
            OfferDescription = jobOffer.OfferDescription;
            Remote = jobOffer.Remote;

            Skills.RemoveAll(x => true);
            foreach (var skill in jobOffer.Skills)
            {
                Skills.Add(skill);
            }
        }

        private bool IsRemoteValid(string remote)
        {
            return !string.IsNullOrWhiteSpace(remote);
        }

        private bool IsOfferDescriptionValid(string offerDescription)
        {
            return !string.IsNullOrWhiteSpace(offerDescription);
        }

        private bool IsExperienceLevelValid(string experienceLevel)
        {
            return ApplicationJobExperienceValues.IsJobExperienceValid(experienceLevel);
        }

        private bool IsCurrencyValid(string currency)
        {
            return ApplicationCurrencyValues.IsCurrencyValid(currency);
        }

        private bool IsSalaryToValid(string salaryTo)
        {
            return long.TryParse(salaryTo, out var tempSalaryTo);
        }

        private bool IsSalaryFromValid(string salaryFrom)
        {
            return long.TryParse(salaryFrom, out var tempSalaryFrom);
        }

        private bool IsAddressValid(string address)
        {
            return !string.IsNullOrWhiteSpace(address);
        }

        private bool IsCityValid(string city)
        {
            return !string.IsNullOrWhiteSpace(city);
        }

        private bool IsCountryValid(string country)
        {
            return !string.IsNullOrWhiteSpace(country);
        }

        private bool IsTitleValid(string title)
        {
            return !string.IsNullOrWhiteSpace(title);
        }
        
        private bool IsWorkTimeValid(string workTime)
        {
            return !string.IsNullOrWhiteSpace(workTime);
        }

        private bool IsInvoiceTypeValid(string invoiceType)
        {
            return !string.IsNullOrWhiteSpace(invoiceType);
        }
    }
}