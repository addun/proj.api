﻿using System;

namespace justfinditApi.Core.Domain
{
    public class EmployerTechnology
    {
        public Guid EmployerId { get; private set; }
        public Guid TechnologyId { get; private set; }
        public Employer Employer { get; private set; }
        public Technology Technology { get; private set; }

        protected EmployerTechnology()
        {}

        protected EmployerTechnology(Technology technology)
        {
            if (technology == null)
                throw new ArgumentNullException(nameof(technology));
            
            Technology = technology;
        }

        public static EmployerTechnology Create(Technology technology)
        {
            return new EmployerTechnology(technology);
        }
    }
}
