using System;

namespace justfinditApi.Core.Domain
{
    public class JobApplication
    {
        public Guid EmployeeId { get; private set; }
        public Employee Employee { get; private set; }
        public Guid JobOfferId { get; private set; }
        public JobOffer JobOffer { get; private set; }
        public byte[] Resume { get; set; }
        public string ResumeExtension { get; set; }

        protected JobApplication() {}

        protected JobApplication(Employee employee, JobOffer jobOffer, byte[] resume, string resumeExtension)
        {
            if (employee == null)
            {
                throw new ArgumentException("Employee name can not be null.", nameof(employee));
            }
            
            if (jobOffer == null)
            {
                throw new ArgumentException("Job offer name can not be null.", nameof(jobOffer));
            }
            
            if (resume == null)
            {
                throw new ArgumentException("Resume name can not be null.", nameof(resume));
            }
            
            Employee = employee;
            EmployeeId = employee.EmployeeId;
            JobOffer = jobOffer;
            JobOfferId = jobOffer.OfferId;
            Resume = resume;
            ResumeExtension = resumeExtension;
        }

        public static JobApplication Create(Employee employee, JobOffer jobOffer, byte[] resume,
            string resumeExtension = "application/pdf")
        {
            return new JobApplication(employee, jobOffer, resume, resumeExtension);
        }
    }
}