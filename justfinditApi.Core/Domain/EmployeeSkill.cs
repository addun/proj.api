using System;

namespace justfinditApi.Core.Domain
{
    public class EmployeeSkill
    {
        public Guid EmployeeId { get; private set; }
        public Employee Employee { get; private set; }
        public Guid TechnologyId { get; private set; }
        public Technology Technology { get; private set; }
        public int Level { get; private set; }

        protected EmployeeSkill()
        {}
        
        protected EmployeeSkill(Technology technology, int level)
        {
            SetSkillLevel(level);
            Technology = technology ?? throw new ArgumentException("Technology argument is null");   
        }

        public static EmployeeSkill Create(Technology technology, int level)
        {
            return new EmployeeSkill(technology,level);
        }

        public void SetSkillLevel(int level)
        {
            if(level < 1 || level > 5)
                throw new ArgumentException("Skill level is not in range");
            
            Level = level;
        }
    }
}