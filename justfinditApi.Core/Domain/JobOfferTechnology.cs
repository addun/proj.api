using System;

namespace justfinditApi.Core.Domain
{
    public class JobOfferTechnology
    {
        public Guid JobOfferId { get; private set; }
        public Guid TechnologyId { get; private set; }
        public JobOffer JobOffer { get; private set; }
        public Technology Technology { get; private set; }

        protected JobOfferTechnology()
        {}

        protected JobOfferTechnology(Technology technology)
        {
            if (technology == null)
                throw new ArgumentNullException(nameof(technology));
            
            Technology = technology;
        }

        public static JobOfferTechnology Create(Technology technology)
        {
            return new JobOfferTechnology(technology);
        }
    }
}