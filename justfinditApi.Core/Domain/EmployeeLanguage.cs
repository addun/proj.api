using System;

namespace justfinditApi.Core.Domain
{
    public class EmployeeLanguage
    {
        public Guid EmployeeId { get; private set; }
        public Employee Employee { get; private set; }
        public Guid LanguageId { get; private set; }
        public Language Language { get; private set; }
        public int Level { get; private set; }

        protected EmployeeLanguage()
        {}

        protected EmployeeLanguage(Language language, int level)
        {
            SetLanguageLevel(level);
            Language = language ?? throw new ArgumentException("Language argument is null");
        }

        public static EmployeeLanguage Create(Language language, int level)
        {
            return new EmployeeLanguage(language, level);
        }

        public void SetLanguageLevel(int level)
        {
            if(level < 1 || level > 5)
                throw new ArgumentException("Language level is not in range");

            Level = level;
        }
    }
}