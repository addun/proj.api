using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace justfinditApi.Core.Domain
{
    public class Language
    {
        public Guid LanguageId { get; private set; }
        public string LanguageName { get; private set; }
        public ICollection<EmployeeLanguage> EmployeeLanguages { get; private set; }

        protected Language()
        {
            LanguageId = Guid.NewGuid();
        }
        
        protected Language(string languageName): this()
        {
            if(string.IsNullOrWhiteSpace(languageName))
                throw new ArgumentException("Language name is invalid (null or empty)");
            LanguageName = languageName;
        }

        public static Language Create(string languageName)
        {
            return new Language(languageName);
        }
    }
}