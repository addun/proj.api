﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace justfinditApi.Core.Domain
{
    public class Technology
    {
        public Guid TechnologyId { get; private set; }
        public string TechnologyName { get; private set; }
        public ICollection<EmployerTechnology> EmployerTechnologies { get; private set; }
        public ICollection<EmployeeSkill> EmployeeSkills { get; private set; }
        public ICollection<JobOfferTechnology> JobOfferTechnologies { get; private set; }

        protected Technology()
        {
            TechnologyId = Guid.NewGuid();
        }

        protected Technology(string technologyName): this()
        {
            if(string.IsNullOrWhiteSpace(technologyName))
                throw new ArgumentException("Language name is invalid (null or empty)");

            TechnologyName = technologyName;
        }

        public static Technology Create(string technologyName)
        {
            return new Technology(technologyName);
        }
    }
}
