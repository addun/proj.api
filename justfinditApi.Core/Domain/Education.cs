using System;

namespace justfinditApi.Core.Domain
{
    public class Education
    {
        public Guid EducationId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public string SchoolName { get; private set; }
        public string StartYear { get; private set; }
        public string EndYear { get; private set; }
        public string BranchOfStudy { get; private set; }
        public string Speciality { get; private set; }
        public string Degree { get; private set; }
        public Employee Employee { get; private set; }

        protected Education()
        {
            EducationId = Guid.NewGuid();
        }

        protected Education(string schoolName, string startYear, string endYear, string branchOfStudy,
            string degree, string speciality=""):this()
        {
            if (string.IsNullOrWhiteSpace(schoolName))
            {
                throw new ArgumentException("School name can not be empty.", nameof(schoolName));
            }
            if (string.IsNullOrWhiteSpace(branchOfStudy))
            {
                throw new ArgumentException("Branch of study can not be empty.", nameof(branchOfStudy));
            }
            if (string.IsNullOrWhiteSpace(degree))
            {
                throw new ArgumentException("Degree can not be empty.", nameof(degree));
            }

            var parseStartYearSuccedeed = int.TryParse(startYear, out int parsedStartYear);
            var parseEndYearSuccedeed =  int.TryParse(endYear, out int parsedEndYear);
            
            if (!parseStartYearSuccedeed)
            {
                throw new ArgumentException("Start year is invalid.");
            }

            StartYear = startYear;

            if (string.IsNullOrWhiteSpace(endYear))
            {
                EndYear = null;
            }
            else if (!parseEndYearSuccedeed)
            {
                throw new ArgumentException("End year is invalid.");
            }
            else
            {
                EndYear = endYear;
            }

            SchoolName = schoolName;
            BranchOfStudy = branchOfStudy;
            Degree = degree;
            Speciality = speciality;
        }

        public static Education Create(string schoolName, string startYear, string endYear, string branchOfStudy,
            string degree, string speciality = "")
        {
            return new Education(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
        }
        
        public void EditFrom(Education education)
        {
            SchoolName = education.SchoolName;
            BranchOfStudy = education.BranchOfStudy;
            Speciality = education.Speciality;
            Degree = education.Degree;
            StartYear = education.StartYear;
            EndYear = education.EndYear;
        }
    }
}