using System;
using System.Security.Permissions;

namespace justfinditApi.Core.Domain
{
    public class Benefit
    {
        public Guid BenefitId { get; private set; }
        public Guid EmployerId { get; private set; }
        public string Title { get; private set; } 
        public string Description { get; private set; } 
        public Employer Employer { get; private set; }

        protected Benefit()
        {
            BenefitId = Guid.NewGuid();
        }
        
        protected Benefit(string title, string description): this()
        {
           if (string.IsNullOrWhiteSpace(title))
               throw new ArgumentException("Title cannot be empty");

            Title = title;
            Description = description;
        }

        public static Benefit Create(string title, string description)
        {
             return new Benefit(title, description);
        }
        
        public void EditFrom(Benefit benefit)
        {
            Title = benefit.Title;
            Description = benefit.Description;
        }
    }
}