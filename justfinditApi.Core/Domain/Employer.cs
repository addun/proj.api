﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;

namespace justfinditApi.Core.Domain
{
    public class Employer
    {
        public Guid EmployerId { get; private set; }
        public User User { get; private set; }
        public string CompanyName { get; private set; }
        public string CompanyDescription { get; private set; }
        public string FoundedYear { get; private set; }
        public string CompanySize { get; private set; }
        public string Nip { get; private set; }
        public string WebsiteUrl { get; private set; }
        public string WhatsWeCreatingDescription { get; private set; }    
        public string WorkingWithUsDescription { get; private set; }    
        public string InternshipDescription { get; private set; }    
        public Guid HeadquartersId { get; private set; }    
        public ICollection<Benefit> Benefits { get; private set; }
        public ICollection<EmployerLocation> Locations { get; private set; }
        public ICollection<JobOffer> JobOffers { get; private set; }
        public ICollection<EmployerTechnology> Technologies { get; private set; }


        protected Employer()
        {
            Benefits = new List<Benefit>();
            Locations = new List<EmployerLocation>();
            Technologies = new List<EmployerTechnology>();
            JobOffers = new List<JobOffer>();
        }

        public Employer(
            string companyName, 
            string companyDescription, 
            string foundedYear, 
            string companySize, 
            string nip,
            string websiteUrl,
            EmployerLocation mainEmployerLocation,
            User user): this()
        {
            if(user == null)
                throw new ArgumentException("User is null");
            
            if(mainEmployerLocation == null)
                throw new ArgumentException("Company location is null");
            
            if(!IsCompanyNameValid(companyName))
                throw new ArgumentException("Company name is invalid");
            
            if(!IsFoundedYearValid(foundedYear))
                throw new ArgumentException("Founded year is invalid");
            
            if(!IsCompanySizeValid(companySize))
                throw new ArgumentException("Company size is invalid");
            
            if(!IsNipValid(nip))
                throw new ArgumentException("Nip is invalid");
            
            if(!IsWebsiteUrlValid(websiteUrl))
                throw new ArgumentException("Website url is invalid");

            User = user;
            // USER ID AND EMPLOYER ID ARE EQUAL
            EmployerId = user.Id;
            CompanyName = companyName;
            CompanyDescription = companyDescription;
            FoundedYear = foundedYear;
            CompanySize = companySize;
            Nip = nip;
            WebsiteUrl = websiteUrl;
            
            Locations.Add(mainEmployerLocation);
            SetAsHeadquarters(mainEmployerLocation.EmployerLocationId);
        }

        public void AddBenefit(Benefit benefit)
        {
            if (benefit == null)
                return;
            
            Benefits.Add(benefit);
        }
        
        public void AddLocation(EmployerLocation location)
        {
            if (location == null)
                return;
            
            Locations.Add(location);
        }
        
        public void AddTechnology(Technology technology)
        {
            if (technology == null)
                return;
            
            if (Technologies.Any(x=>x.TechnologyId == technology.TechnologyId))
                return;
            
            Technologies.Add(EmployerTechnology.Create(technology));
        }
        
        public void AddJobOffer(JobOffer jobOffer)
        {
            if (jobOffer == null)
                return;
            
            JobOffers.Add(jobOffer);
        }
        
        public void DeleteBenefit(Guid benefitId)
        {
            var benefitToDelete = Benefits
                .SingleOrDefault(x => x.BenefitId == benefitId);
            
            if(benefitToDelete == null)
                throw new ArgumentException("Benefit entry doesn't exist.");

            Benefits.Remove(benefitToDelete);
        }
        
        public void DeleteJobOffer(Guid jobOfferId)
        {
            var jobOfferToDelete = JobOffers
                .SingleOrDefault(x => x.OfferId == jobOfferId);
            
            if(jobOfferToDelete == null)
                throw new ArgumentException("Job offer entry doesn't exist.");

            JobOffers.Remove(jobOfferToDelete);
        }

        public void DeleteLocation(Guid locationId)
        {
            var locationToDelete = Locations
                .SingleOrDefault(x => x.EmployerLocationId == locationId);
            
            if(locationToDelete == null)
                throw new ArgumentException("Location entry doesn't exist.");

            Locations.Remove(locationToDelete);
        }
        
        public void DeleteTechnology(Guid technologyId)
        {
            var technologyToDelete = Technologies
                .SingleOrDefault(x => x.TechnologyId == technologyId);
            
            if(technologyToDelete == null)
                throw new ArgumentException("Technology entry doesn't exist.");

            Technologies.Remove(technologyToDelete);
        }
        
        public void EditBenefit(Guid benefitEntryId, Benefit benefit)
        {
            if (benefit == null)
                throw new ArgumentException("Edited benefit parameter equal null");
            
            var benefitEntryToEdit = Benefits
                .SingleOrDefault(x => x.BenefitId == benefitEntryId);
            
            if(benefitEntryToEdit == null)
                throw new ArgumentException("Benefit entry doesn't exist.");
            
            benefitEntryToEdit.EditFrom(benefit);
        }
        
        public void EditEmployerData(string companyName, string companyDescription, string foundedYear,
            string companySize, string nip, string websiteUrl)
        {
            if(!IsCompanyNameValid(companyName))
                throw new ArgumentException("Company name is invalid");
            
            if(!IsFoundedYearValid(foundedYear))
                throw new ArgumentException("Founded year is invalid");
            
            if(!IsCompanySizeValid(companySize))
                throw new ArgumentException("Company size is invalid");
            
            if(!IsNipValid(nip))
                throw new ArgumentException("Nip is invalid");
            
            if(!IsWebsiteUrlValid(websiteUrl))
                throw new ArgumentException("Website url is invalid");
            
            CompanyName = companyName;
            CompanyDescription = companyDescription;
            FoundedYear = foundedYear;
            CompanySize = companySize;
            Nip = nip;
            WebsiteUrl = websiteUrl;
        }
        
        public void EditInternshipDescription(string description)
        {
            InternshipDescription = description;
        }
        
        public void EditJobOffer(Guid jobOfferId, JobOffer jobOffer)
        {
            if (jobOffer == null)
                throw new ArgumentException("Edited job offer parameter equal null");
            
            var jobOfferEntryToEdit = JobOffers
                .SingleOrDefault(x => x.OfferId == jobOfferId);
            
            if(jobOfferEntryToEdit == null)
                throw new ArgumentException("Job offer entry doesn't exist.");
            
            jobOfferEntryToEdit.EditFrom(jobOffer);
        }
        
        public void EditLocation(Guid locationEntryId, EmployerLocation location)
        {
            if (location == null)
                throw new ArgumentException("Edited benefit parameter equal null");
            
            var locationEntryToEdit = Locations
                .SingleOrDefault(x => x.EmployerLocationId == locationEntryId);
            
            if(locationEntryToEdit == null)
                throw new ArgumentException("Location entry doesn't exist.");
            
            locationEntryToEdit.EditFrom(location);
        }

        public void EditWhatWeCreatingDescription(string description)
        {
            WhatsWeCreatingDescription = description;
        }
        
        public void EditWhyWorthWorkingWithUs(string description)
        {
            WorkingWithUsDescription = description;
        } 
        
        public void SetAsHeadquarters(Guid locationId)
        {
            if(!Locations.Any(x=>x.EmployerLocationId == locationId))
                throw new ArgumentException("Location does not exist in object");
            
            HeadquartersId = locationId;
        }

        // PRIVATE
        private bool IsCompanyNameValid(string companyName)
        {
            return !string.IsNullOrWhiteSpace(companyName);
        }

        private bool IsFoundedYearValid(string foundedYear)
        {
            return !string.IsNullOrWhiteSpace(foundedYear) && foundedYear.All(char.IsDigit);
        }

        private bool IsCompanySizeValid(string companySize)
        {
            return !string.IsNullOrWhiteSpace(companySize) && companySize.All(char.IsDigit);
        }

        private bool IsNipValid(string nip)
        {
            return !string.IsNullOrWhiteSpace(nip) && nip.All(char.IsDigit);
        }

        private bool IsWebsiteUrlValid(string websiteUrl)
        {
            return !string.IsNullOrWhiteSpace(websiteUrl);
        }
    }
}
