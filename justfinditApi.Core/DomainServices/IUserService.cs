using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.DomainServices
{
    public interface IUserService
    {
        Task<User> CreateUser(string email, string password);
        Task ConfirmEmail(User user, string token);
        JwtSecurityToken DeserializeJwt(string token);
        Task<string> GeneratePasswordResetTokenAsync(User user);
        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task<string> TryGenerateJwtTokenAsync(User user, string password);
        Task<string> TryRefreshTokenAsync(User user, string refreshToken);
        Task<string> GenerateRefreshTokenForUserAsync(User user);
        Task<bool> CheckPasswordAsync(User user, string password);
        Task<bool> ResetPassword(User user, string newPassword, string token);
        Task AddUserToRoleAsync(User user, string roleName);
    }
}