using System;

namespace justfinditApi.Core.ConstantValues
{
    public class ApplicationCurrencyValues
    {
        public const string PLN = "PLN";
        public const string EUR = "EUR";
        public const string USD = "USD";
        public const string GBP = "GBP";

        public static bool IsCurrencyValid(string currency)
        {
            return string.Equals(currency, PLN, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(currency, EUR, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(currency, USD, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(currency, GBP, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}