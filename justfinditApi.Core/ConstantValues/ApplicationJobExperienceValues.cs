using System;

namespace justfinditApi.Core.ConstantValues
{
    public class ApplicationJobExperienceValues
    {
        public const string JUNIOR = "JUNIOR";
        public const string MID = "MID";
        public const string SENIOR = "SENIOR";
        public const string ANY = "ANY";

        public static bool IsJobExperienceValid(string jobExperience)
        {
            return string.Equals(jobExperience, JUNIOR, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(jobExperience, MID, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(jobExperience, SENIOR, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(jobExperience, ANY, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}