using System;

namespace justfinditApi.Core.ConstantValues
{
    public static class ApplicationGenderValues
    {
        public const string NODEFINED = "NODEFINED";
        public const string WOMAN = "WOMAN";
        public const string MAN = "MAN";

        public static bool IsValidGender(string phrase)
        {
            return string.Equals(phrase, NODEFINED, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(phrase, WOMAN, StringComparison.CurrentCultureIgnoreCase)
                   || string.Equals(phrase, MAN, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}