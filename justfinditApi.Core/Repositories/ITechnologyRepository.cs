using System.Collections.Generic;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface ITechnologyRepository
    {
        Task<Technology> AddTechnologyAsync(Technology technology);
        Task<Technology> GetTechnologyByNameAsync(string name);
        Task<IEnumerable<Technology>> GetAllAsync();
    }
}