using System;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface IJobOffersRepository
    {
        Task<JobOffer> FindById(Guid id);
    }
}