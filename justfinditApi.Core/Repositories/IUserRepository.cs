using System;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetUserByIdAsync(Guid userId);
        Task<User> GetUserByEmailAsync(string email);
        void UpdateUser(User user);
    }
}