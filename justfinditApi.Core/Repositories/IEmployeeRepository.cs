﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee> AddAsync(Employee employee);
        Task<Employee> GetByEmailAsync(string email);
        Task<Employee> GetByUserIdAsync(Guid userId);
        Task UpdateEmployee(Employee employee);
    }
}
