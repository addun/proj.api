﻿using System.Threading.Tasks;

namespace justfinditApi.Core.Repositories
{
    public interface IUnitOfWork
    {
        IJobOffersRepository JobOffers { get; }
        IEmployeeRepository Employees { get; }
        IEmployerRepository Employers { get; }
        ILanguageRepository Languages { get; }
        ITechnologyRepository Technologies { get; }       
        IUserRepository Users { get; }

        Task<int> CompleteAsync();

        Task BeginTransactionAsync();
        void CommitTransaction();
    }
}
