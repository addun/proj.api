using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface ILanguageRepository
    {
        Task<Language> GetLanguageByName(string name);
    }
}