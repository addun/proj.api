﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;

namespace justfinditApi.Core.Repositories
{
    public interface IEmployerRepository
    {
        Task<Employer> AddAsync(Employer employer);
        Task<Employer> GetByEmailAsync(string email);
        Task<Employer> GetByUserIdAsync(Guid userId);
        Task UpdateEmployer(Employer employer);
    }
}
