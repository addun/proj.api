namespace justfinditApi.Infrastucture.Dtos
{
    public class AuthTokenDto
    {
        public string Token { get; private set; }
        public string RefreshToken { get; private set; }

        protected AuthTokenDto()
        {
        }

        protected AuthTokenDto(string token, string refreshToken)
        {
            Token = token;
            RefreshToken = refreshToken;
        }

        public static AuthTokenDto Create(string token, string refreshToken)
        {
            return new AuthTokenDto(token,refreshToken);
        }
    }
}