using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class LanguageDto
    {
        public string LanguageId { get; set; }
        public string LanguageName { get; set; }
        public int Level { get; set; }
    }
}