using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddSkillDto
    {
        public Guid UserId { get; set; }
        public SkillDto Skill { get; set; }
    }
}