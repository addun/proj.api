using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddEducationDto
    {
        public Guid UserId { get; set; }
        public EducationDto Education { get; set; }
    }
}