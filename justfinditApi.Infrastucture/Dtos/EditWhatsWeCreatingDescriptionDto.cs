using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditWhatsWeCreatingDescriptionDto
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
    }
}