namespace justfinditApi.Infrastucture.Dtos
{
    public class EmployerDataDto
    {
        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string CompanyDescription { get; set; }
        public string FoundedYear { get; set; }
        public string CompanySize { get; set; }
        public string Nip { get; set; }
        public string WebsiteUrl { get; set; }
    }
}