using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployerBenefitDto
    {
        public Guid UserId { get; set; }
        public BenefitDto Benefit { get; set; }
    }
}