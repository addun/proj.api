using System;
using Microsoft.AspNetCore.Http;

namespace justfinditApi.Infrastucture.Dtos
{
    public class ApplyForJobDto
    {
        public Guid UserId { get; set; }
        public Guid JobId { get; set; }
        public IFormFile UploadFile { get; set; }
    }
}