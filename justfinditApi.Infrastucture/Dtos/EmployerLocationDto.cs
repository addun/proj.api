using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EmployerLocationDto
    {   
        public string EmployerLocationId { get; set; }
        public string ApartmentNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string FlatNumber { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
    }
}