using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditProfessionalExperienceDto
    {
        public Guid UserId { get; set; }
        public ProfessionalExperienceDto ProfessionalExperience { get; set; }
    }
}