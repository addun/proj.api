using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditJobOfferDto
    {
        public Guid UserId { get; set; }
        public JobOfferDto JobOffer { get; set; }
    }
}