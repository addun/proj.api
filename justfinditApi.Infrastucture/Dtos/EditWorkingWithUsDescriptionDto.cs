using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditWorkingWithUsDescriptionDto
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
    }
}