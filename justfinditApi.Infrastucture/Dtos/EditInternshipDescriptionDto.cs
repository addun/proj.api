using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditInternshipDescriptionDto
    {
        public Guid UserId { get; set; }
        public string Description { get; set; }
    }
}