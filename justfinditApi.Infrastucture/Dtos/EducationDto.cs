namespace justfinditApi.Infrastucture.Dtos
{
    public class EducationDto
    {
        public string EducationId { get; set; }
        public string SchoolName { get; set; }
        public string StartYear { get; set; }
        public string EndYear  { get; set; }
        public string BranchOfStudy { get; set; }
        public string Speciality { get; set; }
        public string Degree { get; set; }
    }
}