﻿namespace justfinditApi.Infrastucture.Dtos
{
    public class RegisterEmployeeDto
    {
        public string About { get; set; }
        
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string BirthDate { get; set; }

        public string Sex { get; set; }

        public string Phone { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string WebsiteUrl { get; set; }
    }
}
