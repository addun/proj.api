﻿namespace justfinditApi.Infrastucture.Dtos
{
    public class ForgotPasswordDto
    {
        public string Email { get; set; }
    }
}
