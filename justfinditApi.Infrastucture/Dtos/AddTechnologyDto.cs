using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddTechnologyDto
    {
        public Guid UserId { get; set; }
        public TechnologyDto Technology { get; set; }
    }
}