using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployerDataDto
    {
        public Guid UserId { get; set; }
        public EmployerDataDto EmployerData { get; set; }
    }
}