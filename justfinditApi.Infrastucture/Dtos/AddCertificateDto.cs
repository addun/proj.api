using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddCertificateDto
    {
        public Guid UserId { get; set; }
        public CourseDto Course { get; set; }
    }
}