using System.Collections.Generic;

namespace justfinditApi.Infrastucture.Dtos
{
    public class JobOfferDto
    {
        public string OfferId { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string CreationDate { get; set; }
        public string SalaryFrom { get; set; }
        public string SalaryTo { get; set; }
        public string Currency { get; set; }
        public string InvoiceType { get; set; }
        public string WorkTime { get; set; }
        public string ExperienceLevel { get; set; }
        public string OfferDescription { get; set; }
        public string Remote { get; set; }
        public List<string> Skills { get; set; } = new List<string>();
        public JobOfferEmployerDto Employer { get; set; }
    }
}