using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployeePersonalDataDto
    {
        public Guid UserId { get; set; }
        public EmployeePersonalDataDto EmployeePersonalData { get; set; }
    }
}