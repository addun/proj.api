﻿using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class ConfirmEmailDto
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
    }
}
