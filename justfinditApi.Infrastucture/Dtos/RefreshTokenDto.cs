namespace justfinditApi.Infrastucture.Dtos
{
    public class RefreshTokenDto
    {
        public string RefreshToken { get; set; }
        public string AuthToken { get; set; }
    }
}