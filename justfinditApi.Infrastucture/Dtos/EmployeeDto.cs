﻿using System.Collections.Generic;
using System.Security;
using justfinditApi.Core.Domain;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EmployeeDto
    {
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string About { get; set; }
        public string BirthDate { get; set; }
        public string Sex { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string WebsiteUrl { get; set; }
        public IEnumerable<ProfessionalExperienceDto> JobExperience { get; set; }
        public IEnumerable<EducationDto> EmployeeEducation { get; set; }
        public IEnumerable<CourseDto> EmployeeCertificates { get; set; }
        public IEnumerable<LanguageDto> EmployeeLanguages { get; set; }
        public IEnumerable<SkillDto> EmployeeSkills { get; set; }
        public IEnumerable<JobOfferDto> Applications { get; set; }
        public IEnumerable<JobOfferDto> Favourites { get; set; }
    }
}
