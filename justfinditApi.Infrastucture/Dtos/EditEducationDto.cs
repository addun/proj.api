using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEducationDto
    {
        public Guid UserId { get; set; }
        public EducationDto Education { get; set; }
    }
}