using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddProfessionalExperienceDto
    {
        public Guid UserId { get; set; }
        public ProfessionalExperienceDto ProfessionalExperience { get; set; }
    }
}