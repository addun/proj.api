using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class SetHeadquartersDto
    {
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
    }
}