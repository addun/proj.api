using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployerLocationDto
    {
        public Guid UserId { get; set; }
        public EmployerLocationDto Location { get; set; }
    }
}