namespace justfinditApi.Infrastucture.Dtos
{
    public class JobOfferEmployerDto
    {
        public string EmployerId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
        public string CompanySize { get; set; }
        public string FoundedYear { get; set; }
        public string WebsiteUrl { get; set; }
    }
}