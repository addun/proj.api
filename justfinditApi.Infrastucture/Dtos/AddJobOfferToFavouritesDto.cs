namespace justfinditApi.Infrastucture.Dtos
{
    public class AddJobOfferToFavouritesDto
    {
        public string UserId { get; set; }
        public string OfferId { get; set; }
    }
}