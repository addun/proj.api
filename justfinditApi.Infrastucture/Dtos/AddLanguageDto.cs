using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddLanguageDto
    {
        public Guid UserId { get; set; }
        public LanguageDto Language { get; set; }
    }
}