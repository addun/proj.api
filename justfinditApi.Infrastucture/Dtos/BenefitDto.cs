using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class BenefitDto
    {
        public string BenefitId { get; set; }
        public string Title { get; set; } 
        public string Description { get; set; } 
    }
}