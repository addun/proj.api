using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployeeSkillDto
    {
        public Guid UserId { get; set; }
        public Guid SkillId { get; set; }
        public int Level { get; set; }
    }
}