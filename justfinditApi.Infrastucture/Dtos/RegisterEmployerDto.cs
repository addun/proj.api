﻿namespace justfinditApi.Infrastucture.Dtos
{
    public class RegisterEmployerDto
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string CompanyName { get; set; }

        public string CompanyDescription { get; set; }

        public string FoundedYear { get; set; }

        public string CompanySize { get; set; }

        public string Nip { get; set; }

        public string WebsiteUrl { get; set; }

        public string Street { get; set; }

        public string ApartmentNumber { get; set; }

        public string FlatNumber { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }
}
