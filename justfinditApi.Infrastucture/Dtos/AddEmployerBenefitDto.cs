using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddEmployerBenefitDto
    {
        public Guid UserId { get; set; }
        public BenefitDto Benefit { get; set; }
    }
}