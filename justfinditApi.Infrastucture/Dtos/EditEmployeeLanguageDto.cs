using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditEmployeeLanguageDto
    {
        public Guid UserId { get; set; }
        public Guid LanguageId { get; set; }
        public int Level { get; set; }
    }
}