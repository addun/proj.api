using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddJobOfferDto
    {
        public Guid UserId { get; set; }
        public JobOfferDto JobOffer { get; set; }
    }
}