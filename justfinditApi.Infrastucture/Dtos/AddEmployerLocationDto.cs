using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class AddEmployerLocationDto
    {
        public Guid UserId { get; set; }
        public EmployerLocationDto Location { get; set; }
    }
}