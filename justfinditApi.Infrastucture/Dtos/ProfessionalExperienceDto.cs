namespace justfinditApi.Infrastucture.Dtos
{
    public class ProfessionalExperienceDto
    {
        public string ProfessionalExperienceId { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string StartMonth { get; set; }
        public string EndMonth { get; set; }
        public string StartYear { get; set; }
        public string EndYear { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string JobDescription { get; set; }
    }
}