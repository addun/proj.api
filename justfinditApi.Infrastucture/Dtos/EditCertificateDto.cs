using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class EditCertificateDto
    {
        public Guid UserId { get; set; }
        public CourseDto Course { get; set; }
    }
}