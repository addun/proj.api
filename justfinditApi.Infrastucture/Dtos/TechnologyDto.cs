namespace justfinditApi.Infrastucture.Dtos
{
    public class TechnologyDto
    {
        public string TechnologyId { get; set; }
        public string TechnologyName { get; set; }
    }
}