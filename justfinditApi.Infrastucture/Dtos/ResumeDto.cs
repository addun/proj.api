namespace justfinditApi.Infrastucture.Dtos
{
    public class ResumeDto
    {
        public string ContentType { get; set; }
        public string ContentTypeShort { get; set; }
        public string FileName { get; set; }
        public byte[] Content { get; set; }
    }
}