using System.Collections.Generic;

namespace justfinditApi.Infrastucture.Dtos
{
    // FOR DISPLAYING COMPANY PROFILE (FOR EMPLOYEES PURPOSES)
    public class EmployerProfile
    {
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }    
        public string CompanyName { get; set; }
        public string CompanyDescription { get; set; }
        public string CompanySize { get; set; }
        public string FoundedYear { get; set; }
        public string Nip { get; set; }
        public string WebsiteUrl { get; set; }
        public string WhatsWeCreatingDescription { get; set; }    
        public string WorkingWithUsDescription { get; set; }    
        public string InternshipDescription { get; set; } 
        public string HeadquartersId { get; set; }
        public IEnumerable<BenefitDto> Benefits { get; set; } 
        public IEnumerable<EmployerLocationDto> Locations { get; set; } 
        public IEnumerable<TechnologyDto> Technologies { get; set; } 
        public IEnumerable<JobOfferDto> JobOffers { get; set; }  
    }
}