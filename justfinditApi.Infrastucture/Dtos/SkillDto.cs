using System;

namespace justfinditApi.Infrastucture.Dtos
{
    public class SkillDto
    {
        public string SkillId { get; set; }
        public string SkillName { get; set; }
        public int Level { get; set; }
    }
}