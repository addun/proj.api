namespace justfinditApi.Infrastucture.Dtos
{
    public class CourseDto
    {
        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public string MonthOfObtain { get; set; }
        public string YearOfObtain { get; set; }
        public string OrganizerName { get; set; }
    }
}