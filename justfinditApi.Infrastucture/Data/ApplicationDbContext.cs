﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using justfinditApi.Core.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;


//dotnet ef --startup-project ../justfinditApi.Api/ migrations add NAME_OF_MIGRATION
//dotnet ef --startup-project ../justfinditApi.Api/ database update
namespace justfinditApi.Infrastucture.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
    {
        protected ApplicationDbContext()
        {}
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
                    : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Employer> Employers { get; set; }
        public DbSet<JobOffer> JobOffers { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Technology> Technologies { get; set; }
        

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().ToTable("Users");
            builder.Entity<Role>().ToTable("Roles");
            builder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles");
            builder.Entity<IdentityUserClaim<Guid>>().ToTable("UserClaims");
            builder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogins");
            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("RoleClaims");
            builder.Entity<IdentityUserToken<Guid>>().ToTable("UserTokens");

            builder.Entity<JobOfferTechnology>().HasKey(k => new {k.JobOfferId, k.TechnologyId});

            builder.Entity<JobOfferTechnology>()
                .HasOne(et => et.JobOffer)
                .WithMany(e => e.Skills)
                .HasForeignKey(et => et.JobOfferId);

            builder.Entity<JobOfferTechnology>()
                .HasOne(et => et.Technology)
                .WithMany(t => t.JobOfferTechnologies)
                .HasForeignKey(et => et.TechnologyId);
            
            builder.Entity<EmployerTechnology>().HasKey(k => new {k.EmployerId, k.TechnologyId});

            builder.Entity<EmployerTechnology>()
                .HasOne(et => et.Employer)
                .WithMany(e => e.Technologies)
                .HasForeignKey(et => et.EmployerId);

            builder.Entity<EmployerTechnology>()
                .HasOne(et => et.Technology)
                .WithMany(t => t.EmployerTechnologies)
                .HasForeignKey(et => et.TechnologyId);

            builder.Entity<Employer>()
                .HasMany(et => et.Locations)
                .WithOne(t => t.Employer)
                .HasForeignKey(pe => pe.EmployerId);
            builder.Entity<EmployerLocation>()
                .HasKey(k => new {CompanyLocationId = k.EmployerLocationId, k.EmployerId});
            
            builder.Entity<Employer>()
                .HasMany(et => et.Benefits)
                .WithOne(t => t.Employer)
                .HasForeignKey(pe => pe.EmployerId);
            builder.Entity<Benefit>()
                .HasKey(k => new {k.BenefitId, k.EmployerId});
            
            builder.Entity<Employer>()
                .HasMany(et => et.JobOffers)
                .WithOne(t => t.Employer)
                .HasForeignKey(pe => pe.EmployerId);
            builder.Entity<JobOffer>()
                .HasKey(k => new { k.OfferId });

            builder.Entity<Employee>()
                .HasMany(et => et.ProfessionalExperience)
                .WithOne(t => t.Employee)
                .HasForeignKey(pe => pe.EmployeeId);
            builder.Entity<ProfessionalExperience>()
                .HasKey(k => new {k.ProfessionalExperienceId, k.EmployeeId});
            builder.Entity<ProfessionalExperience>()
                .Property(p => p.ProfessionalExperienceId)
                .ValueGeneratedOnAdd();

            builder.Entity<Employee>()
                .HasMany(et => et.Education)
                .WithOne(t => t.Employee)
                .HasForeignKey(ed => ed.EmployeeId);
            builder.Entity<Education>()
                .HasKey(k => new {k.EducationId, k.EmployeeId});
            builder.Entity<Education>()
                .Property(p => p.EducationId)
                .ValueGeneratedOnAdd();

            builder.Entity<Employee>()
                .HasMany(et => et.Courses)
                .WithOne(t => t.Employee)
                .HasForeignKey(c => c.EmployeeId);
            builder.Entity<Course>()
                .HasKey(k => new {k.CourseId, k.EmployeeId});
            builder.Entity<Course>()
                .Property(p => p.CourseId)
                .ValueGeneratedOnAdd();

            builder.Entity<EmployeeLanguage>()
                .HasKey(el => new {el.EmployeeId, el.LanguageId});
            
            builder.Entity<EmployeeSkill>()
                .HasKey(es => new {es.EmployeeId, es.TechnologyId});

            builder.Entity<EmployeeLanguage>()
                .HasOne(el => el.Employee)
                .WithMany(e => e.EmployeeLanguages)
                .HasForeignKey(el => el.EmployeeId);
            
            builder.Entity<EmployeeLanguage>()
                .HasOne(el => el.Language)
                .WithMany(e => e.EmployeeLanguages)
                .HasForeignKey(el => el.LanguageId);
            
            builder.Entity<EmployeeSkill>()
                .HasOne(el => el.Employee)
                .WithMany(e => e.EmployeeSkills)
                .HasForeignKey(el => el.EmployeeId);
            
            builder.Entity<EmployeeSkill>()
                .HasOne(el => el.Technology)
                .WithMany(e => e.EmployeeSkills)
                .HasForeignKey(el => el.TechnologyId);
            
            builder.Entity<JobApplication>()
                .HasKey(es => new {es.EmployeeId, es.JobOfferId});

            builder.Entity<JobApplication>()
                .HasOne(el => el.JobOffer)
                .WithMany(e => e.JobApplication)
                .HasForeignKey(el => el.JobOfferId);
            
            builder.Entity<JobApplication>()
                .HasOne(el => el.Employee)
                .WithMany(e => e.JobApplications)
                .HasForeignKey(el => el.EmployeeId);
                   
            builder.Entity<EmployeeFavouriteJobOffer>()
                .HasKey(pc => new { pc.EmployeeId, pc.JobOfferId });

            builder.Entity<EmployeeFavouriteJobOffer>()
                .HasOne(pc => pc.Employee)
                .WithMany(p => p.Favourites)
                .HasForeignKey(pc => pc.EmployeeId);

            builder.Entity<EmployeeFavouriteJobOffer>()
                .HasOne(pc => pc.JobOffer)
                .WithMany(c => c.Favourites)
                .HasForeignKey(pc => pc.JobOfferId);
        }
    }
}
