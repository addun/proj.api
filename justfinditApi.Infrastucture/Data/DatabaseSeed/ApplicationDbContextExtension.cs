using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using justfinditApi.Core.ConstantValues;
using justfinditApi.Core.Domain;
using justfinditApi.Core.DomainServices;
using justfinditApi.Infrastucture.Configurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;


namespace justfinditApi.Infrastucture.Data.DatabaseSeed
{
    public static class ApplicationDbContextExtension
    {
        public static void PopulateDatabase(this ApplicationDbContext context, IServiceScope serviceScope)
        {
            try
            {
                var userService = serviceScope.ServiceProvider.GetService<IUserService>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();

                context.Database.Migrate();

                PopulateLanguages(context);
                PopulateTechnologies(context);
                PopulateRoles(context, roleManager);
                PopulateEmployeesWithData(context, userManager, userService).Wait();
                PopulateEmployersWithData(context, userManager, userService).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private static void PopulateTechnologies(ApplicationDbContext context)
        {
            Debug.WriteLine("============= Adding Technologies to database");
            if (context.Technologies.Any())
            {
                Debug.WriteLine("============= Technologies existing in database. Finishing.");
                return;
            }

            // CREATE INITIAL TECHNOLOGIES
            var technologies = new List<Technology>()
            {
                Technology.Create("Android"),
                Technology.Create("Angular"),
                Technology.Create("AWS"),
                Technology.Create("ASP.NET"),
                Technology.Create("ASP.NET CORE"),
                Technology.Create("Azure"),
                Technology.Create("BI"),
                Technology.Create("Big Data"),
                Technology.Create("Bootstrap"),
                Technology.Create("C#"),
                Technology.Create("C++"),
                Technology.Create("CSS3"),
                Technology.Create("Dart"),
                Technology.Create("Docker"),
                Technology.Create("Elastic Search"),
                Technology.Create("Ember"),
                Technology.Create("Git"),
                Technology.Create("Github"),
                Technology.Create("Golang"),
                Technology.Create("Google Cloud"),
                Technology.Create("Hibernate"),
                Technology.Create("HTML5"),
                Technology.Create("IOS"),
                Technology.Create("Java"),
                Technology.Create("Jenkins"),
                Technology.Create("Jira"),
                Technology.Create("jQuery"),
                Technology.Create("Javascript"),
                Technology.Create("Kotlin"),
                Technology.Create("Kubernetes"),
                Technology.Create("Laravel"),
                Technology.Create("Linux"),
                Technology.Create("MongoDb"),
                Technology.Create("MS SQL"),
                Technology.Create("MySql"),
                Technology.Create(".NET"),
                Technology.Create("Nginx"),
                Technology.Create("NodeJs"),
                Technology.Create("Objective C"),
                Technology.Create("Oracle"),
                Technology.Create("PHP"),
                Technology.Create("Python"),
                Technology.Create("R"),
                Technology.Create("Rails"),
                Technology.Create("React"),
                Technology.Create("Ruby"),
                Technology.Create("Selenium"),
                Technology.Create("Spring"),
                Technology.Create("SQL"),
                Technology.Create("Swift"),
                Technology.Create("Symfony"),
                Technology.Create("Testing"),
                Technology.Create("Typescript"),
                Technology.Create("Vue"),
                Technology.Create("Windows"),
                Technology.Create("Wordpress"),
                Technology.Create("Xamarin"),
                Technology.Create("Zend"),
            };

            // SAVE TECHNOLOGIES TO DATABASE
            context.Technologies.AddRange(technologies);
            context.SaveChanges();
            Debug.WriteLine("============= Technologies added successfully");
        }

        private static void PopulateLanguages(ApplicationDbContext context)
        {
            Debug.WriteLine("============= Adding Languages to database");
            if (context.Languages.Any())
            {
                Debug.WriteLine("============= Technologies existing in database. Finishing.");
                return;
            }

            // CREATE INITIAL LANGUAGES
            var languages = new List<Language>()
            {
                Language.Create("Angielski"),
                Language.Create("Bia�oruski"),
                Language.Create("Bo�niacki"),
                Language.Create("Bu�garski"),
                Language.Create("Chi�ski"),
                Language.Create("Chorwacki"),
                Language.Create("Czeski"),
                Language.Create("Du�ski"),
                Language.Create("Esto�ski"),
                Language.Create("Fi�ski"),
                Language.Create("Francuski"),
                Language.Create("Grecki"),
                Language.Create("Hiszpa�ski"),
                Language.Create("Irlandzki"),
                Language.Create("Islandzki"),
                Language.Create("Japo�ski"),
                Language.Create("Korea�ski"),
                Language.Create("Litewski"),
                Language.Create("Luksembruski"),
                Language.Create("�otewski"),
                Language.Create("Macedo�ski"),
                Language.Create("Malta�ski"),
                Language.Create("Mo�dawski"),
                Language.Create("Niderlandzki"),
                Language.Create("Niemiecki"),
                Language.Create("Norweski"),
                Language.Create("Polski"),
                Language.Create("Portugalski"),
                Language.Create("Rosyjski"),
                Language.Create("Rumu�ski"),
                Language.Create("Serbski"),
                Language.Create("S�owacki"),
                Language.Create("S�owe�ski"),
                Language.Create("Szwedzki"),
                Language.Create("Tajski"),
                Language.Create("Turecki"),
                Language.Create("Ukrai�ski"),
                Language.Create("Walijski"),
                Language.Create("W�gierski"),
                Language.Create("W�oski"),
            };

            // SAVE LANGUAGES TO DATABASE
            context.Languages.AddRange(languages);
            context.SaveChanges();
            Debug.WriteLine("============= Languages added successfully");
        }

        private static async Task PopulateEmployeesWithData(ApplicationDbContext context, UserManager<User> userManager,
            IUserService userService)
        {
            Debug.WriteLine("============= Adding Employees with sample data to database");
            if (context.Employees.Any())
            {
                Debug.WriteLine("============= Employees existing in database. Finishing.");
                return;
            }

            Debug.WriteLine("============= Preparing sample data");
            // CREATE EMPLOYEES COLLECTION TO AUTOMATE ASSOCIATIONS 
            // WITH EDUCATION, JOB EXPERIENCE OBJECTS ETC.
            var employeesToPopulate = new List<Employee>();

            // CREATE SOME USERS TO PASS LATER TO EMPLOYEE OBJECTS
            var user1 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "jkowalski@test.com",
                UserName = "jkowalski@test.com",
            };

            var user2 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "tnowak@test.com",
                UserName = "tnowak@test.com",
            };

            var user3 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "mtarlaga@test.com",
                UserName = "mtarlaga@test.com",
            };

            // NOW LETS CREATING EMPLOYEES OBJECTS AND ADD TO EMPLOYEE COLLECTION
            var employee1 = new Employee("Jan", "Kowalski", ApplicationGenderValues.MAN, "21-01-1986",
                @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora
                incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                "556334234", "jkowalski.com", "Polska", "Warszawa", user1);
            employeesToPopulate.Add(employee1);

            var employee2 = new Employee("Tomasz", "Nowak", ApplicationGenderValues.MAN, "7-04-1985",
                @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora
                incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                "123887655", "tnowak.com", "Polska", "Wroc�aw", user2);
            employeesToPopulate.Add(employee2);

            var employee3 = new Employee("Marcin", "Tarlaga", ApplicationGenderValues.MAN, "21-01-1995",
                @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis
                et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui
                ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora
                incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
                "321556789", "mtarlaga.com", "Polska", "Krak�w", user3);
            employeesToPopulate.Add(employee3);

            // CREATING LIST LANGUAGES THAT WILL BE ASSOCIATED WITH EMPLOYEE OBJECT
            var languages = new List<Language>()
            {
                Language.Create("Angielski"),
                Language.Create("Niemiecki"),
                Language.Create("Francuski"),
                Language.Create("W�oski"),
                Language.Create("Hiszpa�ski"),
                Language.Create("Szwedzki"),
            };

            // CREATING LIST LANGUAGES THAT WILL BE ASSOCIATED WITH EMPLOYEE OBJECT
            // SIMPLY GET THAT ONES WHICH ARE CURRENTLY IN DATABASE
            var technologies = context.Technologies.ToList();

            // CREATE RANDOM OBJECT TO RANDOMIZE PROCESS OF ASSOCIATE OBJECTS WITH EMPLOYEE
            var rng = new Random();

            // ADD INITIAL COUNT OF EMPLOYEE COURSES LANGUAGES AND SKILLS
            var coursesToAddCount = 2;
            var languagesToAddCount = 3;
            var skillsToAddCount = 8;

            //CREATE TRANSACTION TO ENSURE ALL OBJECT WILL BE POPULATED
            context.Database.BeginTransaction();

            // MAIN LOOP OF ADDING ASSOCIATION WITH EMPLOYEE OBJECT
            for (var empIndex = 0; empIndex < employeesToPopulate.Count; empIndex++)
            {
                // CREATING LIST OF PROFESSIONAL EXPERIENCES THAT WILL BE ASSOCIATED WITH EMPLOYEE
                // OBJECT. OBJECTS (THIS ONE AND BELOW) ARE CREATING IN LOOP TO ENSURE UNIQUE OBJECT
                // IS ASSOCIATE TO EACH EMPLOYEE OBJECT (EACH EMPLOYEE HAS HIS OWN OBJECTS. NO OBJECTS
                // ARE SHARED) FIRST POOL IS CREATING TO ENSURE THAT DATES OF EMPLOYMENT FIT WITH SECOND
                // POOL IN TERMS OF DATES INTEGRITY. FIRST POOL CONTAINS DATES TO 12.2016
                var professionalExperienceFirstPool = new List<ProfessionalExperience>()
                {
                    ProfessionalExperience.Create(".NET Junior Developer", "Comarch", "11", "2015", "12",
                        "2016", "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                    ProfessionalExperience.Create("Java Junior Developer", "Sabre", "11", "2014", "10",
                        "2016", "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                    ProfessionalExperience.Create("Angular Junior Developer", "Pragmatic Coders", "11", "2015", "12",
                        "2016", "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                };

                // SECOND POOL CONTAINS DATES FROM 01.2017
                var professionalExperienceSecondPool = new List<ProfessionalExperience>()
                {
                    ProfessionalExperience.Create("Angular Developer", "ABB", "1", "2017", null,
                        null, "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                    ProfessionalExperience.Create("Java Developer", "Sabre", "7", "2017", null,
                        null, "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                    ProfessionalExperience.Create(".NET Developer", "Making Waves", "12", "2017", null,
                        null, "Polska", "Krak�w",
                        @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantiumdoloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatiset quasi."),
                };

                // CREATING LIST OF EDUCATIONS THAT WILL BE ASSOCIATED WITH EMPLOYEE
                // FIRST POOL IS CREATING TO ENSURE THAT DATES OF EDUCATION FIT WITH SECOND POOL
                // IN TERMS OF DATES INTEGRITY. FIRST POOL CONTAINS DATES TO 2014
                var educationHistoryFirstPool = new List<Education>()
                {
                    Education.Create("III LO Krak�w", "2011", "2014", "Matematyka", "Nie dotyczy"),
                    Education.Create("Technikum Informatyczne Wroc�aw", "2010", "2014", "Informatyka", "Nie dotyczy",
                        "Aplikacje interntetowe"),
                    Education.Create("V Liceum Warszawa", "2010", "2013", "Informatyka", "Nie dotyczy"),
                };

                //SECOND POOL CONTAINS DATES FROM 2014
                var educationHistorySecondPool = new List<Education>()
                {
                    Education.Create("AGH", "2015", null, "Informatyka", "In�ynier", "Aplikacje internetowe"),
                    Education.Create("Politechnika Warszawska", "2016", null, "Informatyka", "In�ynier", "Bazy Danych"),
                    Education.Create("Politechnika Krakowska", "2014", "2017", "Informatyka", "Licencjat",
                        "Grafika komputerowa"),
                };

                // CREATING LIST OF EDUCATIONS THAT WILL BE ASSOCIATED WITH EMPLOYEE
                var courses = new List<Course>()
                {
                    Course.Create("Java Developer Course", "Oracle", "11", "2017"),
                    Course.Create("Spring Developer", "Pivotal", "12", "2016"),
                    Course.Create("Oracle Database Course", "Oracle", "1", "2016"),
                    Course.Create(".NET Developer", "Microsoft", "7", "2016"),
                    Course.Create("Microsoft Web Developer", "Microsoft", "7", "2018"),
                };

                // ASSIGN CURRENTLY ITERATED EMPLOYEE TO VARIABLE 
                var employee = employeesToPopulate[empIndex];

                // CREATED USER, GENERATE COINFORMATION TOKEN FOR USER AND ADD TO EMPLOYEE ROLE
                await userManager.CreateAsync(employee.User, "Zaq1@wsx");
                await userService.GenerateEmailConfirmationTokenAsync(employee.User);
                await userService.AddUserToRoleAsync(employee.User, ApplicationRoles.Employee);
                context.SaveChanges();

                //RANDOMLY ADD EDUCATION FROM FIRST POOL AND THEN  FROM SECOND POOL
                employee.AddEducation(educationHistoryFirstPool[rng.Next(0, educationHistoryFirstPool.Count - 1)]);
                employee.AddEducation(educationHistorySecondPool[rng.Next(0, educationHistorySecondPool.Count - 1)]);

                //RANDOMLY ADD PROFESSIONAL EXPERIENCE FROM FIRST POOL AND THEN FROM SECOND POOL
                employee.AddProfessionalExperience(
                    professionalExperienceFirstPool[rng.Next(0, professionalExperienceFirstPool.Count - 1)]);
                employee.AddProfessionalExperience(
                    professionalExperienceSecondPool[rng.Next(0, professionalExperienceSecondPool.Count - 1)]);

                //ADDING CURSES TO EMPLOYEE IN LOOP TO ENSURE THAT ADDED COURSES WILL DIFFER
                var usedIndexes = new List<int>();
                for (var i = 0; i < coursesToAddCount; i++)
                {
                    var index = rng.Next(0, courses.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employee.AddCourse(courses[index]);
                }

                //ADDING LANGUAGES TO EMPLOYEE IN LOOP TO ENSURE THAT LANGUAGES WILL DIFFER
                usedIndexes = new List<int>();
                for (var i = 0; i < languagesToAddCount; i++)
                {
                    var index = rng.Next(0, languages.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employee.AddLanguage(languages[index], rng.Next(1, 5));
                }

                //ADDING SKILLS TO EMPLOYEE IN LOOP TO ENSURE THAT SKILLS WILL DIFFER
                usedIndexes = new List<int>();
                for (var i = 0; i < skillsToAddCount; i++)
                {
                    var index = rng.Next(0, technologies.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employee.AddSkill(technologies[index], rng.Next(1, 5));
                }

            }
            //SAVE EMPLOYEES WITH ASSOCIATIONS TO DATABASE AND FINISH TRNSACTION
            
            Debug.WriteLine("============= Start populating database");
            await context.Employees.AddRangeAsync(employeesToPopulate);
            context.SaveChanges();
            context.Database.CommitTransaction();
            Debug.WriteLine("============= Employees with sample data added successfully to database");
        }

        private static async Task PopulateEmployersWithData(ApplicationDbContext context, UserManager<User> userManager,
            IUserService userService)
        {
            Debug.WriteLine("============= Adding Employers with sample data to database");

            if (context.Employers.Any())
            {
                Debug.WriteLine("============= Employers existing in database. Finishing.");
                return;
            }

            Debug.WriteLine("============= Preparing sample data");
            // CREATE EMPLOYERS COLLECTION TO AUTOMATE ASSOCIATIONS 
            // WITH LOCATIONS, JOB OFFERS OBJECTS ETC.
            var employersToPopulate = new List<Employer>();

            // CREATE SOME USERS TO PASS LATER TO EMPLOYER OBJECTS
            var user1 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "capgemini@test.com",
                UserName = "capgemini@test.com",
            };

            var user2 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "accenture@test.com",
                UserName = "accenture@test.com",
            };

            var user3 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "abb@test.com",
                UserName = "abb@test.com",
            };

            var user4 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "aon@test.com",
                UserName = "aon@test.com",
            };

            var user5 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "aptiv@test.com",
                UserName = "aptiv@test.com",
            };

            var user6 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "bbh@test.com",
                UserName = "bbh@test.com",
            };

            var user7 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "comarch@test.com",
                UserName = "comarch@test.com",
            };

            var user8 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "kotrak@test.com",
                UserName = "kotrak@test.com",
            };

            var user9 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "luxoft@test.com",
                UserName = "luxoft@test.com",
            };

            var user10 = new User()
            {
                Id = Guid.NewGuid(),
                Email = "motorola@test.com",
                UserName = "motorola@test.com",
            };

            // NOW CREATING EMPLOYERS OBJECTS AND ADD TO EMPLOYER COLLECTION
            var employer1 = new Employer("Capgemini Polska Sp. z o.o.",
                @"Firma Capgemini w Polsce zatrudnia obecnie ponad 7000 specjalist�w i jest jednym z
                najwi�kszych zagranicznych inwestor�w z sektora nowoczesnych us�ug dla biznesu w kraju. Obszarami
                specjalizacji Capgemini w Polsce s� projekty transformacji przedsi�biorstw w oparciu o technologie
                informatyczne, outsourcing proces�w biznesowych, us�ugi w zakresie Infrastruktury IT, oraz rozw�j
                oprogramowania spe�niaj�cego indywidualne potrzeby biznesu. Capgemini jest wielokulturow� organizacj�
                z w�asnym sposobem pracy, jakim jest Wsp�dzia�anie w Biznesie (the Collaborative Business Experience�)
                i globalnym modelem dostarczania us�ug Rightshore�. W tym roku Grupa Capgemini obchodzi
                swoje 50. lecie, a Capgemini w Polsce ko�czy 21 lat.",
                "1967", "2000", "5674456435", "capgemini.com",
                EmployerLocation.Create("Polska", "Krak�w", "Starowi�lna", "31-056", "2"), user1);
            employersToPopulate.Add(employer1);

            var employer2 = new Employer("Accenture Sp.z o.o.",
                @"Jeste�my ekspertami realizuj�cymi projekty, kt�rych nikt nie jest w stanie
                zrobi� albo nie jest w stanie zrobi� r�wnie dobrze jak my. W Accenture odpowiadamy na
                prawdziwe wyzwania stoj�ce przed naszymi klientami dzi�ki innowacji,
                najnowocze�niejszej technologii i dog��bnej znajomo�ci rynku. Dzia�amy w 5
                obszarach � Strategy, Consulting, Digital, Technology oraz Operations.
                Dzi�ki temu, nawet najbardziej wymagaj�cy klient podniesie efektywno��
                i jako�� swoich dzia�a�. Nasz� najwi�ksz� satysfakcj� jest widzie� dooko�a
                rozwi�zania Accenture, kt�re nap�dzaj� nowoczesny biznes. Nowoczesno�� to
                nie jest temat jutra � my j� ju� dzi� wdra�amy w �ycie.
                Do��czaj�c do nas, uczysz si� nowych rzeczy i sprawdzasz je w dzia�aniu razem z 
                do�wiadczonymi mentorami. Ju� od pierwszego dnia do�wiadczasz czego� nowego. 
                Tu zdobywasz wiedz� i umiej�tno�ci, kt�re zostan� z Tob� na lata. Twoja kariera staje
                si� gotowa na wszystkie zmiany rynku � przecie� to Ty, razem z nami, ju� dzi� tworzysz jutro biznesu.",
                "1980", "1000", "5782411589", "accenture.com",
                EmployerLocation.Create("Polska", "Warszawa", "Sienna", "00-121", "39"), user2);
            employersToPopulate.Add(employer2);

            var employer3 = new Employer("ABB Sp. z o.o.",
                @"ABB jest globalnym liderem kszta�tuj�cym przysz�o�� w dziedzinie cyfryzacji przemys�u. Ka�dego
                dnia pracownicy ABB na ca�ym �wiecie pracuj� nad rozwi�zaniem najwi�kszych problem�w 
                wsp�czesnych czas�w, takich jak dostarczanie ekologicznej energii, zwi�kszanie
                wydajno�ci i wprowadzanie strategii zr�wnowa�onego rozwoju z jednoczesn� trosk� o 
                minimalizowanie negatywnego wp�ywu na �rodowisko naturalne. Grupa ABB zatrudnia oko�o
                136 000 pracownik�w w ponad 100 krajach �wiata, a jej do�wiadczenie bazuje na ponad 
                125 latach dzia�alno�ci. ",
                "1940", "3400", "7316492877", "abb.com",
                EmployerLocation.Create("Polska", "Krak�w", "Przy Rondzie", "31-547", "4"), user3);
            employersToPopulate.Add(employer3);

            var employer4 = new Employer("Aon Sp. z o.o.",
                @"You may know Aon through our association with Manchester United. You may have seen
                us in the news. You may not know that we�re one of the world�s most successful
                professional services companies, helping the biggest names in business plan for
                every eventuality. From economic upheaval to political crises to natural disasters
                and plenty more besides, the world is full of surprises. Our job is to help our
                clients prepare for and manage those risks, through pensions; general insurance;
                insurance; reinsurance and risk management; investment consulting; health & benefits
                broking and consulting; captive solutions or human capital consulting. Our main offices
                in Poland are in Krakow and Warszawa.",
                "1964", "4500", "3467956648", "aon.com",
                EmployerLocation.Create("Polska", "Krak�w", "Al. Powsta�c�w Wielkopolskich", "30-707", "13"), user4);
            employersToPopulate.Add(employer4);

            var employer5 = new Employer("APTIV",
                @"Pierwszy system sterowania gestami dla motoryzacji? To byli�my my.
                Pierwszy pojazd autonomiczny, kt�ry samodzielnie przemierzy� Stany Zjednoczone? To te�
                zas�uga zespo�u Aptiv (wcze�niej Delphi). Od ponad dekady nasze Krakowskie Centrum
                Techniczne wspiera najwi�ksze marki samochodowe, ��cz�c bran�� motoryzacyjn� z
                nowoczesnymi technologiami. Dzi� mamy jeden cel � �wiat z ograniczon� liczb� wypadk�w
                drogowych. Do��cz do nas! ",
                "1967", "2000", "5674456435", "capgemini.com",
                EmployerLocation.Create("Polska", "Krak�w", "Powsta�c�w Wielkopolskich", "30-399", "13"), user5);
            employersToPopulate.Add(employer5);

            var employer6 = new Employer("BROWN BROTHERS HARRIMAN Sp. z o.o.",
                @"Join a place where your perspective, ideas and expertise really count.
                For nearly 200 years, Brown Brothers Harriman has provided expertise in Private Banking,
                Investment Management, and Investor Services. As a privately-owned and managed Firm,
                we are able to take a thoughtful, long-term approach to managing our business that 
                aligns with the needs of our clients, colleagues and communities. With 18 locations, 
                our 5,000 employees operate throughout North America, Europe and Asia. Our Krakow office
                is an important hub for a wide range of financial services, handling end-to-end processes
                and working directly with clients all over the world.",
                "1985", "2000", "7426985555", "bbh.com",
                EmployerLocation.Create("Polska", "Krak�w", "Klimeckiego", "30-705", "1"), user6);
            employersToPopulate.Add(employer6);

            var employer7 = new Employer("Comarch SA",
                @"Jeste�my jedn� z najbardziej licz�cych si� firm IT w naszej cz�ci Europy.
                Dostarczamy systemy i oprogramowanie dla tysi�cy klient�w na ca�ym �wiecie � naszym rozwi�zaniom 
                zaufa�o ponad 220 000 firm!
                Jest nas ju� ponad 5600, a w 2018 roku �wi�tujemy jubileusz 25-lecia firmy.",
                "1975", "1500", "1475683285", "comarch.com",
                EmployerLocation.Create("Polska", "Krak�w", "Al. Jana Paw�a II", "31-864", "39"), user7);
            employersToPopulate.Add(employer7);

            var employer8 = new Employer("KOTRAK S.A.",
                @"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia 
                voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores 
                eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem
                ipsum quia dolor sit amet, consectetur",
                "1992", "800", "5674456435", "kotrak.com",
                EmployerLocation.Create("Polska", "Katowice", "Bogus�awa Parczewskiego", "40-582", "25"), user8);
            employersToPopulate.Add(employer8);

            var employer9 = new Employer("Luxoft Poland Sp. z o.o.",
                @"Luxoft is a leading provider of software development services and innovative IT 
                solutions to a global client base consisting primarily of large multinational corporations.
                The Company develops its solutions and delivers its services from 32 dedicated delivery
                centers worldwide. It has over 12,000 employees across 39 offices in 22 countries located
                on 5 continents.",
                "1987", "4000", "6498888512", "luxoft.com",
                EmployerLocation.Create("Polska", "Zabierz�w", "Krakowska", "32-080", "280"), user9);
            employersToPopulate.Add(employer9);

            var employer10 = new Employer("Motorola Solutions",
                @"Jeste�my liderem w bran�y telekomunikacyjnej, powsta�ym w 1928 roku. Zatrudniamy ponad
                15000 pracownik�w w 65 krajach i dostarczamy produkty zar�wno klientom biznesowym, jak i
                instytucjom rz�dowym. G��wne obszary naszej dzia�alno�ci to obs�uga organ�w rz�dowych 
                odpowiedzialnych za bezpiecze�stwo publiczne oraz przedsi�biorstw komercyjnych. Z naszych
                produkt�w korzystaj� policjanci, stra�acy, wojsko, s�u�by specjalne, firmy transportowe 
                i energetyczne na ca�ym �wiecie. W Polsce Motorola Solutions posiada siedzib� w Krakowie i Warszawie.
                Nasze Centrum w Krakowie to jeden z czo�owych o�rodk�w Motoroli na �wiecie, gdzie obecnie pracuje
                ponad 1600 os�b, w tym 1100 in�ynier�w.",
                "1975", "3500", "7524441222", "motorola.com",
                EmployerLocation.Create("Polska", "Krak�w", "Czerwone Maki", "30-392", "82"), user10);
                employersToPopulate.Add(employer10);

            // CREATING LIST LANGUAGES THAT WILL BE ASSOCIATED WITH EMPLOYER OBJECT
            // SIMPLY GET THAT ONES WHICH ARE CURRENTLY IN DATABASE
            var technologies = context.Technologies.ToList();

            //DESCRIPTION GENERAL DUMMY TEXT
            var generalDescription = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est laborum.";

            // CREATE SAMPLE CITIES THAT WILL BE PART OF LOCATION OBJECT AND WILL BE RANDOMLY CHOOSEN
            var cities = new List<string>()
            {
                "Krak�w", "Warszawa", "Wroc�aw", "Gda�sk", "��d�", "Pozna�", "Rzesz�w", "Katowice", "Bia�ystok", "Szczecin"
            };

            // CREATE SAMPLE STREETS THAT WILL BE PART OF LOCATION OBJECT AND WILL BE RANDOMLY CHOOSEN
            var streets = new List<string>()
            {
                "Legion�w", "Solidarno�ci", "Aleje", "Armii Krajowej", "Browarna", "C. K. Norwida",
                "Orzeszkowej", "Szareckiego", "Andersa", "Tarnowskiego", "Krakowska", "Warszawska",
                "Kazimierza Wielkiego",
            };

            // CREATE SAMPLE POSTAL CODES THAT WILL BE PART OF LOCATION OBJECT AND WILL BE RANDOMLY CHOOSEN
            var postalCodes = new List<string>()
            {
                "34-056", "07-434", "45-566", "34-667", "23-567", "34-567", "34-654",
                "34-566", "22-456", "23-567", "23-567", "34-556", "33-445", "23-456"
            };

            // CREATE RANDOM OBJECT TO RANDOMIZE PROCESS OF ASSOCIATE OBJECTS WITH EMPLOYER
            var rng = new Random();

            // ADD INITIAL COUNT OF EMPLOYEE COURSES LANGUAGES AND SKILLS
            var locationsToAddCount = 2;
            var benefitsToAddCount = 4;
            var technologiesToAddCount = 8;
            // BEGIN TRANSACTION TO ENSURE ALL DATA WILL BE ADDED TO DATABASE
            context.Database.BeginTransaction();

            // MAIN LOOP OF ADDING ASSOCIATION WITH EMPLOYER OBJECT
            for (var empIndex = 0; empIndex < employersToPopulate.Count; empIndex++)
            {
                // CREATING LIST OF BENEFITS THAT WILL BE ASSOCIATED WITH EMPLOYER
                // OBJECT. OBJECTS (THIS ONE AND BELOW) ARE CREATING IN LOOP TO ENSURE UNIQUE OBJECT
                // IS ASSOCIATE TO EACH EMPLOYER OBJECT (EACH EMPLOYER HAS HIS OWN OBJECTS. NO OBJECTS
                // ARE SHARED)
                var benefits = new List<Benefit>()
                {
                    Benefit.Create("Global opportunities",
                        "We are leading global professional services firm providing a broad range of risk, retirement and health solutions in 60% of countries worldwide."),
                    Benefit.Create("Work life balance",
                        "We are a remote-friendly firm so colleagues may benefit from work-from-home option as well."),
                    Benefit.Create("Wellbeing awareness",
                        "We know that our wellbeing is a crucial success factor in exploiting our potential as a company and we care about it every day."),
                    Benefit.Create("Unique workplace culture",
                        "We create a great place to work with a friendly and supportive atmosphere. We value openness, honesty and authenticity, so don�t expect formal dress code nor managers hidden behind closed doors."),
                    Benefit.Create("Internal career opportunities",
                        "We value contributions made by our people and appreciate their hard work. For that reason our aim is to encourage more internal moves and promotions."),
                    Benefit.Create("Taking time off",
                        "You need time to play, relax, renew, and take care of yourself. We provide the following types of paid time off to help you achieve a work/life balance: vacation, illness or injury, personal business, holiday, family care, marriage, and bereavement."),
                    Benefit.Create("Flexibility at work",
                        "We provide you the flexibility and tools you need to be the most effective at getting your job done. Depending on job requirements, you are able enjoy our flexible work arrangements"),
                    Benefit.Create("Employee assistance program",
                        "The Employee Assistance Program (EAP) offers you and your family confidential support from licensed, professional counselors. The EAP staff is available to assist with issues relating to work and personal life."),
                    Benefit.Create("Car benefit",
                        "Wehicle leasing program for employees with full service support, mobility guarantee and a set of winter tires."),
                    Benefit.Create("Internal mobility program",
                        "Opportunity to change project and relocate to other Luxoft locations around 5 continents."),
                    Benefit.Create("Sport activities",
                        "Opportunity to join Luxoft Volleyball, Basketball, Football, CrossFit teams and relax during weekend bike or kayak tours."),
                    Benefit.Create("Cafeteria system",
                        "Multisport Card can be exchanged for benefit available in Cafeteria System, ex. cinema tickets, shopping cards, foreign trips."),
                    Benefit.Create("Special banking services offer",
                        "Opportunity to get special banking offers and discounts."),
                    Benefit.Create("Proffesional training centre",
                        "Possibility to take part in regular online and offline trainings (domain knowledge/ technical trainings/ soft-skills workshops) or becoming certified Internal Trainer and conducting employee�s own trainings."),
                };

                // CREATING LIST OF LOCATIONS TO CHOOSE FROM WHEN WE WILL CREATING ASSOCIATION WITH
                // EMPLOYER AND POPULATE USING CITIES STREETS AND POSTAL CODES COLLECTION CREATED ABOVE
                var locations = new List<EmployerLocation>();     
                for (var i = 0; i < 20; i++)
                {
                    var location = EmployerLocation.Create("Polska", cities[rng.Next(0, cities.Count - 1)],
                        streets[rng.Next(0, streets.Count - 1)], postalCodes[rng.Next(0, postalCodes.Count - 1)],
                        rng.Next(1, 345).ToString());
                    locations.Add(location);
                }

                // ASSING CURRENTLY ITERATING EMPLOYER TO VARIABLE
                var employer = employersToPopulate[empIndex];

                //CREATE USER, GENERATE COINFORMATION TOKEN AND ADD TO EMPLOYER ROLE
                await userManager.CreateAsync(employer.User, "Zaq1@wsx");
                await userService.GenerateEmailConfirmationTokenAsync(employer.User);
                await userService.AddUserToRoleAsync(employer.User, ApplicationRoles.Employer);
                context.SaveChanges();

                // ADD EMPLOYER PROFILE DESCRIPTION USING DUMMY DATA CREATED EALIER
                employer.EditInternshipDescription(generalDescription);
                employer.EditWhatWeCreatingDescription(generalDescription);
                employer.EditWhyWorthWorkingWithUs(generalDescription);

                // ADD LOCATIONS TO EMPLOYER WITH LOOP TO ENSURE UNIQUE LOCATIONS WILL BE ADDED
                var usedIndexes = new List<int>();
                for (var i = 0; i < locationsToAddCount; i++)
                {
                    var index = rng.Next(0, locations.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employer.AddLocation(locations[index]);
                }

                // ADD BENEFITS TO EMPLOYER WITH LOOP TO ENSURE UNIQUE BENEFITS WILL BE ADDED
                usedIndexes = new List<int>();
                for (var i = 0; i < benefitsToAddCount; i++)
                {
                    var index = rng.Next(0, benefits.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employer.AddBenefit(benefits[index]);
                }

                // AADD TECHNOLOGIES TO EMPLOYER WITH LOOP TO ENSURE UNIQUE TECHNOLOGIES WILL BE ADDED
                usedIndexes = new List<int>();
                for (var i = 0; i < technologiesToAddCount; i++)
                {
                    var index = rng.Next(0, technologies.Count - 1);
                    if (usedIndexes.Contains(index))
                    {
                        i--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    employer.AddTechnology(technologies[index]);
                }

                // ADD SAMPLE JOB OFFERS TO EMPLOYER
                CreateSampleJobOffersForEmployer(employer, technologies);
            }

            //SAVE EMPLOYErs WITH ASSOCIATIONS TO DATABASE AND FINISH TRNSACTION
            Debug.WriteLine("============= Start populating database");
            await context.Employers.AddRangeAsync(employersToPopulate);
            context.SaveChanges();
            context.Database.CommitTransaction();
            Debug.WriteLine("============= Employers with sample data added successfully to database");
        }

        private static void PopulateRoles(ApplicationDbContext context, RoleManager<Role> roleManager)
        {
            Debug.WriteLine("============= Adding Roles to database");
            if (context.Roles.Any())
            {
                Debug.WriteLine("============= Roles existing in database. Finishing.");
                return;
            }

            // CREATING INITIAL ROLES (EMPLOYEE ROLE AND EMPLOYER ROLE)
            var roles = new List<Role>()
            {
                new Role() {Name = ApplicationRoles.Employee},
                new Role() {Name = ApplicationRoles.Employer},
            };

            foreach (var role in roles)
            {
                // CREATING AND SAVING ROLE TO DATABASE
                var identityResult = roleManager.CreateAsync(role).Result;
                if (!identityResult.Succeeded)
                {
                    Debug.WriteLine("========= Error during populating roles: " + role.Name);
                }
            }

            Debug.WriteLine("============= Roles added to database");
        }

        // ADD TECHNOLOGIES PARAMETER TO REDUCE COUNT OF CALL DATABASE SINCE CALLER OF THIS METHOD 
        // HAS ALL TECHNOLOGIES LOADED
        private static void CreateSampleJobOffersForEmployer(Employer employer, List<Technology> technologies)
        {
            // CREATING INITIAL TECHNOLOGIES LIST THAT WILL BE USED LATER TO CREATE JOB OFFER TITLE
            // (TECHNOLOGY.NAME + DEVELOPER)
            var mainJobSkills = new List<Technology>()
            {
                Technology.Create("Android"),
                Technology.Create("Angular"),
                Technology.Create("ASP.NET"),
                Technology.Create("ASP.NET CORE"),
                Technology.Create("Azure"),
                Technology.Create("Big Data"),
                Technology.Create("C#"),
                Technology.Create("C++"),
                Technology.Create("Golang"),
                Technology.Create("IOS"),
                Technology.Create("Java"),
                Technology.Create("jQuery"),
                Technology.Create("Javascript"),
                Technology.Create("Kotlin"),
                Technology.Create("Laravel"),
                Technology.Create(".NET"),
                Technology.Create("NodeJs"),
                Technology.Create("Objective C"),
                Technology.Create("PHP"),
                Technology.Create("Python"),
                Technology.Create("R"),
                Technology.Create("React"),
                Technology.Create("Ruby"),
                Technology.Create("Spring"),
                Technology.Create("Swift"),
                Technology.Create("Symfony"),
                Technology.Create("Vue"),
                Technology.Create("Xamarin"),
                Technology.Create("Zend"),
            };

            // CREATE EXPERIENCE LEVELS VALUES LIST THAT WILL BE USED LATER TO CREATE JOB OFFER
            var experienceLevelsList = new List<string>()
            {
                ApplicationJobExperienceValues.ANY,
                ApplicationJobExperienceValues.JUNIOR,
                ApplicationJobExperienceValues.MID,
                ApplicationJobExperienceValues.SENIOR
            };

            // CREATE WORK TIME VALUES LIST THAT WILL BE USED LATER TO CREATE JOB OFFER
            var workTimeValues = new List<string>()
            {
                "Pe�ny etat",
                "1/2 etatu",
                "do uzgodnienia",
            };

            // CREATE INVOICE TYPE VALUES LIST THAT WILL BE USED LATER TO CREATE JOB OFFER
            // VALUES ARE REAPETING TO INCREASE CHANCE TO BEING CHOOSED BY RNG
            var invoiceTypeValues = new List<string>()
            {
                "Umowa o prac�",
                "Umowa o dzie�o",
                "Umowa zlecenie",
                "Umowa B2B",
                "Do uzgodnienia",
                "Do uzgodnienia",
                "Do uzgodnienia",
            };

            // CREATE REMOTE WORK VALUES LIST THAT WILL BE USED LATER TO CREATE JOB OFFER
            // VALUES ARE REAPETING TO INCREASE CHANCE TO BEING CHOOSED BY RNG
            var remoteWorkValues = new List<string>()
            {
                "Mo�liwa",
                "Mo�liwa ok 1/4 czasu",
                "Mo�liwa ok 1/3 czasu",
                "Niemo�liwa",
                "Do uzgodnienia",
                "Do uzgodnienia",
                "Do uzgodnienia"
            };


            var rng = new Random();
            
            // GENERATE RANDOM NUMBER OF JOBS TO GIVEN EMPLOYER
            var jobsOffersToAdd = rng.Next(3, 25);

            // MAIN LOOP OF CREATING JOB OFFERS
            for (var i = 0; i < jobsOffersToAdd; i++)
            {
                // GET RANDOM MAIN JOB SKILL TO CREATE JOB NAME AND ADD MAIN SKILL TO OFFER
                var mainTechnology = mainJobSkills[rng.Next(0, mainJobSkills.Count - 1)];

                // GET RANDOM EMPLOYER LOCATION TO POPULATE ADDRESS OF JOB OFFER
                var employerLocation = employer.Locations.ToList()[rng.Next(0, employer.Locations.Count - 1)];

                // GET RANDOM EXPERIENCE LEVEL TO POPULATE JOB OFFER
                var experienceLevel = experienceLevelsList[rng.Next(0, experienceLevelsList.Count - 1)];

                // CREATE SALARY FROM AND SALARY TO VARIABLES AND ASSING ITS VALUE BASE ON EXPERIENCE LEVEL
                // IN SWITCH STATEMENT
                var salaryFrom = 0;
                var salaryTo = 0;
                switch (experienceLevel)
                {
                    // USING MATH.ROUND TO ROUND RANDOM VALUES TO THOUSANDS
                    case ApplicationJobExperienceValues.ANY:
                    {
                        salaryFrom = (int)(Math.Round(rng.Next(3000, 8000) / 1000d) * 1000);
                        salaryTo = (int)(Math.Round(rng.Next(9000, 15000) / 1000d) * 1000);
                        break;
                    }
                    case ApplicationJobExperienceValues.JUNIOR:
                    {
                        salaryFrom = (int)(Math.Round(rng.Next(3000, 4000) / 1000d) * 1000);
                        salaryTo = (int)(Math.Round(rng.Next(5000, 7000) / 1000d) * 1000);
                        break;
                    }
                    case ApplicationJobExperienceValues.MID:
                    {
                        salaryFrom = (int)(Math.Round(rng.Next(5000, 7000) / 1000d) * 1000);
                        salaryTo = (int)(Math.Round(rng.Next(8000, 12000) / 1000d) * 1000);
                        break;
                    }
                    case ApplicationJobExperienceValues.SENIOR:
                    {
                        salaryFrom = (int)(Math.Round(rng.Next(8000, 10000) / 1000d) * 1000);
                        salaryTo = (int)(Math.Round(rng.Next(12000, 20000) / 1000d) * 1000);
                        break;
                    }
                }

                // CREATIG JOB OFFER USING SAMPLE DATA DEFINED ABOVE
                var jobOffer = JobOffer.Create(mainTechnology.TechnologyName + " " + "Developer", "Polska",
                    employerLocation.City, employerLocation.Street + " " + employerLocation.ApartmentNumber,
                    salaryFrom.ToString(), salaryTo.ToString(), ApplicationCurrencyValues.PLN,
                    workTimeValues[rng.Next(0, workTimeValues.Count - 1)],
                    invoiceTypeValues[rng.Next(0, invoiceTypeValues.Count - 1)], experienceLevel,
                    @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                    irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum. At vero eos et accusamus et iusto odio
                    dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti
                    quos dolores et quas molestias excepturi sint occaecati cupiditate non provident,
                    similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et
                    dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam
                    libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo
                    minus id quod maxime placeat facere possimus, omnis voluptas assumenda est,
                    omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis
                    aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et
                    molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus,
                    ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis
                    doloribus asperiores repellat",
                    remoteWorkValues[rng.Next(0, remoteWorkValues.Count - 1)]);

                //ADD MAIN TECHNOLOGY TO JOB OFFER SKILLS
                jobOffer.AddSkill(mainTechnology);

                // CREATE LIST THAT CONTAINS JOB OFFER TECHNOLOGIES OTHER THAN MAIN TECHNOLOGY
                var jobOfferOtherTechnologies = new List<Technology>();

                //ADD OTHER TECHNOLOGIES TO TEMPORARY TECHNOLOGIES LIST DECLARED ABOVE
                var technologiesToAddCount = rng.Next(5, 10);
                var usedIndexes = new List<int>();
                for (var j = 0; j < technologiesToAddCount; j++)
                {
                    var index = rng.Next(0, technologies.Count - 1);
                    var technologyName = technologies[index].TechnologyName;

                    // IF TECHNOLOGY IS MAIN TECHNOLOGY OR WAS ADDED BEFORE REPEAT LOOP ONE MORE TIME
                    if (usedIndexes.Contains(index) || technologyName == mainTechnology.TechnologyName)
                    {
                        j--;
                        continue;
                    }

                    usedIndexes.Add(index);

                    jobOfferOtherTechnologies.Add(technologies[index]);
                }

                //ADD OTHER TECHNOLOGIES TO JOB OFFER SKILLS
                jobOffer.AddSkills(jobOfferOtherTechnologies);

                // ASSOCIATE JOB OFFER WITH SKILLS TO GIVEN EMPLOYER
                employer.AddJobOffer(jobOffer);
            }
        }
    }
}