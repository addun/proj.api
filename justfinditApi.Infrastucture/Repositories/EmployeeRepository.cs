﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployeeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Employee> AddAsync(Employee employee)
        {
            await _context.Employees.AddAsync(employee);
            return employee;
        }

        public async Task<Employee> GetByEmailAsync(string email)
        {
            return await _context.Employees
                .Include(x => x.User)
                .Include(x=>x.Courses)
                .Include(x=>x.Education)
                .Include(x=>x.EmployeeLanguages)
                    .ThenInclude(x=>x.Language)
                .Include(x=>x.EmployeeSkills)
                    .ThenInclude(x=>x.Technology)
                .Include(x=>x.ProfessionalExperience)
                .Include(x=>x.JobApplications)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Employer)
                .Include(x=>x.JobApplications)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Skills).ThenInclude(x=>x.Technology)
                .Include(x=>x.Favourites)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Employer)
                .Include(x=>x.Favourites)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Skills).ThenInclude(x=>x.Technology)
                .SingleOrDefaultAsync(x => x.User.Email == email);
        }

        public async Task<Employee> GetByUserIdAsync(Guid userId)
        {
            var result = await _context.Employees
                .Include(x=>x.User)
                .Include(x=>x.Courses)
                .Include(x=>x.Education)
                .Include(x=>x.EmployeeLanguages)
                    .ThenInclude(x=>x.Language)
                .Include(x=>x.EmployeeSkills)
                    .ThenInclude(x=>x.Technology)
                .Include(x=>x.ProfessionalExperience)
                .Include(x=>x.JobApplications)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Employer)
                .Include(x=>x.JobApplications)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Skills).ThenInclude(x=>x.Technology)
                .Include(x=>x.Favourites)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Employer)
                .Include(x=>x.Favourites)
                    .ThenInclude(x=>x.JobOffer).ThenInclude(x=>x.Skills).ThenInclude(x=>x.Technology)
                .SingleOrDefaultAsync(x => x.User.Id == userId);

            return result;
        }

        public async Task UpdateEmployee(Employee employee)
        {
            _context.Employees.Update(employee);
        }
    }
}
