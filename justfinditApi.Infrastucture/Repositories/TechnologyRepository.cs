using System.Collections.Generic;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class TechnologyRepository: ITechnologyRepository
    {
        private readonly ApplicationDbContext _context;

        public TechnologyRepository(ApplicationDbContext context){
            _context = context;
        }

        public async Task<Technology> AddTechnologyAsync(Technology technology)
        {
            var result = await _context.Technologies.AddAsync(technology);
            return result.Entity;
        }

        public async Task<Technology> GetTechnologyByNameAsync(string name)
        {
            return await _context.Technologies.SingleOrDefaultAsync(x => x.TechnologyName.ToLower() == name.ToLower());
        }

        public async Task<IEnumerable<Technology>> GetAllAsync()
        {
            return await _context.Technologies.ToListAsync();
        }
    }
}