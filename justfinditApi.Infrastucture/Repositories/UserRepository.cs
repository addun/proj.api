using System;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class UserRepository: IUserRepository
    {     
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public async Task<User> GetUserByIdAsync(Guid userId)
        {
            return await _context.Users.SingleOrDefaultAsync(user => user.Id == userId);
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await _context.Users.SingleOrDefaultAsync(user => user.Email == email);
        }

        public void UpdateUser(User user)
        {
            _context.Users.Update(user);
        }
    }
}