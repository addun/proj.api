using System;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class JobOffersRepository: IJobOffersRepository
    {
        private readonly ApplicationDbContext _context;

        public JobOffersRepository(ApplicationDbContext context){
            _context = context;
        }

        public async Task<JobOffer> FindById(Guid id)
        {
            var jobOffer = await _context.JobOffers.SingleOrDefaultAsync(x => x.OfferId == id);
            return jobOffer;
        }
    }
}