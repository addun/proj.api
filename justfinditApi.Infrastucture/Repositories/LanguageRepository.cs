using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class LanguageRepository: ILanguageRepository
    {
        private readonly ApplicationDbContext _context;

        public LanguageRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public async Task<Language> GetLanguageByName(string name)
        {
            return await _context.Languages.SingleOrDefaultAsync(l => l.LanguageName.ToLower() == name.ToLower());
        }
    }
}