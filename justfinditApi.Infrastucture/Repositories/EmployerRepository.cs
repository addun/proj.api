﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Repositories
{
    public class EmployerRepository : IEmployerRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployerRepository(ApplicationDbContext context){
            _context = context;
        }

        public async Task<Employer> AddAsync(Employer employer)
        {
            var newEmployer = await _context.Employers.AddAsync(employer);
            return newEmployer.Entity;
        }

        public async Task<Employer> GetByEmailAsync(string email)
        {
            return await _context.Employers
                .Include(x => x.User)
                .Include(x => x.Benefits)
                .Include(x => x.Locations)
                .Include(x => x.Technologies)
                    .ThenInclude(x => x.Technology)
                .Include(x => x.JobOffers)
                    .ThenInclude(x => x.Skills)
                        .ThenInclude(x=>x.Technology)
                .Include(x => x.JobOffers)
                    .ThenInclude(x=>x.JobApplication)
                        .ThenInclude(x=>x.JobOffer)
                .Include(x => x.JobOffers)
                    .ThenInclude(x=>x.JobApplication)
                        .ThenInclude(x=>x.Employee)
                            .ThenInclude(x=>x.User)
                .SingleOrDefaultAsync(x => x.User.Email == email);
        }

        public async Task<Employer> GetByUserIdAsync(Guid userId)
        {
            return await _context.Employers
                .Include(x => x.User)
                .Include(x => x.Benefits)
                .Include(x => x.Locations)
                .Include(x => x.Technologies)
                    .ThenInclude(x=>x.Technology)
                .Include(x => x.JobOffers)
                    .ThenInclude(x => x.Skills)
                        .ThenInclude(x=>x.Technology)
                .Include(x => x.JobOffers)
                    .ThenInclude(x=>x.JobApplication)
                        .ThenInclude(x=>x.JobOffer)
                .Include(x => x.JobOffers)
                    .ThenInclude(x=>x.JobApplication)
                        .ThenInclude(x=>x.Employee)
                            .ThenInclude(x=>x.User)
                .SingleOrDefaultAsync(x => x.User.Id == userId);
        }

        public async Task UpdateEmployer(Employer employer)
        {
            _context.Employers.Update(employer);
        }
    }
}
