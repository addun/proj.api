﻿using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using Microsoft.EntityFrameworkCore.Storage;

namespace justfinditApi.Infrastucture.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private IDbContextTransaction _transaction;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            JobOffers = new JobOffersRepository(context);
            Employees = new EmployeeRepository(context);
            Employers = new EmployerRepository(context);
            Languages = new LanguageRepository(context);
            Technologies = new TechnologyRepository(context);
            Users = new UserRepository(context);
        }

        public IJobOffersRepository JobOffers { get; private set; }
        public IEmployeeRepository Employees { get; private set; }
        public IEmployerRepository Employers { get; private set; }
        public ILanguageRepository Languages { get; private set; }
        public ITechnologyRepository Technologies { get; private set; }
        public IUserRepository Users { get; private set; }

        public async Task BeginTransactionAsync()
        {
            _transaction = await _context.Database.BeginTransactionAsync();
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
