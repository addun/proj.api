﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeNameOfTablesRows : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyLocations_Employers_EmployerId",
                table: "CompanyLocations");

            migrationBuilder.DropForeignKey(
                name: "FK_Courses_Employees_EmployeeId",
                table: "Courses");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeLanguages_Employees_EmployeeId",
                table: "EmployeeLanguages");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeLanguages_Languages_LanguageId",
                table: "EmployeeLanguages");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeSkills_Employees_EmployeeId",
                table: "EmployeeSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeSkills_Technologies_TechnologyId",
                table: "EmployeeSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployerTechnologies_Employers_EmployerId",
                table: "EmployerTechnologies");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployerTechnologies_Technologies_TechnologyId",
                table: "EmployerTechnologies");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOffer_Employers_CompanyId",
                table: "JobOffer");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOfferTechnology_JobOffer_JobOfferId",
                table: "JobOfferTechnology");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobOffer",
                table: "JobOffer");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeSkills",
                table: "EmployeeSkills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeLanguages",
                table: "EmployeeLanguages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Courses",
                table: "Courses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyLocations",
                table: "CompanyLocations");

            migrationBuilder.RenameTable(
                name: "JobOffer",
                newName: "JobOffers");

            migrationBuilder.RenameTable(
                name: "EmployerTechnologies",
                newName: "EmployerTechnology");

            migrationBuilder.RenameTable(
                name: "EmployeeSkills",
                newName: "EmployeeSkill");

            migrationBuilder.RenameTable(
                name: "EmployeeLanguages",
                newName: "EmployeeLanguage");

            migrationBuilder.RenameTable(
                name: "Courses",
                newName: "Course");

            migrationBuilder.RenameTable(
                name: "CompanyLocations",
                newName: "EmployerLocation");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "JobOffers",
                newName: "EmployerId");

            migrationBuilder.RenameIndex(
                name: "IX_JobOffer_CompanyId",
                table: "JobOffers",
                newName: "IX_JobOffers_EmployerId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployerTechnologies_TechnologyId",
                table: "EmployerTechnology",
                newName: "IX_EmployerTechnology_TechnologyId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployeeSkills_TechnologyId",
                table: "EmployeeSkill",
                newName: "IX_EmployeeSkill_TechnologyId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployeeLanguages_LanguageId",
                table: "EmployeeLanguage",
                newName: "IX_EmployeeLanguage_LanguageId");

            migrationBuilder.RenameIndex(
                name: "IX_Courses_EmployeeId",
                table: "Course",
                newName: "IX_Course_EmployeeId");

            migrationBuilder.RenameIndex(
                name: "IX_CompanyLocations_EmployerId",
                table: "EmployerLocation",
                newName: "IX_EmployerLocation_EmployerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobOffers",
                table: "JobOffers",
                column: "OfferId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployerTechnology",
                table: "EmployerTechnology",
                columns: new[] { "EmployerId", "TechnologyId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeSkill",
                table: "EmployeeSkill",
                columns: new[] { "EmployeeId", "TechnologyId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeLanguage",
                table: "EmployeeLanguage",
                columns: new[] { "EmployeeId", "LanguageId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Course",
                table: "Course",
                columns: new[] { "CourseId", "EmployeeId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployerLocation",
                table: "EmployerLocation",
                columns: new[] { "EmployerLocationId", "EmployerId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Course_Employees_EmployeeId",
                table: "Course",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeLanguage_Employees_EmployeeId",
                table: "EmployeeLanguage",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeLanguage_Languages_LanguageId",
                table: "EmployeeLanguage",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeSkill_Employees_EmployeeId",
                table: "EmployeeSkill",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeSkill_Technologies_TechnologyId",
                table: "EmployeeSkill",
                column: "TechnologyId",
                principalTable: "Technologies",
                principalColumn: "TechnologyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployerLocation_Employers_EmployerId",
                table: "EmployerLocation",
                column: "EmployerId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployerTechnology_Employers_EmployerId",
                table: "EmployerTechnology",
                column: "EmployerId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployerTechnology_Technologies_TechnologyId",
                table: "EmployerTechnology",
                column: "TechnologyId",
                principalTable: "Technologies",
                principalColumn: "TechnologyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOffers_Employers_EmployerId",
                table: "JobOffers",
                column: "EmployerId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOfferTechnology_JobOffers_JobOfferId",
                table: "JobOfferTechnology",
                column: "JobOfferId",
                principalTable: "JobOffers",
                principalColumn: "OfferId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_Employees_EmployeeId",
                table: "Course");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeLanguage_Employees_EmployeeId",
                table: "EmployeeLanguage");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeLanguage_Languages_LanguageId",
                table: "EmployeeLanguage");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeSkill_Employees_EmployeeId",
                table: "EmployeeSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeSkill_Technologies_TechnologyId",
                table: "EmployeeSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployerLocation_Employers_EmployerId",
                table: "EmployerLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployerTechnology_Employers_EmployerId",
                table: "EmployerTechnology");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployerTechnology_Technologies_TechnologyId",
                table: "EmployerTechnology");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOffers_Employers_EmployerId",
                table: "JobOffers");

            migrationBuilder.DropForeignKey(
                name: "FK_JobOfferTechnology_JobOffers_JobOfferId",
                table: "JobOfferTechnology");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobOffers",
                table: "JobOffers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployerTechnology",
                table: "EmployerTechnology");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployerLocation",
                table: "EmployerLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeSkill",
                table: "EmployeeSkill");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployeeLanguage",
                table: "EmployeeLanguage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Course",
                table: "Course");

            migrationBuilder.RenameTable(
                name: "JobOffers",
                newName: "JobOffer");

            migrationBuilder.RenameTable(
                name: "EmployerTechnology",
                newName: "EmployerTechnologies");

            migrationBuilder.RenameTable(
                name: "EmployerLocation",
                newName: "CompanyLocations");

            migrationBuilder.RenameTable(
                name: "EmployeeSkill",
                newName: "EmployeeSkills");

            migrationBuilder.RenameTable(
                name: "EmployeeLanguage",
                newName: "EmployeeLanguages");

            migrationBuilder.RenameTable(
                name: "Course",
                newName: "Courses");

            migrationBuilder.RenameColumn(
                name: "EmployerId",
                table: "JobOffer",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_JobOffers_EmployerId",
                table: "JobOffer",
                newName: "IX_JobOffer_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployerTechnology_TechnologyId",
                table: "EmployerTechnologies",
                newName: "IX_EmployerTechnologies_TechnologyId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployerLocation_EmployerId",
                table: "CompanyLocations",
                newName: "IX_CompanyLocations_EmployerId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployeeSkill_TechnologyId",
                table: "EmployeeSkills",
                newName: "IX_EmployeeSkills_TechnologyId");

            migrationBuilder.RenameIndex(
                name: "IX_EmployeeLanguage_LanguageId",
                table: "EmployeeLanguages",
                newName: "IX_EmployeeLanguages_LanguageId");

            migrationBuilder.RenameIndex(
                name: "IX_Course_EmployeeId",
                table: "Courses",
                newName: "IX_Courses_EmployeeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobOffer",
                table: "JobOffer",
                column: "OfferId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies",
                columns: new[] { "EmployerId", "TechnologyId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyLocations",
                table: "CompanyLocations",
                columns: new[] { "EmployerLocationId", "EmployerId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeSkills",
                table: "EmployeeSkills",
                columns: new[] { "EmployeeId", "TechnologyId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployeeLanguages",
                table: "EmployeeLanguages",
                columns: new[] { "EmployeeId", "LanguageId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Courses",
                table: "Courses",
                columns: new[] { "CourseId", "EmployeeId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyLocations_Employers_EmployerId",
                table: "CompanyLocations",
                column: "EmployerId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Courses_Employees_EmployeeId",
                table: "Courses",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeLanguages_Employees_EmployeeId",
                table: "EmployeeLanguages",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeLanguages_Languages_LanguageId",
                table: "EmployeeLanguages",
                column: "LanguageId",
                principalTable: "Languages",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeSkills_Employees_EmployeeId",
                table: "EmployeeSkills",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "EmployeeId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeSkills_Technologies_TechnologyId",
                table: "EmployeeSkills",
                column: "TechnologyId",
                principalTable: "Technologies",
                principalColumn: "TechnologyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployerTechnologies_Employers_EmployerId",
                table: "EmployerTechnologies",
                column: "EmployerId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployerTechnologies_Technologies_TechnologyId",
                table: "EmployerTechnologies",
                column: "TechnologyId",
                principalTable: "Technologies",
                principalColumn: "TechnologyId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOffer_Employers_CompanyId",
                table: "JobOffer",
                column: "CompanyId",
                principalTable: "Employers",
                principalColumn: "EmployerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobOfferTechnology_JobOffer_JobOfferId",
                table: "JobOfferTechnology",
                column: "JobOfferId",
                principalTable: "JobOffer",
                principalColumn: "OfferId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
