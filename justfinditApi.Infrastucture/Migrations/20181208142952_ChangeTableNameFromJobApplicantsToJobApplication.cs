﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeTableNameFromJobApplicantsToJobApplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobApplicant");

            migrationBuilder.CreateTable(
                name: "JobApplication",
                columns: table => new
                {
                    EmployeeId = table.Column<Guid>(nullable: false),
                    JobOfferId = table.Column<Guid>(nullable: false),
                    Resume = table.Column<byte[]>(nullable: true),
                    ResumeExtension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApplication", x => new { x.EmployeeId, x.JobOfferId });
                    table.ForeignKey(
                        name: "FK_JobApplication_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobApplication_JobOffers_JobOfferId",
                        column: x => x.JobOfferId,
                        principalTable: "JobOffers",
                        principalColumn: "OfferId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobApplication_JobOfferId",
                table: "JobApplication",
                column: "JobOfferId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobApplication");

            migrationBuilder.CreateTable(
                name: "JobApplicant",
                columns: table => new
                {
                    EmployeeId = table.Column<Guid>(nullable: false),
                    JobOfferId = table.Column<Guid>(nullable: false),
                    Resume = table.Column<byte[]>(nullable: true),
                    ResumeExtension = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApplicant", x => new { x.EmployeeId, x.JobOfferId });
                    table.ForeignKey(
                        name: "FK_JobApplicant_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobApplicant_JobOffers_JobOfferId",
                        column: x => x.JobOfferId,
                        principalTable: "JobOffers",
                        principalColumn: "OfferId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobApplicant_JobOfferId",
                table: "JobApplicant",
                column: "JobOfferId");
        }
    }
}
