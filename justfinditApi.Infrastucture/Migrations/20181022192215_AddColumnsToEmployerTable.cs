﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class AddColumnsToEmployerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InternshipDescription",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WhatsWeCreatingDescription",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WhyWorthWorkingWithUsDescription",
                table: "Employers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InternshipDescription",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "WhatsWeCreatingDescription",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "WhyWorthWorkingWithUsDescription",
                table: "Employers");
        }
    }
}
