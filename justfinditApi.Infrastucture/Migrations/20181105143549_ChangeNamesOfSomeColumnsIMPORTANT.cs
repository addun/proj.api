﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeNamesOfSomeColumnsIMPORTANT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CompanyLocationId",
                table: "CompanyLocations",
                newName: "EmployerLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmployerLocationId",
                table: "CompanyLocations",
                newName: "CompanyLocationId");
        }
    }
}
