﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeNameOftechnologyIdColumnToTechnologyId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Technologies",
                newName: "TechnologyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TechnologyId",
                table: "Technologies",
                newName: "Id");
        }
    }
}
