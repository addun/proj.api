﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeEmployerTechnologiesPrimmaryKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies");

            migrationBuilder.DropIndex(
                name: "IX_EmployerTechnologies_EmployerId",
                table: "EmployerTechnologies");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "EmployerTechnologies");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies",
                columns: new[] { "EmployerId", "TechnologyId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "EmployerTechnologies",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmployerTechnologies",
                table: "EmployerTechnologies",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_EmployerTechnologies_EmployerId",
                table: "EmployerTechnologies",
                column: "EmployerId");
        }
    }
}
