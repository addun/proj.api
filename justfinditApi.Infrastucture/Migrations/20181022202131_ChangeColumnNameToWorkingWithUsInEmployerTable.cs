﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeColumnNameToWorkingWithUsInEmployerTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WhyWorthWorkingWithUsDescription",
                table: "Employers",
                newName: "WorkingWithUsDescription");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkingWithUsDescription",
                table: "Employers",
                newName: "WhyWorthWorkingWithUsDescription");
        }
    }
}
