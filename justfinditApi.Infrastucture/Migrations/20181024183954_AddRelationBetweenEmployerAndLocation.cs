﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class AddRelationBetweenEmployerAndLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsMainLocation",
                table: "CompanyLocations");

            migrationBuilder.AddColumn<Guid>(
                name: "HeadquartersId",
                table: "Employers",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeadquartersId",
                table: "Employers");

            migrationBuilder.AddColumn<bool>(
                name: "IsMainLocation",
                table: "CompanyLocations",
                nullable: false,
                defaultValue: false);
        }
    }
}
