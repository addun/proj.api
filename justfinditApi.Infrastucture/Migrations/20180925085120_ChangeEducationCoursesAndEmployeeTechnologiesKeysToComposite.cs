﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeEducationCoursesAndEmployeeTechnologiesKeysToComposite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessionalExperience",
                table: "ProfessionalExperience");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Education",
                table: "Education");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Courses",
                table: "Courses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessionalExperience",
                table: "ProfessionalExperience",
                columns: new[] { "ProfessionalExperienceId", "EmployeeId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Education",
                table: "Education",
                columns: new[] { "EducationId", "EmployeeId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Courses",
                table: "Courses",
                columns: new[] { "CourseId", "EmployeeId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessionalExperience",
                table: "ProfessionalExperience");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Education",
                table: "Education");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Courses",
                table: "Courses");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessionalExperience",
                table: "ProfessionalExperience",
                column: "ProfessionalExperienceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Education",
                table: "Education",
                column: "EducationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Courses",
                table: "Courses",
                column: "CourseId");
        }
    }
}
