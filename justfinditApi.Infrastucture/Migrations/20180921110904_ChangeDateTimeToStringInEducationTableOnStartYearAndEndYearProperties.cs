﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class ChangeDateTimeToStringInEducationTableOnStartYearAndEndYearProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "Education");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Education");

            migrationBuilder.AddColumn<string>(
                name: "EndYear",
                table: "Education",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StartYear",
                table: "Education",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndYear",
                table: "Education");

            migrationBuilder.DropColumn(
                name: "StartYear",
                table: "Education");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "Education",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Education",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
