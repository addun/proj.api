﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class AddWorkTimeAndInvoiceTypeColumtToJobOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvoiceType",
                table: "JobOffer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkTime",
                table: "JobOffer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceType",
                table: "JobOffer");

            migrationBuilder.DropColumn(
                name: "WorkTime",
                table: "JobOffer");
        }
    }
}
