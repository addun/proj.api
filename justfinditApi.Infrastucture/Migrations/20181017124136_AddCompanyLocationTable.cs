﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class AddCompanyLocationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApartmentNumber",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "FlatNumber",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "Employers");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "Employers");

            migrationBuilder.CreateTable(
                name: "CompanyLocations",
                columns: table => new
                {
                    Street = table.Column<string>(nullable: true),
                    ApartmentNumber = table.Column<string>(nullable: true),
                    FlatNumber = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    IsMainLocation = table.Column<bool>(nullable: false),
                    CompanyLocationId = table.Column<Guid>(nullable: false),
                    EmployerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyLocations", x => new { x.CompanyLocationId, x.EmployerId });
                    table.ForeignKey(
                        name: "FK_CompanyLocations_Employers_EmployerId",
                        column: x => x.EmployerId,
                        principalTable: "Employers",
                        principalColumn: "EmployerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyLocations_EmployerId",
                table: "CompanyLocations",
                column: "EmployerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyLocations");

            migrationBuilder.AddColumn<string>(
                name: "ApartmentNumber",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FlatNumber",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "Employers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Employers",
                nullable: true);
        }
    }
}
