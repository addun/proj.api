﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace justfinditApi.Infrastucture.Migrations
{
    public partial class AddRelationsJobOfferWithEmployerAndTechnologyWithJobOffer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobOffer",
                columns: table => new
                {
                    OfferId = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    SalaryFrom = table.Column<long>(nullable: false),
                    SalaryTo = table.Column<long>(nullable: false),
                    Currency = table.Column<string>(nullable: true),
                    ExperienceLevel = table.Column<string>(nullable: true),
                    OfferDescription = table.Column<string>(nullable: true),
                    Remote = table.Column<string>(nullable: true),
                    CompanyId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobOffer", x => x.OfferId);
                    table.ForeignKey(
                        name: "FK_JobOffer_Employers_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Employers",
                        principalColumn: "EmployerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobOfferTechnology",
                columns: table => new
                {
                    JobOfferId = table.Column<Guid>(nullable: false),
                    TechnologyId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobOfferTechnology", x => new { x.JobOfferId, x.TechnologyId });
                    table.ForeignKey(
                        name: "FK_JobOfferTechnology_JobOffer_JobOfferId",
                        column: x => x.JobOfferId,
                        principalTable: "JobOffer",
                        principalColumn: "OfferId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobOfferTechnology_Technologies_TechnologyId",
                        column: x => x.TechnologyId,
                        principalTable: "Technologies",
                        principalColumn: "TechnologyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobOffer_CompanyId",
                table: "JobOffer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_JobOfferTechnology_TechnologyId",
                table: "JobOfferTechnology",
                column: "TechnologyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobOfferTechnology");

            migrationBuilder.DropTable(
                name: "JobOffer");
        }
    }
}
