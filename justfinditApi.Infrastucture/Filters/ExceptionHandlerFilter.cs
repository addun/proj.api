﻿using System.Collections.Generic;
using System.Linq;
using justfinditApi.Infrastucture.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;

namespace justfinditApi.Infrastucture.Filters
{
    public class ExceptionHandlerFilter: ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var defaultError = new Dictionary<string, List<string>>()
            {
                // { "", new List<string>() { "Wystąpił niespodziewany błąd.Proszę spróbować później lub skontaktować się z supportem: support.justfindit @gmail.com" } }
                { "", new List<string>() { context.Exception.Message } }
            };
            IActionResult result = null;

            if(context.Exception is ServerException)
            {
                var serverException = context.Exception as ServerException;
                if(serverException.Errors.Any())
                    result = new BadRequestObjectResult(serverException.Errors);
                else
                    result = new BadRequestObjectResult(defaultError);

                context.Result = result;
            }
            else if (context.Exception is SecurityTokenException)
            {
                var unauthorizedResult = new UnauthorizedResult();
                context.Result = unauthorizedResult;
            }
            else
            {
                var unexpectedExceptionResult = new JsonResult(defaultError){ StatusCode = 500 };
                context.Result = unexpectedExceptionResult;
            }
        }
    }
}
