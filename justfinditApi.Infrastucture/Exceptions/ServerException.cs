﻿using System;
using System.Collections.Generic;

namespace justfinditApi.Infrastucture.Exceptions
{
    public class ServerException: Exception
    {
        public Dictionary<string, List<string>> Errors { get; private set; } = new Dictionary<string, List<string>>();

        public ServerException() : base() { }
        public ServerException(string exceptionMessage) : base(exceptionMessage) { }
        public ServerException(string exceptionMessage, string errorKey, string clientErrorMessage) : base(exceptionMessage)
        {
            Errors.Add(errorKey, new List<string>() { clientErrorMessage });
        }
        public ServerException(string exceptionMessage, Dictionary<string, List<string>> errorsToReturnToClient) : base(exceptionMessage)
        {
            Errors = errorsToReturnToClient;
        }
    }
}
