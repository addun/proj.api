using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using AutoMapper;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore;

namespace justfinditApi.Infrastucture.Services
{
    public class JobFinder : IJobFinder
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;

        public JobFinder(ApplicationDbContext dbContext,IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public IEnumerable<JobOfferDto> Find(List<string> skills, List<string> cities, string salary, string currency)
        {


            var result = new List<JobOfferDto>();
            using (var command = ((SqlConnection) _dbContext.Database.GetDbConnection()).CreateCommand())
            {
                command.CommandType = CommandType.Text;
                var sql = new StringBuilder();

                // ==============================================================================================
                // EACH CHANGE IN SELECT STATEMENT SHOULD BE PERFORM CAREFULLY 
                // MAKE SURE THAT COLUMN NAMES IN READING OPERATIONS RESPOND COLUMN NAMES IN SELECT STATEMENT
                // ==============================================================================================

                sql.Append("SELECT [jobofferid] as jobofferid, [title] as title, [country] as country,");
                sql.Append(" city as city, [address] as [address], salaryfrom as salaryfrom, salaryto as salaryto,");
                sql.Append(
                    " currency as currency, experiencelevel as experiencelevel, offerdescription as offerdescription,");
                sql.Append(
                    " [remote] as [remote] ,joboffers.employerId as [employerId], creationDate as creationdate,");
                sql.Append(
                    " invoicetype as invoicetype, worktime as worktime, Technologies.TechnologyId as technologyid,");
                sql.Append(" Technologies.TechnologyName as technologyname,");
                sql.Append(
                    " companyname as companyname, companydescription as companydescription, foundedyear as foundedyear,");
                sql.Append(" companysize as companysize, websiteurl as  websiteurl");
                sql.Append(" FROM JobOfferTechnology");
                sql.Append(" JOIN JobOffers on JobOffers.OfferId = JobOfferTechnology.JobOfferId");
                sql.Append(" join Employers on JobOffers.EmployerId = Employers.EmployerId");
                sql.Append(" JOIN Technologies on Technologies.TechnologyId = JobOfferTechnology.TechnologyId");
                if (skills.Count > 0)
                {
                    sql.Append(" WHERE jobofferid IN (");
                    sql.Append(" SELECT jobofferid");
                    sql.Append(" FROM joboffertechnology");
                    sql.Append(" JOIN joboffers on joboffers.offerid = joboffertechnology.jobofferid");
                    sql.Append(" JOIN technologies on technologies.technologyid = joboffertechnology.technologyid");
                    sql.Append(" WHERE TechnologyName in ({0})");
                    sql.Append(" )");
                }

                if (cities.Count > 0)
                {
                    sql.Append(skills.Count > 0 ? " AND" : " WHERE");
                    sql.Append(" City IN ({1})");
                }

                if (!string.IsNullOrWhiteSpace(salary))
                {
                    if (skills.Count > 0 || cities.Count > 0)
                    {
                        sql.Append(" AND");
                    }
                    else
                    {
                        sql.Append(" WHERE");
                    }

                    sql.Append(" SalaryTo >= {2}");
                }

                sql.Append(" ORDER BY jobofferid");

                var skillsParameterList = new List<string>();
                var index = 0;
                foreach (var skill in skills)
                {
                    var paramName = "@skillParam" + index;
                    command.Parameters.AddWithValue(paramName, skill);
                    skillsParameterList.Add(paramName);
                    index++;
                }

                var citiesParameter = new List<string>();
                index = 0; // Reset the index
                foreach (var city in cities)
                {
                    var paramName = "@cityParam" + index;
                    command.Parameters.AddWithValue(paramName, city);
                    citiesParameter.Add(paramName);
                    index++;
                }

                var salaryParamName = "@salaryParam";
                if (!string.IsNullOrWhiteSpace(salary))
                {
                    command.Parameters.AddWithValue(salaryParamName, salary);
                }

                command.CommandText = String.Format(sql.ToString(),
                    string.Join(",", skillsParameterList),
                    string.Join(",", citiesParameter),
                    salaryParamName);

                _dbContext.Database.OpenConnection();

                // JOB OFFERS ARE ORDERED BY JOBOFFERID SO 
                // WHEN JOB OFFERS ID ARE THE SAME WE CAN SIMPLY JUST ADD TECHNOLOGY FROM CURRENT ROW
                // AND OMIT REMAINING VALUES
                using (var reader = command.ExecuteReader())
                {
                    var lastReadedJobOfferDto = new JobOfferDto();
                    
                    while (reader.Read())
                    {
                        var jobOfferId = reader["jobofferid"].ToString();
                        if (lastReadedJobOfferDto.OfferId == jobOfferId)
                        {
                            lastReadedJobOfferDto.Skills.Add(reader["technologyname"].ToString());
                            continue;
                        }
                        
                        lastReadedJobOfferDto = new JobOfferDto();
                        lastReadedJobOfferDto.OfferId = reader["jobofferid"].ToString();
                        lastReadedJobOfferDto.Title = reader["title"].ToString();
                        lastReadedJobOfferDto.Country = reader["country"].ToString();
                        lastReadedJobOfferDto.City = reader["city"].ToString();
                        lastReadedJobOfferDto.Address = reader["address"].ToString();
                        lastReadedJobOfferDto.SalaryFrom = reader["salaryfrom"].ToString();
                        lastReadedJobOfferDto.SalaryTo = reader["salaryto"].ToString();
                        lastReadedJobOfferDto.Currency = reader["currency"].ToString();
                        lastReadedJobOfferDto.ExperienceLevel = reader["experiencelevel"].ToString();
                        lastReadedJobOfferDto.Remote = reader["remote"].ToString();
                        lastReadedJobOfferDto.CreationDate = reader["creationdate"].ToString();
                        try
                        {
                            lastReadedJobOfferDto.CreationDate
                                = DateTime.Parse(lastReadedJobOfferDto.CreationDate).ToShortDateString();
                        }
                        catch (Exception e)
                        {
                            // ignored
                        }

                        lastReadedJobOfferDto.InvoiceType = reader["invoicetype"].ToString();
                        lastReadedJobOfferDto.WorkTime = reader["worktime"].ToString();

                        //ADD EMPLOYER TO JOB OFFER
                        var employerDto = new JobOfferEmployerDto();
                        employerDto.EmployerId = reader["employerId"].ToString();
                        employerDto.CompanyName = reader["companyname"].ToString();
                        employerDto.CompanyDescription = reader["companydescription"].ToString();
                        employerDto.CompanySize = reader["companysize"].ToString();
                        employerDto.FoundedYear = reader["foundedyear"].ToString();
                        employerDto.WebsiteUrl = reader["websiteurl"].ToString();
                        lastReadedJobOfferDto.Employer = employerDto;

                        //ADD TECHNOLOGIES TO JOB OFFER
                        lastReadedJobOfferDto.Skills.Add(reader["technologyname"].ToString());

                        result.Add(lastReadedJobOfferDto);
                    }
                }

                _dbContext.Database.CloseConnection();
            }

            return result;
        }

        public JobOfferDto FindById(Guid offerId)
        {
            var offer = _dbContext.JobOffers
                .Include(x => x.Employer)
                .Include(x => x.Skills)
                .ThenInclude(x => x.Technology)
                .SingleOrDefault(x => x.OfferId == offerId);

            if(offer == null)
                throw new ServerException("offer not found");
            
            var result = _mapper.Map<JobOffer, JobOfferDto>(offer);

            return result;
        }
    }
}