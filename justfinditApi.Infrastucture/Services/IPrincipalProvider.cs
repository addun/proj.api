﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;

namespace justfinditApi.Infrastucture.Services
{
    public interface IPrincipalProvider
    {
        IPrincipal User { get; }
        IEnumerable<Claim> GetUserClaims();
        string GetUserId();
        string GetAuthToken();
    }
}
