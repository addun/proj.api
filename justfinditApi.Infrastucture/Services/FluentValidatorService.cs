﻿using FluentValidation.Results;
using justfinditApi.Infrastucture.Validators;
using justfinditApi.Infrastucture.Validators.Extensions;
using justfinditApi.Infrastucture.Validators.ValidatorResolver;

namespace justfinditApi.Infrastucture.Services
{
    public class FluentValidatorService : IValidatorService
    {
        private readonly IValidatorResolver _resolver;

        public FluentValidatorService(IValidatorResolver resolver)
        {
            _resolver = resolver;
        }

        public FluentValidationServiceResult Validate<T>(T obj)
        {
            var validator = _resolver.Resolve<T>();
            ValidationResult result = validator.Validate(obj);

            return FluentValidationServiceResult.Create(result.GetErrorsInDictionaryFormat());
        }
    }
}
