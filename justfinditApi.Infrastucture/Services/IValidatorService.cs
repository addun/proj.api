﻿using justfinditApi.Infrastucture.Validators;

namespace justfinditApi.Infrastucture.Services
{
    public interface IValidatorService
    {
        FluentValidationServiceResult Validate<T>(T obj);
    }
}
