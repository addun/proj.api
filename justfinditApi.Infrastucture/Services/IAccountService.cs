﻿using System.Threading.Tasks;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Services
{
    public interface IAccountService
    {
        Task ConfirmEmail(ConfirmEmailDto confirmEmailDto);

        Task ForgotPassword(ForgotPasswordDto forgotPassword);

        Task<AuthTokenDto> GetEmployeeToken(string authorizationHeader);

        Task<AuthTokenDto> GetEmployerToken(string authorizationHeader);
        
        Task<AuthTokenDto> RefreshToken(RefreshTokenDto refreshToken);

        Task<EmployeeDto> RegisterEmployee(RegisterEmployeeDto employee);

        Task<EmployerDto> RegisterEmployer(RegisterEmployerDto employer);

        Task ResendEmail();

        Task ResetPassword(ResetPasswordDto resetPasswordDto);
    }
}
