using System;
using System.IO;
using System.Threading.Tasks;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Services
{
    public interface IEmployerService
    {
        Task<EmployerDto> GetEmployerByUserId(Guid id);
        Task<EmployeeProfile> GetEmployeeProfileByUserId(Guid id);
        Task<ResumeDto> GetApplicantCv(Guid employerId, Guid jobOfferId, Guid applicantId);
        Task AddBenefitAsync(AddEmployerBenefitDto addEmployerBenefitDto);
        Task AddLocationAsync(AddEmployerLocationDto addEmployerLocationDto);
        Task AddTechnologyAsync(AddTechnologyDto AddTechnologyDto);
        Task AddJobOfferAsync(AddJobOfferDto addJobOfferDto);
        Task DeleteBenefitAsync(Guid employerId, Guid benefitId);
        Task DeleteLocationAsync(Guid employerId, Guid locationId);
        Task DeleteJobOfferAsync(Guid userId, Guid jobOfferId);
        Task DeleteTechnologyAsync(Guid employerId, Guid technologyId);
        Task EditBenefitAsync(EditEmployerBenefitDto editEmployerBenefitDto);
        Task EditEmployerDataAsync(EditEmployerDataDto editEmployerDataDto);
        Task EditJobOfferAsync(EditJobOfferDto editJobOfferDto);
        Task EditLocationAsync(EditEmployerLocationDto editEmployerLocationDto);
        Task EditWhatsWeCreatingDescription(EditWhatsWeCreatingDescriptionDto editWhatsWeCreatingDescriptionDto);
        Task EditWorkingWithUsDescription(EditWorkingWithUsDescriptionDto editWorkingWithUsDescriptionDto);
        Task EditInternshipDescription(EditInternshipDescriptionDto editInternshipDescriptionDto);
        Task SetHeadquartersAsync(SetHeadquartersDto setHeadquartersDto);
    }
}