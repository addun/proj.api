﻿using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;

namespace justfinditApi.Infrastucture.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _db;
        private readonly IValidatorService _validatorService;
        private readonly IMapper _mapper;

        public EmployeeService(IUnitOfWork db, IValidatorService validatorService, IMapper mapper)
        {
            _db = db;
            _validatorService = validatorService;
            _mapper = mapper;
        }

        public async Task AddCertificateAsync(AddCertificateDto addCertificateDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(addCertificateDto.UserId);
            
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var course = Course.Create(
                addCertificateDto.Course.CourseName,
                addCertificateDto.Course.OrganizerName, addCertificateDto.Course.MonthOfObtain,
                addCertificateDto.Course.YearOfObtain);
            
            employee.AddCourse(course);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task AddEducationAsync(AddEducationDto addEducationDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(addEducationDto.UserId);
            
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var education = Education.Create(addEducationDto.Education.SchoolName, 
                addEducationDto.Education.StartYear, 
                addEducationDto.Education.EndYear, 
                addEducationDto.Education.BranchOfStudy,
                addEducationDto.Education.Degree, 
                addEducationDto.Education.Speciality);
            
            employee.AddEducation(education);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task AddJobOfferToFavouritesAsync(AddJobOfferToFavouritesDto addJobOfferToFavouritesDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(Guid.Parse(addJobOfferToFavouritesDto.UserId));  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            var jobOffer = await _db.JobOffers.FindById(Guid.Parse(addJobOfferToFavouritesDto.OfferId));  
            if(jobOffer == null)
                throw new ServerException("Job offer with given id doesn't exist.");
            
            employee.AddJobOfferToFavourites(jobOffer);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task AddLanguageAsync(AddLanguageDto addLanguageDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(addLanguageDto.UserId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var language = await _db.Languages.GetLanguageByName(addLanguageDto.Language.LanguageName);
            if(language == null)
                throw new ServerException("Language doesn't exist.");
            
            employee.AddLanguage(language, addLanguageDto.Language.Level);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task AddSkillAsync(AddSkillDto addSkillDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(addSkillDto.UserId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var technology = await _db.Technologies.GetTechnologyByNameAsync(addSkillDto.Skill.SkillName);
            if(technology == null)
                throw new ServerException("Technology doesn't exist.");
            
            employee.AddSkill(technology, addSkillDto.Skill.Level);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task AddProfessionalExperienceAsync(AddProfessionalExperienceDto addProfessionalExperienceDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(addProfessionalExperienceDto.UserId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var professionalExperience = ProfessionalExperience.Create(
                addProfessionalExperienceDto.ProfessionalExperience.Position,
                addProfessionalExperienceDto.ProfessionalExperience.CompanyName,
                addProfessionalExperienceDto.ProfessionalExperience.StartMonth,
                addProfessionalExperienceDto.ProfessionalExperience.StartYear,
                addProfessionalExperienceDto.ProfessionalExperience.EndMonth,
                addProfessionalExperienceDto.ProfessionalExperience.EndYear,
                addProfessionalExperienceDto.ProfessionalExperience.Country,
                addProfessionalExperienceDto.ProfessionalExperience.City,
                addProfessionalExperienceDto.ProfessionalExperience.JobDescription);
            
            employee.AddProfessionalExperience(professionalExperience);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task ApplyForJobAsync(ApplyForJobDto applyForJobDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(applyForJobDto.UserId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            var jobOffer = await _db.JobOffers.FindById(applyForJobDto.JobId);  
            if(jobOffer == null)
                throw new ServerException("Job offer with given id doesn't exist.");

            var cv = applyForJobDto.UploadFile;
            if ( !(cv.Length > 0) )
                throw new ServerException("Invalid pdf file.");

            byte[] cvBytes;
            using (var ms = new MemoryStream())
            {
                cv.CopyTo(ms);
                cvBytes = ms.ToArray();
            }

            try
            {
                employee.ApplyForJob(jobOffer, cvBytes);
                await _db.Employees.UpdateEmployee(employee);
                await _db.CompleteAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task DeleteCertificateAsync(Guid userId, Guid certificateId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteCertificate(certificateId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }
        
        public async Task DeleteEducationAsync(Guid userId, Guid educationId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteEducation(educationId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task DeleteJobOfferFromFavouritesAsync(Guid userId, Guid offerId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteJobOfferFromFavourites(offerId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task DeleteLanguageAsync(Guid userId, Guid languageId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteLanguage(languageId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task DeleteProfessionalExperienceAsync(Guid userId, Guid professionalExperienceId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteProfessionalExperience(professionalExperienceId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task DeleteSkillAsync(Guid userId, Guid skillId)
        {
            var employee = await _db.Employees.GetByUserIdAsync(userId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.DeleteSkill(skillId);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditCertificateAsync(EditCertificateDto editCertificateDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(editCertificateDto.UserId);
            if (employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var certificate = Course.Create(editCertificateDto.Course.CourseName,
                editCertificateDto.Course.OrganizerName, editCertificateDto.Course.MonthOfObtain,
                editCertificateDto.Course.YearOfObtain);
            
            employee.EditCertificate(Guid.Parse(editCertificateDto.Course.CourseId), certificate);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditEmployeeLanguageAsync(EditEmployeeLanguageDto editEmployeeLanguageDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(editEmployeeLanguageDto.UserId);
            if (employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.EditLanguage(editEmployeeLanguageDto.LanguageId, editEmployeeLanguageDto.Level);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditEducationAsync(EditEducationDto editEducationDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(editEducationDto.UserId);
            if (employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var education = Education.Create(editEducationDto.Education.SchoolName,
                editEducationDto.Education.StartYear, editEducationDto.Education.EndYear,
                editEducationDto.Education.BranchOfStudy, editEducationDto.Education.Degree,
                editEducationDto.Education.Speciality);
            
            employee.EditEducation(Guid.Parse(editEducationDto.Education.EducationId), education);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditPersonalDataAsync(EditEmployeePersonalDataDto editEmployeePersonalDataDto)
        {
            var validateResult = _validatorService.Validate(editEmployeePersonalDataDto.EmployeePersonalData);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("EmployeePersonalDataDto is not valid", validateResult.GetErrors());
            
            var employee = await _db.Employees.GetByUserIdAsync(editEmployeePersonalDataDto.UserId);  
            if(employee == null)
                throw new ServerException("User with given id doesn't exist.");

            employee.EditPersonalData(editEmployeePersonalDataDto.EmployeePersonalData.Name,
                editEmployeePersonalDataDto.EmployeePersonalData.Surname,
                editEmployeePersonalDataDto.EmployeePersonalData.About,
                editEmployeePersonalDataDto.EmployeePersonalData.Sex,
                editEmployeePersonalDataDto.EmployeePersonalData.BirthDate,
                editEmployeePersonalDataDto.EmployeePersonalData.Country,
                editEmployeePersonalDataDto.EmployeePersonalData.City,
                editEmployeePersonalDataDto.EmployeePersonalData.Phone,
                editEmployeePersonalDataDto.EmployeePersonalData.WebsiteUrl);

            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditEmployeeSkillAsync(EditEmployeeSkillDto editEmployeeSkillDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(editEmployeeSkillDto.UserId);
            if (employee == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employee.EditSkill(editEmployeeSkillDto.SkillId, editEmployeeSkillDto.Level);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task EditProfessionalExperienceAsync(EditProfessionalExperienceDto editProfessionalExperienceDto)
        {
            var employee = await _db.Employees.GetByUserIdAsync(editProfessionalExperienceDto.UserId);
            if (employee == null)
                throw new ServerException("User with given id doesn't exist.");

            var professionalExperience = ProfessionalExperience.Create(
                editProfessionalExperienceDto.ProfessionalExperience.Position,
                editProfessionalExperienceDto.ProfessionalExperience.CompanyName,
                editProfessionalExperienceDto.ProfessionalExperience.StartMonth,
                editProfessionalExperienceDto.ProfessionalExperience.StartYear,
                editProfessionalExperienceDto.ProfessionalExperience.EndMonth,
                editProfessionalExperienceDto.ProfessionalExperience.EndYear,
                editProfessionalExperienceDto.ProfessionalExperience.Country,
                editProfessionalExperienceDto.ProfessionalExperience.City,
                editProfessionalExperienceDto.ProfessionalExperience.JobDescription);

            employee.EditProfessionalExperience(
                Guid.Parse(editProfessionalExperienceDto.ProfessionalExperience.ProfessionalExperienceId),
                professionalExperience);
            await _db.Employees.UpdateEmployee(employee);
            await _db.CompleteAsync();
        }

        public async Task<EmployeeDto> GetEmployeeByUserId(Guid id)
        {
            if (id == Guid.Empty)
                throw new ServerException("Id is not valid");

            var employee = await _db.Employees.GetByUserIdAsync(id);
            var employeeDto = _mapper.Map<EmployeeDto>(employee);

            return employeeDto;
        }

        public async Task<EmployerProfile> GetEmployerProfileByUserId(Guid id)
        {
            if (id == Guid.Empty)
                throw new ServerException("Id is not valid");

            var employer = await _db.Employers.GetByUserIdAsync(id);
            var employerDto = _mapper.Map<EmployerProfile>(employer);

            return employerDto;
        }
    }
}
