﻿using System.Threading.Tasks;

namespace justfinditApi.Infrastucture.Services
{
    public interface IEmailService
    {
        Task SendActivationEmail(string receiverEmail, string userId, string token);

        Task SendResetPasswordLink(string receiverEmail, string userId, string token);
    }
}
