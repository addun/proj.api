using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace justfinditApi.Infrastucture.Services
{
    public interface IJwtProvider
    {
        string CreateJwt(IEnumerable<Claim> claims);
        JwtSecurityToken DeserializeJwt(string token);
    }
}