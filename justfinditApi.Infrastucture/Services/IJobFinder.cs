using System;
using System.Collections.Generic;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Services
{
    public interface IJobFinder
    {
        IEnumerable<JobOfferDto> Find(List<string> skills, List<string> cities, string salary, string currency);
        JobOfferDto FindById(Guid offerId);
    }
}