﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace justfinditApi.Infrastucture.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;
        private readonly string _username;
        private readonly string _password;
        private readonly string _smtpClient;
        private readonly int _port;
        private readonly bool _useDefaultCredentials;
        private readonly bool _enableSsl;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
            _username = _configuration["EmailServiceCredentials:Username"];
            _password = _configuration["EmailServiceCredentials:Password"];
            _smtpClient = _configuration["EmailServiceCredentials:SmtpClient"];
            _port = Int32.Parse(_configuration["EmailServiceCredentials:Port"]);
            _useDefaultCredentials = Boolean.Parse(_configuration["EmailServiceCredentials:UseDefaultCredentials"]);
            _enableSsl = Boolean.Parse(_configuration["EmailServiceCredentials:EnableSsl"]);
        }

        public async Task SendActivationEmail(string receiverEmail, string userId, string token)
        {
            string activationLink =
                "http://localhost:4200/confirm-email/"
                + Uri.EscapeDataString(userId) + "/"
                + Uri.EscapeDataString(token);

            var smtpServer = CreateStmpClient();

            var mail = new MailMessage { From = new MailAddress("reg.justfindit@gmail.com") };
            mail.To.Add(receiverEmail);
            mail.Subject = "JustFindIT - Verification Email";
            mail.Body = "Klinkij w poniższy link aby dokonać aktywacji: "
                        + activationLink;

            await smtpServer.SendMailAsync(mail);
        }

        public async Task SendResetPasswordLink(string receiverEmail, string userId, string token)
        {
            string resetPasswordLink =
                "http://localhost:4200/reset-password/"
                + Uri.EscapeDataString(userId) + "/"
                + Uri.EscapeDataString(token);

            var smtpServer = CreateStmpClient();

            var mail = new MailMessage { From = new MailAddress("justfindit@gmail.com") };
            mail.To.Add(receiverEmail);
            mail.Subject = "JustFindIT - Reset password coinformation";
            mail.Body = "Klinkij w poniższy link aby zresetować hasło: "
                        + resetPasswordLink;

            await smtpServer.SendMailAsync(mail);
        }

        private SmtpClient CreateStmpClient()
        {
            var smtpServer = new SmtpClient(_smtpClient)
            {
                Port = _port,
                UseDefaultCredentials = _useDefaultCredentials,
                Credentials = new NetworkCredential(_username, _password),
                EnableSsl = _enableSsl
            };

            return smtpServer;
        }
    }
}
