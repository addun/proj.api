using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;

namespace justfinditApi.Infrastucture.Services
{
    public class EmployerService: IEmployerService
    {
        private readonly IUnitOfWork _db;
        private readonly IValidatorService _validatorService;
        private readonly IMapper _mapper;
        private readonly IPrincipalProvider _principalProvider;

        public EmployerService(IUnitOfWork db, IValidatorService validatorService, IMapper mapper, IPrincipalProvider principalProvider)
        {
            _db = db;
            _validatorService = validatorService;
            _mapper = mapper;
            _principalProvider = principalProvider;
        }
        
        public async Task<EmployerDto> GetEmployerByUserId(Guid id)
        {
            if (id == Guid.Empty)
                throw new ServerException("Id is not valid");

            var employer = await _db.Employers.GetByUserIdAsync(id);
            var employerDto = _mapper.Map<EmployerDto>(employer);

            return employerDto;
        }
        
        public async Task<EmployeeProfile> GetEmployeeProfileByUserId(Guid id)
        {
            if (id == Guid.Empty)
                throw new ServerException("Id is not valid");

            var employee = await _db.Employees.GetByUserIdAsync(id);
            var employeeDto = _mapper.Map<EmployeeProfile>(employee);

            return employeeDto;
        }

        public async Task<ResumeDto> GetApplicantCv(Guid employerId, Guid jobOfferId, Guid applicantId)
        {
            if (employerId == Guid.Empty)
                throw new ServerException("Employer id is not valid");
            
            if (applicantId == Guid.Empty)
                throw new ServerException("Applicant id is not valid");
            
            if (jobOfferId == Guid.Empty)
                throw new ServerException("Job offer is not valid");
            
            var employer = await _db.Employers.GetByUserIdAsync(employerId);
            if(employer == null)
                throw new ServerException("Employer does not exist");
            
            var jobOffer = employer.JobOffers.SingleOrDefault(x => x.OfferId == jobOfferId);
            if(jobOffer == null)
                throw new ServerException("Job offer does not exist");

            var jobApplication = jobOffer.JobApplication.SingleOrDefault(x => x.Employee.User.Id == applicantId);       
            if(jobApplication == null)
                throw new ServerException("CV does not exist");

            var resumeDto = new ResumeDto
            {
                FileName = jobApplication.Employee.Surname
                           + "_"
                           + jobApplication.JobOffer.Title
                           + "_resume",
                ContentType = jobApplication.ResumeExtension,
                ContentTypeShort = jobApplication.ResumeExtension.Split("/")[1],
                Content = jobApplication.Resume
            };

            return resumeDto;
        }

        public async Task AddBenefitAsync(AddEmployerBenefitDto addEmployerBenefitDto)
        {
            // TODO VALIDATE
            var employer = await _db.Employers.GetByUserIdAsync(addEmployerBenefitDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");
            
            var benefitToAdd = Benefit.Create(addEmployerBenefitDto.Benefit.Title,
                addEmployerBenefitDto.Benefit.Description);
            
            employer.AddBenefit(benefitToAdd);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task AddLocationAsync(AddEmployerLocationDto addEmployerLocationDto)
        {
            // TODO VALIDATE
            var employer = await _db.Employers.GetByUserIdAsync(addEmployerLocationDto.UserId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");

            var locationToAdd = EmployerLocation.Create(addEmployerLocationDto.Location.Country,
                addEmployerLocationDto.Location.City, addEmployerLocationDto.Location.Street,
                addEmployerLocationDto.Location.PostalCode, addEmployerLocationDto.Location.ApartmentNumber,
                addEmployerLocationDto.Location.FlatNumber);
            
            employer.AddLocation(locationToAdd);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task AddTechnologyAsync(AddTechnologyDto addTechnologyDto)
        {
            var employer = await _db.Employers.GetByUserIdAsync(addTechnologyDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");

            var technology = await _db.Technologies.GetTechnologyByNameAsync(addTechnologyDto.Technology.TechnologyName);
            if(technology == null)
                throw new ServerException("Technology doesn't exist.");
            
            employer.AddTechnology(technology);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task AddJobOfferAsync(AddJobOfferDto addJobOfferDto)
        {
            // TODO VALIDATE
            var employer = await _db.Employers.GetByUserIdAsync(addJobOfferDto.UserId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");
            
            var jobOfferToAdd = JobOffer.Create(addJobOfferDto.JobOffer.Title, addJobOfferDto.JobOffer.Country,
                addJobOfferDto.JobOffer.City, addJobOfferDto.JobOffer.Address, addJobOfferDto.JobOffer.SalaryFrom,
                addJobOfferDto.JobOffer.SalaryTo, addJobOfferDto.JobOffer.Currency, addJobOfferDto.JobOffer.WorkTime,
                addJobOfferDto.JobOffer.InvoiceType,addJobOfferDto.JobOffer.ExperienceLevel, 
                addJobOfferDto.JobOffer.OfferDescription, addJobOfferDto.JobOffer.Remote);

            var technologiesFromDb = _db.Technologies.GetAllAsync().Result.ToList();
            var technologiesToAdd = new List<Technology>();
            
            foreach (var skillName in addJobOfferDto.JobOffer.Skills)
            {
                var tempTechnology = technologiesFromDb.SingleOrDefault(x => x.TechnologyName.ToLower() == skillName.ToLower());
                
                if (tempTechnology != null)
                {
                    technologiesToAdd.Add(tempTechnology);
                }
            }
            //save technologies
            await _db.CompleteAsync();
            
            jobOfferToAdd.AddSkills(technologiesToAdd);
            employer.AddJobOffer(jobOfferToAdd);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task DeleteBenefitAsync(Guid employerId, Guid benefitId)
        {
            var employer = await _db.Employers.GetByUserIdAsync(employerId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");
            
            employer.DeleteBenefit(benefitId);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task DeleteLocationAsync(Guid employerId, Guid locationId)
        {
            var employer = await _db.Employers.GetByUserIdAsync(employerId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");
            
            employer.DeleteLocation(locationId);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task DeleteJobOfferAsync(Guid userId, Guid jobOfferId)
        {
            var employer = await _db.Employers.GetByUserIdAsync(userId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");
            
            employer.DeleteJobOffer(jobOfferId);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task DeleteTechnologyAsync(Guid employerId, Guid technologyId)
        {
            var employer = await _db.Employers.GetByUserIdAsync(employerId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");
            
            employer.DeleteTechnology(technologyId);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditBenefitAsync(EditEmployerBenefitDto editEmployerBenefitDto)
        {
            // TODO VALIDATE
            
            var employer = await _db.Employers.GetByUserIdAsync(editEmployerBenefitDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");

            var editedBenefit = Benefit.Create(editEmployerBenefitDto.Benefit.Title,
                editEmployerBenefitDto.Benefit.Description);
            
            employer.EditBenefit(Guid.Parse(editEmployerBenefitDto.Benefit.BenefitId), editedBenefit);

            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditEmployerDataAsync(EditEmployerDataDto editEmployerDataDto)
        {
            var validateResult = _validatorService.Validate(editEmployerDataDto.EmployerData);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("EditEmployerDataDto is not valid", validateResult.GetErrors());
            
            var employer = await _db.Employers.GetByUserIdAsync(editEmployerDataDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");

            employer.EditEmployerData(editEmployerDataDto.EmployerData.CompanyName,
                editEmployerDataDto.EmployerData.CompanyDescription, editEmployerDataDto.EmployerData.FoundedYear,
                editEmployerDataDto.EmployerData.CompanySize, editEmployerDataDto.EmployerData.Nip,
                editEmployerDataDto.EmployerData.WebsiteUrl);

            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditJobOfferAsync(EditJobOfferDto editJobOfferDto)
        {
            // TODO VALIDATE
            
            var employer = await _db.Employers.GetByUserIdAsync(editJobOfferDto.UserId);  
            if(employer == null)
                throw new ServerException("Employer with given id doesn't exist.");

            var jobOffer = JobOffer.Create(editJobOfferDto.JobOffer.Title, editJobOfferDto.JobOffer.Country,
                editJobOfferDto.JobOffer.City, editJobOfferDto.JobOffer.Address, editJobOfferDto.JobOffer.SalaryFrom,
                editJobOfferDto.JobOffer.SalaryTo, editJobOfferDto.JobOffer.Currency, editJobOfferDto.JobOffer.WorkTime,
                editJobOfferDto.JobOffer.InvoiceType,editJobOfferDto.JobOffer.ExperienceLevel, 
                editJobOfferDto.JobOffer.OfferDescription, editJobOfferDto.JobOffer.Remote);
            
            var technologiesFromDb = _db.Technologies.GetAllAsync().Result.ToList();
            var technologiesToAdd = new List<Technology>();
            
            foreach (var skillName in editJobOfferDto.JobOffer.Skills)
            {
                var technologyToAdd = Technology.Create(skillName);
                var tempTechnology = technologiesFromDb.SingleOrDefault(x => x.TechnologyName == skillName);
                
                if (tempTechnology == null)
                {
                    await _db.Technologies.AddTechnologyAsync(technologyToAdd);
                }
                else
                {
                    technologyToAdd = tempTechnology;
                }
                
                technologiesToAdd.Add(technologyToAdd);
            }
            //save technologies
            await _db.CompleteAsync();
            
            jobOffer.AddSkills(technologiesToAdd);
            employer.EditJobOffer(Guid.Parse(editJobOfferDto.JobOffer.OfferId), jobOffer);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditLocationAsync(EditEmployerLocationDto editEmployerLocationDto)
        {
            // TODO VALIDATE
            
            var employer = await _db.Employers.GetByUserIdAsync(editEmployerLocationDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");

            var editedLocation = EmployerLocation.Create(editEmployerLocationDto.Location.Country,
                editEmployerLocationDto.Location.City, editEmployerLocationDto.Location.Street,
                editEmployerLocationDto.Location.PostalCode, editEmployerLocationDto.Location.ApartmentNumber,
                editEmployerLocationDto.Location.FlatNumber);
            
            employer.EditLocation(Guid.Parse(editEmployerLocationDto.Location.EmployerLocationId), 
                editedLocation);

            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }


        public async Task EditInternshipDescription(EditInternshipDescriptionDto editInternshipDescriptionDto)
        {
            var employer = await _db.Employers.GetByUserIdAsync(editInternshipDescriptionDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employer.EditInternshipDescription(editInternshipDescriptionDto.Description);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task SetHeadquartersAsync(SetHeadquartersDto setHeadquartersDto)
        {
            var employer = await _db.Employers.GetByUserIdAsync(setHeadquartersDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employer.SetAsHeadquarters(setHeadquartersDto.LocationId);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditWhatsWeCreatingDescription(EditWhatsWeCreatingDescriptionDto editWhatsWeCreatingDescriptionDto)
        {
            var employer = await _db.Employers.GetByUserIdAsync(editWhatsWeCreatingDescriptionDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employer.EditWhatWeCreatingDescription(editWhatsWeCreatingDescriptionDto.Description);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

        public async Task EditWorkingWithUsDescription(EditWorkingWithUsDescriptionDto editWorkingWithUsDescriptionDto)
        {
            var employer = await _db.Employers.GetByUserIdAsync(editWorkingWithUsDescriptionDto.UserId);  
            if(employer == null)
                throw new ServerException("User with given id doesn't exist.");
            
            employer.EditWhyWorthWorkingWithUs(editWorkingWithUsDescriptionDto.Description);
            await _db.Employers.UpdateEmployer(employer);
            await _db.CompleteAsync();
        }

    }
}