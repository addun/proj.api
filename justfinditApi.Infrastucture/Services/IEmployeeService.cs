﻿using System;
using System.Threading.Tasks;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Services
{
    public interface IEmployeeService
    {
        Task AddCertificateAsync(AddCertificateDto addCertificateDto);
        Task AddEducationAsync(AddEducationDto addEducationDto);
        Task AddJobOfferToFavouritesAsync(AddJobOfferToFavouritesDto addEducationDto);
        Task AddLanguageAsync(AddLanguageDto addLanguageDto);
        Task AddSkillAsync(AddSkillDto addSkillDto);   
        Task AddProfessionalExperienceAsync(AddProfessionalExperienceDto addProfessionalExperienceDto);
        Task ApplyForJobAsync(ApplyForJobDto applyForJobDto);
        Task DeleteCertificateAsync(Guid userId, Guid certificateId);
        Task DeleteEducationAsync(Guid userId, Guid educationId);
        Task DeleteJobOfferFromFavouritesAsync(Guid userId, Guid offerId);
        Task DeleteLanguageAsync(Guid userId, Guid languageId);
        Task DeleteProfessionalExperienceAsync(Guid userId, Guid professionalExperienceId);
        Task DeleteSkillAsync(Guid userId, Guid skillId);
        Task EditProfessionalExperienceAsync(EditProfessionalExperienceDto editProfessionalExperienceDto);
        Task EditEducationAsync(EditEducationDto editEducationDto);
        Task EditCertificateAsync(EditCertificateDto editCertificateDto);
        Task EditEmployeeLanguageAsync(EditEmployeeLanguageDto editEmployeeLanguageDto);
        Task EditPersonalDataAsync(EditEmployeePersonalDataDto editEmployeePersonalDataDto);
        Task EditEmployeeSkillAsync(EditEmployeeSkillDto editEmployeeSkillDto);
        Task<EmployeeDto> GetEmployeeByUserId(Guid id);
        Task<EmployerProfile> GetEmployerProfileByUserId(Guid id);
    }
}
