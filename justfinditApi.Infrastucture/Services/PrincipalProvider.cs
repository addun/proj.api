﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;

namespace justfinditApi.Infrastucture.Services
{
    public class PrincipalProvider : IPrincipalProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PrincipalProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IPrincipal User => _httpContextAccessor.HttpContext?.User;

        public IEnumerable<Claim> GetUserClaims()
        {
            return _httpContextAccessor.HttpContext.User.Claims;
        }

        public string GetUserId()
        {
            var userIdClaim
                = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier);

            return userIdClaim?.Value;
        }

        public string GetAuthToken()
        {
            var operationSuccess = 
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("Authorization", out var token);

            return operationSuccess ? token.ToString() : null;
        }
    }
}
