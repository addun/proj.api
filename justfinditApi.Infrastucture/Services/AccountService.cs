﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using justfinditApi.Core.Domain;
using justfinditApi.Core.DomainServices;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Configurations;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;

namespace justfinditApi.Infrastucture.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _db;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IPrincipalProvider _principalProvider;
        private readonly IValidatorService _validatorService;
        private readonly IMapper _mapper;

        public AccountService(
            IUnitOfWork db, 
            IUserService userService,
            IEmailService emailService,
            IPrincipalProvider principalProvider,
            IValidatorService validatorService,
            IMapper mapper)
        {
            _db = db;
            _userService = userService;
            _emailService = emailService;
            _principalProvider = principalProvider;
            _validatorService = validatorService;
            _mapper = mapper;
        }

        public async Task ConfirmEmail(ConfirmEmailDto confirmEmailDto)
        {
            var validateResult = _validatorService.Validate(confirmEmailDto);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("ConfirmEmailDto is not valid");

            var user = await _db.Users.GetUserByIdAsync(confirmEmailDto.UserId);
            if (user == null)
            {
                throw new ServerException("User with given id doesn't exist.");
            }

            await _userService.ConfirmEmail(user, confirmEmailDto.Token);
            _db.Users.UpdateUser(user);
            await _db.CompleteAsync();
        }

        public async Task ForgotPassword(ForgotPasswordDto forgotPassword)
        {
            var validateResult = _validatorService.Validate(forgotPassword);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("ForgotPasswordDto is not valid", validateResult.GetErrors());

            var user = await _db.Users.GetUserByEmailAsync(forgotPassword.Email);
            if(user == null)
                throw new ServerException("User does not exist", nameof(forgotPassword.Email),
                        "Użytkownik o podanym emailu nie istnieje.");
            
            if (!user.EmailConfirmed)
                throw new ServerException("User has no email confirmed", nameof(forgotPassword.Email),
                    "Użytkownik nie potwierdził swojego adresu email.");

            var securityToken = await _userService.GeneratePasswordResetTokenAsync(user);
            _db.Users.UpdateUser(user);
            await _db.CompleteAsync();
            
            await _emailService.SendResetPasswordLink(user.Email, user.Id.ToString(), securityToken);
        }

        public async Task<AuthTokenDto> GetEmployeeToken(string authorizationHeader)
        {
            string[] credentialsResult = GetCredentialsFromAuthorizationHeader(authorizationHeader);

            var employee = await _db.Employees.GetByEmailAsync(credentialsResult[0]);
            if (employee == null)
                throw new ServerException("Authorization error.", "Unauthorized", "Nieprawidłowy email lub hasło");

            var areCredentialsValid = await _userService.CheckPasswordAsync(employee.User, credentialsResult[1]);
            if (!areCredentialsValid)
            {
                throw new ServerException("Authorization error.", "Unauthorized", "Nieprawidłowy email lub hasło");
            }

            var refreshToken = await _userService.GenerateRefreshTokenForUserAsync(employee.User);
            var token = await _userService.TryGenerateJwtTokenAsync(employee.User, credentialsResult[1]);
            
            return AuthTokenDto.Create(token, refreshToken);
        }

        public async Task<AuthTokenDto> GetEmployerToken(string authorizationHeader)
        {
            string[] credentialsResult = GetCredentialsFromAuthorizationHeader(authorizationHeader);

            var employer = await _db.Employers.GetByEmailAsync(credentialsResult[0]);
            if (employer == null)
                throw new ServerException("Authorization error.", "Unauthorized", "Nieprawidłowy email lub hasło");

            var areCredentialsValid = await _userService.CheckPasswordAsync(employer.User, credentialsResult[1]);
            if (!areCredentialsValid)
            {
                throw new ServerException("Authorization error.", "Unauthorized", "Nieprawidłowy email lub hasło");
            }

            var refreshToken = await _userService.GenerateRefreshTokenForUserAsync(employer.User);
            var token = await _userService.TryGenerateJwtTokenAsync(employer.User, credentialsResult[1]);
            
            return AuthTokenDto.Create(token, refreshToken);
        }

        public async Task<AuthTokenDto> RefreshToken(RefreshTokenDto refreshToken)
        {
            var jwt = _userService.DeserializeJwt(refreshToken.AuthToken);
            var userIdClaim = jwt.Claims.SingleOrDefault(x => x.Type == ClaimTypes.NameIdentifier);

            var user = await _db.Users.GetUserByIdAsync(Guid.Parse(userIdClaim?.Value));
            if (user.RefreshToken != refreshToken.RefreshToken)
                throw new ServerException("Refresh token operation failed");

            var newAuthToken = await _userService.TryRefreshTokenAsync(user, refreshToken.RefreshToken);
            var newRefreshToken = await _userService.GenerateRefreshTokenForUserAsync(user);
            
            return AuthTokenDto.Create(newAuthToken, newRefreshToken);
        }

        public async Task<EmployeeDto> RegisterEmployee(RegisterEmployeeDto employee)
        {
            var validateResult = _validatorService.Validate(employee);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("RegisterEmployeeDto is not valid", validateResult.GetErrors());

            await _db.BeginTransactionAsync();
            var newUser = await _userService.CreateUser(employee.Email, employee.Password);
            await _userService.GenerateEmailConfirmationTokenAsync(newUser);

            var newEmployee = 
                new Employee(employee.Name, employee.Surname, employee.Sex, employee.BirthDate, employee.About, employee.Phone, employee.WebsiteUrl, employee.Country,employee.City, newUser);
            await _userService.AddUserToRoleAsync(newUser, ApplicationRoles.Employee);

            await _db.Employees.AddAsync(newEmployee);
            await _db.CompleteAsync();

            await _emailService.SendActivationEmail(newUser.Email, newUser.Id.ToString(), newUser.VerificationCode);
            var employeeDto = _mapper.Map<Employee, EmployeeDto>(newEmployee);
            _db.CommitTransaction();

            return employeeDto;
        }

        public async Task<EmployerDto> RegisterEmployer(RegisterEmployerDto employer)
        {
            var validateResult = _validatorService.Validate(employer);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("RegisterEmployerDto is not valid", validateResult.GetErrors());

            await _db.BeginTransactionAsync();
            var newUser = await _userService.CreateUser(employer.Email, employer.Password);
            await _userService.GenerateEmailConfirmationTokenAsync(newUser);
            await _userService.AddUserToRoleAsync(newUser, ApplicationRoles.Employer);

            var mainCompanyLocation = EmployerLocation.Create(employer.Country, employer.City, employer.Street,
                employer.PostalCode, employer.ApartmentNumber, employer.FlatNumber);

            var newEmployer = new Employer(employer.CompanyName, employer.CompanyDescription,
                employer.FoundedYear, employer.CompanySize, employer.Nip, employer.WebsiteUrl, mainCompanyLocation,
                newUser);

            await _db.Employers.AddAsync(newEmployer);
            await _db.CompleteAsync();
            
            await _emailService.SendActivationEmail(newUser.Email, newUser.Id.ToString(), newUser.VerificationCode);
            var employerDto = _mapper.Map<Employer, EmployerDto>(newEmployer);
            _db.CommitTransaction();

            return employerDto;
        }

        public async Task ResendEmail()
        {
            var userId = _principalProvider.GetUserId();
            var user = await _db.Users.GetUserByIdAsync(Guid.Parse(userId));
            if (user == null)
            {
                throw new ServerException("User with given id doesn't exist.");
            }

            var newVerificationCode = await _userService.GenerateEmailConfirmationTokenAsync(user);
            _db.Users.UpdateUser(user);
            await _db.CompleteAsync();
            
            await _emailService.SendActivationEmail(user.Email, user.Id.ToString(), newVerificationCode);
        }

        public async Task ResetPassword(ResetPasswordDto resetPasswordDto)
        {
            var validateResult = _validatorService.Validate(resetPasswordDto);
            if (!validateResult.IsValid)
                throw new ServerException
                    ("ResetPasswordDto is not valid", validateResult.GetErrors());

            var user = await _db.Users.GetUserByIdAsync(resetPasswordDto.UserId);
            if (user == null)
                throw new ServerException("User with given id doesn't exist.");

            if (!await _userService.ResetPassword(user, resetPasswordDto.NewPassword, resetPasswordDto.Token))
                return;
            
            _db.Users.UpdateUser(user);
            await _db.CompleteAsync();
        }

        private string[] GetCredentialsFromAuthorizationHeader(string header)
        {
            if (!header.StartsWith("Basic"))
                throw new ServerException("Can't find basic authentication header.");

            var credentialsInB64 = header.Substring("Basic ".Length).Trim();
            string[] encodedCredentials = EncodeCredentials(credentialsInB64);

            if(encodedCredentials.Length != 2)
                throw new ServerException("Error during parsing base64 string to credentials.");

            return encodedCredentials;
        }

        private string[] EncodeCredentials(string credentials)
        {
            var base64EncodedCredentials = Convert.FromBase64String(credentials);
            var stringCredentials = System.Text.Encoding.UTF8.GetString(base64EncodedCredentials);

            return stringCredentials.Split(new [] { ':' });
        }
    }
}
