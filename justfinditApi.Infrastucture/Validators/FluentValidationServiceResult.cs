﻿using System.Collections.Generic;
using System.Linq;

namespace justfinditApi.Infrastucture.Validators
{
    public class FluentValidationServiceResult
    {
        public bool IsValid {
            get { return !this._errors.Any(); }
        }
        private Dictionary<string, List<string>> _errors;

        private FluentValidationServiceResult(Dictionary<string, List<string>> errors)
        {
            this._errors = errors;
        }

        public Dictionary<string, List<string>> GetErrors()
        {
            return _errors;
        }

        public static FluentValidationServiceResult Create(Dictionary<string, List<string>> errors)
        {
            return new FluentValidationServiceResult(errors);
        }
    }
}
