﻿using FluentValidation;

namespace justfinditApi.Infrastucture.Validators.ValidatorResolver
{
    public interface IValidatorResolver
    {
        IValidator Resolve<T>();
    }
}
