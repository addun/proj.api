﻿using Autofac;
using FluentValidation;

namespace justfinditApi.Infrastucture.Validators.ValidatorResolver
{
    public class ValidatorResolver : IValidatorResolver
    {
        private readonly IComponentContext _context;

        public ValidatorResolver(IComponentContext context)
        {
            _context = context;
        }

        public IValidator Resolve<T>()
        {
            var validator = _context.Resolve<AbstractValidator<T>>();
            return validator;
        }
    }
}
