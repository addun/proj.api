﻿using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class RegisterEmployerDtoValidator: AbstractValidator<RegisterEmployerDto>
    {
        public RegisterEmployerDtoValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email jest wymagany")
                .EmailAddress().WithMessage("Email jest nieprawidłowy");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Hasło nie może być puste");

            RuleFor(x => x.CompanyName)
                .NotEmpty().WithMessage("Należy podać nazwę firmy");

            RuleFor(x => x.FoundedYear)
                .NotEmpty().WithMessage("Należy podać rok założenia");

            RuleFor(x => x.CompanySize)
                .NotEmpty().WithMessage("Należy podać przybliżoną liczbę pracowników");

            RuleFor(x => x.Nip)
                .NotEmpty().WithMessage("Należy podać NIP");

            RuleFor(x => x.WebsiteUrl)
                .NotEmpty().WithMessage("Należy podać adres strony internetowej firmy");

            RuleFor(x => x.Street)
                .NotEmpty().WithMessage("Należy podać ulicę");

            RuleFor(x => x.ApartmentNumber)
                .NotEmpty().WithMessage("Należy podać numer domu");

            RuleFor(x => x.Country)
                .NotEmpty().WithMessage("Należy podać kraj");

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("Należy podać miasto");

            RuleFor(x => x.PostalCode)
                .NotEmpty().WithMessage("Należy podać kod pocztowy");
        }
    }
}
