﻿using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class RegisterEmployeeDtoValidator: AbstractValidator<RegisterEmployeeDto>
    {
        public RegisterEmployeeDtoValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email jest wymagany")
                .EmailAddress().WithMessage("Email jest nieprawidłowy");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Hasło nie może być puste");

            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("Należy podać imię");

            RuleFor(x => x.Surname)
                .NotEmpty().WithMessage("Należy podać nazwisko");

            RuleFor(x => x.Sex)
                .NotEmpty().WithMessage("Należy podać płeć");

            RuleFor(x => x.BirthDate)
                .NotEmpty().WithMessage("Należy podać datę urodzenia");

            RuleFor(x => x.Country)
                .NotEmpty().WithMessage("Należy podać kraj");

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("Należy podać miasto");
        }
    }
}
