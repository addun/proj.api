﻿using System.Collections.Generic;
using FluentValidation.Results;

namespace justfinditApi.Infrastucture.Validators.Extensions
{
    public static class ValidatorExtension
    {
        public static Dictionary<string, List<string>> GetErrorsInDictionaryFormat(this ValidationResult validationResult)
        {
            var result = new Dictionary<string, List<string>>();
            foreach (var error in validationResult.Errors)
            {
                if (!result.ContainsKey(error.PropertyName.ToLower()))
                    result.Add(error.PropertyName.ToLower(), new List<string>());

                result[error.PropertyName.ToLower()].Add(error.ErrorMessage);
            }

            return result;
        }
    }
}
