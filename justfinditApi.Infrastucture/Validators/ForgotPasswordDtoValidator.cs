﻿using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class ForgotPasswordDtoValidator: AbstractValidator<ForgotPasswordDto>
    {
        public ForgotPasswordDtoValidator()
        {
            RuleFor(x => x.Email)
                .Configure(opt => opt.CascadeMode = CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("Email nie może być pusty")
                .EmailAddress().WithMessage("Nieprawidłowy adres email.");
        }
    }
}
