using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class EmployeePersonalDataDtoValidator: AbstractValidator<EmployeePersonalDataDto>
    {
        public EmployeePersonalDataDtoValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage((emp, s) 
                    => $"{nameof(emp.Name)} should not be empty");
            
            RuleFor(x => x.Surname)
                .NotEmpty().WithMessage((emp, s) 
                    => $"{nameof(emp.Surname)} should not be empty");

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage((emp, s)
                    => $"{nameof(emp.Email)} should not be empty")
                .EmailAddress().WithMessage((emp, s)
                    => $"{nameof(emp.Email)} is invalid");
            
            RuleFor(x => x.Country)
                .NotEmpty().WithMessage((emp, s) 
                    => $"{nameof(emp.Country)} should not be empty");
            
            RuleFor(x => x.City)
                .NotEmpty().WithMessage((emp, s) 
                    => $"{nameof(emp.City)} should not be empty");
            
            RuleFor(x => x.BirthDate)
                .NotEmpty().WithMessage((emp, s) 
                    => $"{nameof(emp.BirthDate)} should not be empty");
        }
    }
}