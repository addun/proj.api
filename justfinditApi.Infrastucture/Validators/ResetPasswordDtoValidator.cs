﻿using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class ResetPasswordDtoValidator: AbstractValidator<ResetPasswordDto>
    {
        public ResetPasswordDtoValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("Id użytkownika nie może być puste");

            RuleFor(x => x.Token)
                .NotEmpty().WithMessage("Token nie może być pusty");

            RuleFor(x => x.NewPassword)
                .NotEmpty().WithMessage("Hasło nie może być puste");

            RuleFor(x => x.NewPasswordConfirm)
                .NotEmpty().WithMessage("Potwierdzenie hasła nie może być puste")
                .Equal(x=>x.NewPassword).WithMessage("Podane hasła nie pasują do siebie.");
        }
    }
}
