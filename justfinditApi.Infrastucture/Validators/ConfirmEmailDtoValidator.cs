﻿using FluentValidation;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Validators
{
    public class ConfirmEmailDtoValidator: AbstractValidator<ConfirmEmailDto>
    {
        public ConfirmEmailDtoValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("Id użytkownika nie może być puste");

            RuleFor(x => x.Token)
                .NotEmpty().WithMessage("Token nie może być pusty");
        }
    }
}
