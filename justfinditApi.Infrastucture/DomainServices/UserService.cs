using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using justfinditApi.Core.Domain;
using justfinditApi.Core.DomainServices;
using justfinditApi.Infrastucture.Configurations;
using justfinditApi.Infrastucture.Exceptions;
using justfinditApi.Infrastucture.Extensions;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace justfinditApi.Infrastucture.DomainServices
{
    public class UserService: IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IJwtProvider _jwtProvider;

        public UserService(UserManager<User> userManager, RoleManager<Role> roleManager, IJwtProvider jwtProvider)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtProvider = jwtProvider;
        }
        
        public async Task<User> CreateUser(string email, string password)
        {
            var newUser = new User { UserName = email, Email = email };
            var userCreationResult = await _userManager.CreateAsync(newUser, password);

            if (!userCreationResult.Succeeded)
            {
                var userCreationErrors = userCreationResult.GetIdentityResultErrors();
                throw new ServerException("Error during create User account.", userCreationErrors);
            }
            
            return newUser;
        }

        public async Task ConfirmEmail(User user, string token)
        {
            if (!await _userManager.VerifyUserTokenAsync(user, TokenOptions.DefaultProvider, ApplicationTokenPurposes.EmailConfirmation,
                token))
            {
                throw new ServerException("Invalid verification code.");
            }

            if (token != user.VerificationCode)
            {
                throw new ServerException("Invalid verification code.");
            }

            user.EmailConfirmed = true;
        }

        public JwtSecurityToken DeserializeJwt(string token)
        {
            return _jwtProvider.DeserializeJwt(token);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            if (!user.EmailConfirmed)
                throw new ServerException("User has no email confirmed");

            var securityToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            user.LastGeneratedSecurityToken = securityToken;

            return securityToken;
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            var securityToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            user.VerificationCode = securityToken;

            return securityToken;
        }

        public async Task<string> TryGenerateJwtTokenAsync(User user, string password)
        {
            if (!await CheckPasswordAsync(user, password))
            {
                throw new SecurityTokenException("User is not authenticated to generate auth token");
            }
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Email, user.Email),
            };

            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (string userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }
   
            return _jwtProvider.CreateJwt(claims);
        }

        public async Task<string> TryRefreshTokenAsync(User user, string refreshToken)
        {
            if (user.RefreshToken != refreshToken)
            {
                throw new SecurityTokenException("Refresh token is invalid. Generating token failed.");
            }
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Email, user.Email),
            };

            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (string userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }
   
            return _jwtProvider.CreateJwt(claims);
        }

        public async Task<string> GenerateRefreshTokenForUserAsync(User user)
        {   
            //GENERATE REFRESH TOKEN
            var refreshToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            
            //SAVE REFRESH TOKEN
            user.RefreshToken = refreshToken;
            await _userManager.UpdateAsync(user);
            
            return refreshToken;
        }

        public async Task<bool> CheckPasswordAsync(User user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);          
        }

        public async Task<bool> ResetPassword(User user, string newPassword, string token)
        {
            if (!await _userManager.VerifyUserTokenAsync(user, TokenOptions.DefaultProvider, ApplicationTokenPurposes.ResetPassword,
                token))
                throw new ServerException("Invalid verification code.");

            if (token != user.LastGeneratedSecurityToken)
                throw new ServerException("Invalid verification code.");
                    
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);

            if(!result.Succeeded)
                throw new ServerException
                    ("Error during reseting password", result.GetIdentityResultErrors(ApplicationIdentityErrorCodesOptions.Create("email", "newPassword")));
            
            user.LastGeneratedSecurityToken = null;
            return true;
        }

        public async Task AddUserToRoleAsync(User user, string roleName)
        {
            if (!await _roleManager.RoleExistsAsync(roleName))
            {
                var role = new Role();
                role.Name = roleName;
                await _roleManager.CreateAsync(role);
            }

            var result = await _userManager.AddToRoleAsync(user, roleName);
            if (!result.Succeeded)
            {
                throw new ServerException("Error during adding user to role");
            }
        }
    }
}