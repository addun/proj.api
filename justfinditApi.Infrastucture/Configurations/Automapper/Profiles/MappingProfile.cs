﻿using System.Linq;
using AutoMapper;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Dtos;

namespace justfinditApi.Infrastucture.Configurations.Automapper.Profiles
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Employee, EmployeeDto>()
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.User.Email))
                .ForMember(x => x.BirthDate, opt => opt.MapFrom(x => x.BirthDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.EmailConfirmed, opt => opt.MapFrom(x => x.User.EmailConfirmed))
                .ForMember(x => x.JobExperience, opt => opt.MapFrom(x => x.ProfessionalExperience))
                .ForMember(x => x.EmployeeEducation, opt => opt.MapFrom(x => x.Education))
                .ForMember(x => x.EmployeeCertificates, opt => opt.MapFrom(x => x.Courses))
                .ForMember(x => x.Applications, opt => opt.MapFrom(x => x.JobApplications.Select(y => y.JobOffer)))
                .ForMember(x => x.Favourites, opt => opt.MapFrom(x => x.Favourites.Select(y => y.JobOffer)));
                

            CreateMap<Employee, EmployeeProfile>()
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.User.Email))
                .ForMember(x => x.BirthDate, opt => opt.MapFrom(x => x.BirthDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.EmailConfirmed, opt => opt.MapFrom(x => x.User.EmailConfirmed))
                .ForMember(x => x.JobExperience, opt => opt.MapFrom(x => x.ProfessionalExperience))
                .ForMember(x => x.EmployeeEducation, opt => opt.MapFrom(x => x.Education))
                .ForMember(x => x.EmployeeCertificates, opt => opt.MapFrom(x => x.Courses));
            
            CreateMap<Employer, EmployerDto>()
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.User.Email))
                .ForMember(x => x.EmailConfirmed, opt => opt.MapFrom(x => x.User.EmailConfirmed));
            
            CreateMap<Employer, EmployerProfile>()
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.User.Email))
                .ForMember(x => x.EmailConfirmed, opt => opt.MapFrom(x => x.User.EmailConfirmed));
                
            CreateMap<EmployeeLanguage, LanguageDto>()
                .ForMember(x=>x.LanguageId, opt => opt.MapFrom(x=>x.LanguageId))
                .ForMember(x => x.LanguageName, opt => opt.MapFrom(x => x.Language.LanguageName))
                .ForMember(x => x.Level, opt => opt.MapFrom(x => x.Level));
            
            CreateMap<EmployeeSkill, SkillDto>()
                .ForMember(x => x.SkillId, opt => opt.MapFrom(x=>x.TechnologyId))
                .ForMember(x => x.SkillName, opt => opt.MapFrom(x => x.Technology.TechnologyName))
                .ForMember(x => x.Level, opt => opt.MapFrom(x => x.Level));

            CreateMap<ProfessionalExperience, ProfessionalExperienceDto>()
                .ForMember(x => x.StartMonth, opt => opt.MapFrom(x => x.StartDate.ToString("MM")))
                .ForMember(x => x.StartYear, opt => opt.MapFrom(x => x.StartDate.ToString("yyyy")))    
                .ForMember(x => x.EndMonth, 
                    opt => opt.MapFrom(x =>x.EndDate.HasValue ? x.EndDate.Value.ToString("MM") : null ))
                .ForMember(x => x.EndYear, 
                    opt => opt.MapFrom(x =>x.EndDate.HasValue ? x.EndDate.Value.ToString("yyyy") : null ));

            CreateMap<Course, CourseDto>()
                .ForMember(x => x.MonthOfObtain, opt => opt.MapFrom(x => x.DateOfObtain.ToString("MM")))
                .ForMember(x => x.YearOfObtain, opt => opt.MapFrom(x => x.DateOfObtain.ToString("yyyy")));

            CreateMap<Education, EducationDto>();
            
            CreateMap<Benefit, BenefitDto>();
            
            CreateMap<EmployerLocation, EmployerLocationDto>();
            
            CreateMap<EmployerTechnology, TechnologyDto>()
                .ForMember(x=>x.TechnologyName, opt => opt.MapFrom(x=>x.Technology.TechnologyName));

            CreateMap<JobOffer, JobOfferDto>()
                .ForMember(x=>x.Employer, opt => opt.MapFrom(x=>x.Employer))
                .ForMember(x => x.Skills,
                    opt => opt.MapFrom(x => x.Skills.Select(p => p.Technology.TechnologyName).ToList()))
                .ForMember(x => x.CreationDate, opt => opt.MapFrom(x => x.CreationDate.ToString("yyyy-MM-dd")));

            CreateMap<JobOffer, EmployerJobOfferDto>()
                .ForMember(x => x.Employer, opt => opt.MapFrom(x => x.Employer))
                .ForMember(x => x.Skills,
                    opt => opt.MapFrom(x => x.Skills.Select(p => p.Technology.TechnologyName).ToList()))
                .ForMember(x => x.CreationDate, opt => opt.MapFrom(x => x.CreationDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.Applicants, opt => opt.MapFrom(x => x.JobApplication.Select(y => y.Employee)));

            CreateMap<Employee, ApplicantShortDto>()
                .ForMember(x=>x.Id, opt => opt.MapFrom(x=>x.User.Id))
                .ForMember(x=>x.Email, opt => opt.MapFrom(x=>x.User.Email));
        }
    }

}
