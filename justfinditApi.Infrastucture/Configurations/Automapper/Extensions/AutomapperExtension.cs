﻿using AutoMapper;

namespace justfinditApi.Infrastucture.Configurations.Automapper.Extensions
{
    public static class AutomapperExtension
    {
        public static TDestination MapFromSources<TSource1, TSource2, TDestination>(this IMapper mapper, TSource1 source1, TSource2 source2)
        {
            return mapper.Map(source2, mapper.Map<TDestination>(source1));
        }
    }
}
