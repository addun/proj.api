﻿namespace justfinditApi.Infrastucture.Configurations
{
    public class ApplicationIdentityErrorCodesOptions
    {
        public string EmailErrorKeyName { get; private set; }
        public string PasswordErrorKeyName { get; private set; }

        private ApplicationIdentityErrorCodesOptions(string emailErrorKeyName, string passwordErrorKeyName)
        {
            this.EmailErrorKeyName = emailErrorKeyName;
            this.PasswordErrorKeyName = passwordErrorKeyName;
        }

        public static ApplicationIdentityErrorCodesOptions Create(string emailErrorKeyName, string passwordErrorKeyName)
        {
            return new ApplicationIdentityErrorCodesOptions(emailErrorKeyName, passwordErrorKeyName);
        }
    }
}
