﻿namespace justfinditApi.Infrastucture.Configurations
{
    public static class ApplicationTokenPurposes
    {
        public const string ResetPassword = "ResetPassword";
        public const string EmailConfirmation = "EmailConfirmation";
    }
}
