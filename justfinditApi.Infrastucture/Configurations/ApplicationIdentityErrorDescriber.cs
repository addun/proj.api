﻿using Microsoft.AspNetCore.Identity;

namespace justfinditApi.Infrastucture.Configurations
{
    public class ApplicationdentityErrorDescriber : IdentityErrorDescriber
    {
        public override IdentityError DefaultError() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Default, Description = $"Wystąpił nieznany błąd." }; }
        public override IdentityError ConcurrencyFailure() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = "Optimistic concurrency failure, obiekt został zmodyfikowany." }; }
        public override IdentityError PasswordMismatch() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = "Nieprawidłowe hasło." }; }
        public override IdentityError InvalidToken() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = "Nieprawidłowy token." }; }
        public override IdentityError LoginAlreadyAssociated() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Email, Description = "Użytkownik z tym emailem już istnieje." }; }
        public override IdentityError InvalidUserName(string userName) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Email, Description = $"Nazwa użytkownika '{userName}' jest niepoprawna. Powinna zawierać tylko litery i cyfry." }; }
        public override IdentityError InvalidEmail(string email) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Email, Description = $"Email '{email}' jest niepoprawny." }; }
        public override IdentityError DuplicateUserName(string userName) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Email, Description = $"Nazwa użytkownika '{userName}' jest już zajęta." }; }
        public override IdentityError DuplicateEmail(string email) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Email, Description = $"Email '{email}' jest już zajęty." }; }
        public override IdentityError InvalidRoleName(string role) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = $"Nazwa roli '{role}' jest nieprawidłowa." }; }
        public override IdentityError DuplicateRoleName(string role) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = $"Nazwa roli '{role}' jest zajęta." }; }
        public override IdentityError UserAlreadyHasPassword() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = "Użytkownik ma już ustawione hasło." }; }
        public override IdentityError UserLockoutNotEnabled() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = "Lockout jest wyłączony dla tego użytkownika." }; }
        public override IdentityError UserAlreadyInRole(string role) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = $"Użytkownik już ma rolę '{role}'." }; }
        public override IdentityError UserNotInRole(string role) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Internal, Description = $"Użytkownik nie jest w roli '{role}'." }; }
        public override IdentityError PasswordTooShort(int length) { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = $"Hasło musi zawierać conajmniej {length} znaków." }; }
        public override IdentityError PasswordRequiresNonAlphanumeric() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = "Hasło musi zawierać conajmniej jeden znak alfanumeryczny." }; }
        public override IdentityError PasswordRequiresDigit() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = "Hasło musi zawierać conajmniej jedną cyfrę ('0'-'9')." }; }
        public override IdentityError PasswordRequiresLower() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = "Hasło musi zawierać conajmniej jedną małą literę ('a'-'z')." }; }
        public override IdentityError PasswordRequiresUpper() { return new IdentityError { Code = ApplicationIdentityErrorCodes.Password, Description = "Hasło musi zawierać conajmniej jedną wielką literę ('A'-'Z')." }; }
    }

    public static class ApplicationIdentityErrorCodes
    {
        public const string Default = "";
        public const string Email = "Email";
        public const string Password = "Password";
        public const string Internal = "Internal";
    }
}
