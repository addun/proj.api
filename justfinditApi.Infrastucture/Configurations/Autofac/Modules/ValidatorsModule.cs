﻿using Autofac;
using FluentValidation;
using justfinditApi.Infrastucture.Services;
using justfinditApi.Infrastucture.Validators.ValidatorResolver;

namespace justfinditApi.Infrastucture.Configurations.Autofac.Modules
{
    public class ValidatorsModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ValidatorsModule).Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IValidator<>))
                .InstancePerLifetimeScope();

            builder.RegisterType<FluentValidatorService>()
                .As<IValidatorService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ValidatorResolver>()
                .As<IValidatorResolver>()
                .InstancePerLifetimeScope();
        }
    }
}
