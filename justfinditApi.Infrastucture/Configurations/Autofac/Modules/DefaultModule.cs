using Autofac;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Http;

namespace justfinditApi.Infrastucture.Configurations.Autofac.Modules
{
    public class DefaultModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {          
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();
            builder.RegisterType<PrincipalProvider>().As<IPrincipalProvider>().InstancePerDependency();
        }
    }
}