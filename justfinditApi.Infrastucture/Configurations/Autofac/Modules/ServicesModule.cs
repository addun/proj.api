using Autofac;
using justfinditApi.Core.DomainServices;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.DomainServices;
using justfinditApi.Infrastucture.Repositories;
using justfinditApi.Infrastucture.Services;

namespace justfinditApi.Infrastucture.Configurations.Autofac.Modules
{
    public class ServicesModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmailService>().As<IEmailService>().InstancePerLifetimeScope();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<EmployeeService>().As<IEmployeeService>().InstancePerLifetimeScope();
            builder.RegisterType<EmployerService>().As<IEmployerService>().InstancePerLifetimeScope();
            builder.RegisterType<JobFinder>().As<IJobFinder>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }
    }
}