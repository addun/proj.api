﻿namespace justfinditApi.Infrastucture.Configurations
{
    public static class ApplicationRoles
    {
        public const string Employee = "employee";
        public const string Employer = "employer";
    }
}
