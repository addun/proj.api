using System.Collections.Generic;
using justfinditApi.Infrastucture.Configurations;
using Microsoft.AspNetCore.Identity;

namespace justfinditApi.Infrastucture.Extensions
{
    public static class IdentityResultExtensions
    {
        public static Dictionary<string, List<string>> GetIdentityResultErrors(this IdentityResult result)
        {
            var errors = new Dictionary<string, List<string>>();

            foreach(var error in result.Errors)
            {
                //IF ERROR IS MARK AS INTERNAL WE DONT WANT TO SHOW THEM TO CLIENT
                if (error.Code == ApplicationIdentityErrorCodes.Internal)
                    continue;

                if (!errors.ContainsKey(error.Code))
                    errors.Add(error.Code, new List<string>());

                errors[error.Code].Add(error.Description);
            }

            return errors;
        }

        public static Dictionary<string, List<string>> GetIdentityResultErrors(this IdentityResult result, ApplicationIdentityErrorCodesOptions errorKeyNamesOptions)
        {
            var errors = GetIdentityResultErrors(result);
            if (errors.ContainsKey(ApplicationIdentityErrorCodes.Email))
            {
                var dictValue = errors[ApplicationIdentityErrorCodes.Email];
                errors.Remove(ApplicationIdentityErrorCodes.Email);
                errors.Add(errorKeyNamesOptions.EmailErrorKeyName, dictValue);
            }

            if (errors.ContainsKey(ApplicationIdentityErrorCodes.Password))
            {
                var dictValue = errors[ApplicationIdentityErrorCodes.Password];
                errors.Remove(ApplicationIdentityErrorCodes.Password);
                errors.Add(errorKeyNamesOptions.PasswordErrorKeyName, dictValue);
            }

            return errors;
        }
    }
}