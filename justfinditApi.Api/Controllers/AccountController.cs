﻿using System.Threading.Tasks;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Filters;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace justfinditApi.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/account")]
    [ExceptionHandlerFilter]
    public class AccountController : Controller
    {
        private readonly IAccountService _userService;

        public AccountController(IAccountService userService)
        {
            _userService = userService;
        }

        [HttpPost("confirm-email")]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmailDto confirmEmailDto)
        {
            await _userService.ConfirmEmail(confirmEmailDto);
            return Ok();
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDto forgotPassword)
        {
            await _userService.ForgotPassword(forgotPassword);
            return Ok();
        }

        [HttpPost("register/employee")]
        public async Task<IActionResult> RegisterEmployee([FromBody] RegisterEmployeeDto employee)
        {
            EmployeeDto result = await _userService.RegisterEmployee(employee);
            return Ok(result);
        }

        [HttpPost("register/employer")]
        public async Task<IActionResult> RegisterEmployer([FromBody]RegisterEmployerDto employer)
        {
            EmployerDto result = await _userService.RegisterEmployer(employer);
            return Ok(result);
        }
        
        [HttpPost("/api/token/refresh")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken([FromBody]RefreshTokenDto refreshToken)
        {
            var tokenResult = await _userService.RefreshToken(refreshToken);
            return Ok(tokenResult);
        }

        [HttpPost("resend-email")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ResendEmail()
        {
            await _userService.ResendEmail();
            return Ok();
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordDto resetPasswordDto)
        {
            await _userService.ResetPassword(resetPasswordDto);
            return Ok();
        }

        [HttpGet("/api/employee/token")]
        public async Task<IActionResult> EmployeeToken()
        {
            if (string.IsNullOrEmpty(Request.Headers["Authorization"].ToString()))
                return Unauthorized();

            var token = await _userService.GetEmployeeToken(Request.Headers["Authorization"]);

            return Ok(token);
        }

        [HttpGet("/api/employer/token")]
        public async Task<IActionResult> EmployerToken()
        {
            if (string.IsNullOrEmpty(Request.Headers["Authorization"].ToString()))
                return Unauthorized();

            var token = await _userService.GetEmployerToken(Request.Headers["Authorization"]);

            return Ok(token);
        }
    }
}