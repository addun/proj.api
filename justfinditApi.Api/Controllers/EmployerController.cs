using System;
using System.Threading.Tasks;
using justfinditApi.Infrastucture.Configurations;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Filters;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace justfinditApi.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/employer")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoles.Employer)]
    [ExceptionHandlerFilter]
    public class EmployerController : Controller
    {
        private readonly IEmployerService _employerService;

        public EmployerController(IEmployerService employerService)
        {
            _employerService = employerService;
        }
        
        [HttpPost("benefit")]
        public async Task<IActionResult> AddEmployerBenefit([FromBody] AddEmployerBenefitDto addEmployerBenefitDto)
        {
            await _employerService.AddBenefitAsync(addEmployerBenefitDto);
            return NoContent();
        }
        
        [HttpPost("location")]
        public async Task<IActionResult> AddEmployerLocation([FromBody] AddEmployerLocationDto addEmployerLocationDto)
        {
            await _employerService.AddLocationAsync(addEmployerLocationDto);
            return NoContent();
        }
        
        [HttpPost("technology")]
        public async Task<IActionResult> AddEmployerTechnology([FromBody] AddTechnologyDto addTechnologyDto)
        {
            await _employerService.AddTechnologyAsync(addTechnologyDto);
            return NoContent();
        }
        
        [HttpPost("job-offer")]
        public async Task<IActionResult> AddJobOffer([FromBody] AddJobOfferDto addJobOfferDto)
        {
            await _employerService.AddJobOfferAsync(addJobOfferDto);
            return NoContent();
        }
        
        [HttpDelete("benefit/delete/{employerId}/{benefitId}")]
        public async Task<IActionResult> DeleteBenefit(Guid employerId, Guid benefitId)
        {
            await _employerService.DeleteBenefitAsync(employerId, benefitId);
            return NoContent();
        }
        
        [HttpDelete("job-offer/delete/{userId}/{jobOfferId}")]
        public async Task<IActionResult> DeleteJobOffer(Guid userId, Guid jobOfferId)
        {
            await _employerService.DeleteJobOfferAsync(userId, jobOfferId);
            return NoContent();
        }  
  
        [HttpDelete("location/delete/{employerId}/{locationId}")]
        public async Task<IActionResult> DeleteLocation(Guid employerId, Guid locationId)
        {
            await _employerService.DeleteLocationAsync(employerId, locationId);
            return NoContent();
        }
        
        [HttpDelete("technology/delete/{employerId}/{technologyId}")]
        public async Task<IActionResult> DeleteTechnology(Guid employerId, Guid technologyId)
        {
            await _employerService.DeleteTechnologyAsync(employerId, technologyId);
            return NoContent();
        }
        
        [HttpPost("benefit/edit")]
        public async Task<IActionResult> EditEmployerBenefit([FromBody] EditEmployerBenefitDto editEmployerBenefitDto)
        {
            await _employerService.EditBenefitAsync(editEmployerBenefitDto);
            return NoContent();
        }
        
        [HttpPost("location/edit")]
        public async Task<IActionResult> EditEmployerLocation([FromBody] EditEmployerLocationDto editEmployerLocationDto)
        {
            await _employerService.EditLocationAsync(editEmployerLocationDto);
            return NoContent();
        }
        
        [HttpPost("employer-data/edit")]
        public async Task<IActionResult> EditEmployerData([FromBody] EditEmployerDataDto editEmployerDataDto)
        {
            await _employerService.EditEmployerDataAsync(editEmployerDataDto);
            return NoContent();
        }
        
        [HttpPost("internships/edit")]
        public async Task<IActionResult> EditInternshipDescription(
            [FromBody]EditInternshipDescriptionDto editInternshipDescriptionDto)
        {
            await _employerService.EditInternshipDescription(editInternshipDescriptionDto);
            return NoContent();
        }
        
        [HttpPost("job-offer/edit")]
        public async Task<IActionResult> EditJobOffer(
            [FromBody]EditJobOfferDto editJobOfferDto)
        {
            await _employerService.EditJobOfferAsync(editJobOfferDto);
            return NoContent();
        }
        
        [HttpPost("whats-we-creating/edit")]
        public async Task<IActionResult> EditWhatsWeCreatingDescription
            ([FromBody]EditWhatsWeCreatingDescriptionDto editWhatsWeCreatingDescriptionDto)
        {
            await _employerService.EditWhatsWeCreatingDescription(editWhatsWeCreatingDescriptionDto);
            return NoContent();
        }
        
        [HttpPost("working-with-us/edit")]
        public async Task<IActionResult> EditWorkingWithUsDescription
            ([FromBody]EditWorkingWithUsDescriptionDto editWorkingWithUsDescriptionDto)
        {
            await _employerService.EditWorkingWithUsDescription(editWorkingWithUsDescriptionDto);
            return NoContent();
        }      
        
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetEmployer(Guid userId)
        {
            var result = await _employerService.GetEmployerByUserId(userId);
            return Ok(result);
        }
        
        [HttpGet("employee-profile/{userId}")]
        public async Task<IActionResult> GetEmployeeProfile(Guid userId)
        {
            var result = await _employerService.GetEmployeeProfileByUserId(userId);
            return Ok(result);
        }
        
        [HttpGet("offer/{employerId}/{offerId}/{applicantId}/cv")]
        public async Task<IActionResult> GetApplicantCv(Guid employerId, Guid offerId, Guid applicantId)
        {
            var result = await _employerService.GetApplicantCv(employerId, offerId, applicantId);
            return Ok(result);
        }
        
        [HttpPost("headquarters")]
        public async Task<IActionResult> SetHeadquarters([FromBody]SetHeadquartersDto setHeadquartersDto)
        {
            await _employerService.SetHeadquartersAsync(setHeadquartersDto);
            return NoContent();
        }
    }
}