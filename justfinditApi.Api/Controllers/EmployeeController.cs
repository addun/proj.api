﻿using System;
using System.Threading.Tasks;
using justfinditApi.Infrastucture.Configurations;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Filters;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace justfinditApi.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/employee")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, 
        Roles = ApplicationRoles.Employee)]
    [ExceptionHandlerFilter]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        
        [HttpPost("certificate")]
        public async Task<IActionResult> AddCertificate([FromBody]AddCertificateDto addCertificateDto)
        {
            await _employeeService.AddCertificateAsync(addCertificateDto);
            return NoContent();
        }
        
        [HttpPost("education")]
        public async Task<IActionResult> AddEducation([FromBody]AddEducationDto addEducationDto)
        {
            await _employeeService.AddEducationAsync(addEducationDto);
            return NoContent();
        }
        
        [HttpPost("job-offer/favourite")]
        public async Task<IActionResult> AddJobOfferToFavourites(
            [FromBody]AddJobOfferToFavouritesDto addJobOfferToFavouritesDto)
        {
            await _employeeService.AddJobOfferToFavouritesAsync(addJobOfferToFavouritesDto);
            return NoContent();
        }
        
        [HttpPost("language")]
        public async Task<IActionResult> AddLanguage([FromBody]AddLanguageDto addLanguageDto)
        {
            await _employeeService.AddLanguageAsync(addLanguageDto);
            return NoContent();
        }
        
        [HttpPost("professional-experience")]
        public async Task<IActionResult> AddProfessionalExperience([FromBody]AddProfessionalExperienceDto addProfessionalExperienceDto)
        {
            await _employeeService.AddProfessionalExperienceAsync(addProfessionalExperienceDto);
            return NoContent();
        }
        
        [HttpPost("skill")]
        public async Task<IActionResult> AddSkill([FromBody]AddSkillDto addSkillDto)
        {
            await _employeeService.AddSkillAsync(addSkillDto);
            return NoContent();
        }
        
        [HttpPost("apply-for-job")]
        public async Task<IActionResult> ApplyForJob([FromForm]ApplyForJobDto applyForJobDto)
        {
            await _employeeService.ApplyForJobAsync(applyForJobDto);
            return NoContent();
        }
        
        [HttpDelete("certificate/delete/{employeeId}/{certificateId}")]
        public async Task<IActionResult> DeleteCertificate(Guid employeeId, Guid certificateId)
        {
            await _employeeService.DeleteCertificateAsync(employeeId, certificateId);
            return NoContent();
        }
        
        [HttpDelete("education/delete/{employeeId}/{educationId}")]
        public async Task<IActionResult> DeleteEducation(Guid employeeId, Guid educationId)
        {
            await _employeeService.DeleteEducationAsync(employeeId, educationId);
            return NoContent();
        }
        
        [HttpDelete("job-offer/favourite/delete/{employeeId}/{offerId}")]
        public async Task<IActionResult> AddJobOfferToFavourites(Guid employeeId, Guid offerId)
        {
            await _employeeService.DeleteJobOfferFromFavouritesAsync(employeeId, offerId);
            return NoContent();
        }
        
        [HttpDelete("language/delete/{employeeId}/{languageId}")]
        public async Task<IActionResult> DeleteLanguage(Guid employeeId, Guid languageId)
        {
            await _employeeService.DeleteLanguageAsync(employeeId, languageId);
            return NoContent();
        }
        
        [HttpDelete("skill/delete/{employeeId}/{skillId}")]
        public async Task<IActionResult> DeleteSkill(Guid employeeId, Guid skillId)
        {
            await _employeeService.DeleteSkillAsync(employeeId, skillId);
            return NoContent();
        }
        
        [HttpDelete("professional-experience/delete/{employeeId}/{jobId}")]
        public async Task<IActionResult> DeleteProfessionalExperience(Guid employeeId, Guid jobId)
        {
            await _employeeService.DeleteProfessionalExperienceAsync(employeeId, jobId);
            return NoContent();
        }
        
        [HttpPost("certificate/edit")]
        public async Task<IActionResult> EditCertificate([FromBody]EditCertificateDto editCertificate)
        {
            await _employeeService.EditCertificateAsync(editCertificate);
            return NoContent();
        }
              
        [HttpPost("education/edit")]
        public async Task<IActionResult> EditEducation([FromBody]EditEducationDto editEducation)
        {
            await _employeeService.EditEducationAsync(editEducation);
            return NoContent();
        }
        
        [HttpPost("language/edit")]
        public async Task<IActionResult> EditLanguage([FromBody]EditEmployeeLanguageDto editEmployeeLanguage)
        {
            await _employeeService.EditEmployeeLanguageAsync(editEmployeeLanguage);
            return NoContent();
        }
        
        [HttpPost("personal-data")]
        public async Task<IActionResult> EditPersonalData([FromBody]EditEmployeePersonalDataDto editEmployeePersonalDataDto)
        {
            await _employeeService.EditPersonalDataAsync(editEmployeePersonalDataDto);
            return NoContent();
        }   
        
        [HttpPost("skill/edit")]
        public async Task<IActionResult> EditSkill([FromBody]EditEmployeeSkillDto editEmployeeSkill)
        {
            await _employeeService.EditEmployeeSkillAsync(editEmployeeSkill);
            return NoContent();
        }
        
        [HttpPost("professional-experience/edit")]
        public async Task<IActionResult> EditProfessionalExperience([FromBody]EditProfessionalExperienceDto editProfessionalExperienceDto)
        {
            await _employeeService.EditProfessionalExperienceAsync(editProfessionalExperienceDto);
            return NoContent();
        }
        
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetEmployee(Guid userId)
        {
            var result = await _employeeService.GetEmployeeByUserId(userId);
            return Ok(result);
        }
        
        [HttpGet("employer-profile/{userId}")]
        public async Task<IActionResult> GetEmployerProfile(Guid userId)
        {
            var result = await _employeeService.GetEmployerProfileByUserId(userId);
            return Ok(result);
        }
    }
}