using System;
using System.Linq;
using System.Threading.Tasks;
using justfinditApi.Infrastucture.Filters;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Mvc;


namespace justfinditApi.Api.Controllers
{
    [Produces("application/json")]
    [ExceptionHandlerFilter]
    public class JobController : Controller
    {
        private readonly IJobFinder _jobFinderService;

        public JobController(IJobFinder jobFinderService)
        {
            _jobFinderService = jobFinderService;
        }

        [HttpGet("/api/job-search")]
        public async Task<IActionResult> FindJobs(
            [FromQuery] string[] skills, [FromQuery] string[] cities,
            [FromQuery] string salary, [FromQuery] string currency)
        {
            var result = _jobFinderService.Find(skills.ToList(), cities.ToList(), salary, currency);
            return Ok(result);
        }

        [HttpGet("/api/job/{id}")]
        public async Task<IActionResult> FindById([FromRoute] string id)
        {
            var result = _jobFinderService.FindById(Guid.Parse(id));
            return Ok(result);
        }

        [HttpGet("/api/test")]
        public async Task<IActionResult> Test()
        {
            return Ok("OK");
        }
    }
}