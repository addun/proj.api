using System.Linq;
using System.Threading.Tasks;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Filters;
using Microsoft.AspNetCore.Mvc;

namespace justfinditApi.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/autocomplete")]
    [ExceptionHandlerFilter]
    public class AutocompleteController : Controller
    {
        private readonly ApplicationDbContext _db;

        public AutocompleteController(ApplicationDbContext db)
        {
            _db = db;
        }
                
        [HttpGet("languages")]
        public async Task<IActionResult> GetLanguagesAutocomplete([FromQuery]string phrase)
        {
            var res = _db.Languages
                .Where(x => x.LanguageName.StartsWith(phrase))
                .Select(x => x.LanguageName).ToList();
            
            return Ok(res);
        }
        
        [HttpGet("skills")]
        public async Task<IActionResult> GetSkillsAutocomplete([FromQuery]string phrase)
        {            
            var res = _db.Technologies
                .Where(x => x.TechnologyName.StartsWith(phrase))
                .Select(x => x.TechnologyName).ToList();
            
            return Ok(res);
        }
    }
}