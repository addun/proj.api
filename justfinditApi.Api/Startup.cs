﻿using System;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using FluentValidation.AspNetCore;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Configurations;
using justfinditApi.Infrastucture.Configurations.Autofac.Modules;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Data.DatabaseSeed;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace justfinditApi.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("justfindit")));

            services.AddIdentity<User, Role>()
                .AddErrorDescriber<ApplicationdentityErrorDescriber>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddSingleton<IJwtProvider, JwtProvider>();
            services.AddAuthentication().AddJwtBearer(options =>
            {
                options.TokenValidationParameters.ValidateIssuerSigningKey = true;
                options.TokenValidationParameters.IssuerSigningKey
                    = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:signInKey"]));
                options.TokenValidationParameters.ValidAudience = Configuration["Jwt:audience"];
                options.TokenValidationParameters.ValidIssuer = Configuration["Jwt:issuer"];
                options.TokenValidationParameters.ValidateLifetime = true;
                options.TokenValidationParameters.ClockSkew = TimeSpan.Zero;
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }

                        return Task.CompletedTask;
                    }
                };
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .WithExposedHeaders("token-expired")
                    );
            });

            services.AddMvc().AddJsonOptions(o =>
            {
                o.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).AddFluentValidation();

            services.AddAutoMapper();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule<DefaultModule>();
            containerBuilder.RegisterModule<ServicesModule>();
            containerBuilder.RegisterModule<ValidatorsModule>();
            ApplicationContainer = containerBuilder.Build();

            return new AutofacServiceProvider(ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            IApplicationLifetime applicationLifetime)
        {

            if (!env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAll");
            app.UseAuthentication();

            app.UseMvc();

            if (!env.IsDevelopment())
            {
                // SeedDatabase
                // using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                // {
                //     serviceScope.ServiceProvider.GetService<ApplicationDbContext>().PopulateDatabase(serviceScope);
                // }
            }

            applicationLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }
    }
}