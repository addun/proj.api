using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Repositories;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Repositories
{
    public class TechnologyRepositoryTests: IDisposable
    {
        private readonly TechnologyRepository _technologyRepository;
        private readonly Mock<ApplicationDbContext> _dbContextMock;
        public TechnologyRepositoryTests()
        {
            _dbContextMock =  new Mock<ApplicationDbContext>();
            _technologyRepository = new TechnologyRepository(_dbContextMock.Object);
        }
        
        [Fact]
        public void GetTechnologyByNameAsync_WhenItemWithThatNameExist_ShouldReturnProperItem()
        {
            var queriedTechnology = Technology.Create("C#");
            var technologies = new List<Technology>
            {
                queriedTechnology,
                Technology.Create("Java"),
                Technology.Create("Angular")
            };
            var technologiesMock = technologies.AsQueryable().BuildMockDbSet();                       
            _dbContextMock.Object.Technologies = technologiesMock.Object;

            var response = _technologyRepository.GetTechnologyByNameAsync("C#");
            
            response.Result.Should().Be(queriedTechnology);
        }
        
        [Fact]
        public void GetTechnologyByNameAsync_WhenItemWithThatNameDoesNotExist_ShouldReturnNull()
        {
            var queriedTechnologyThatDoesNotExist = Technology.Create("C#");
            var technologies = new List<Technology>
            {
                Technology.Create("Java"),
                Technology.Create("Angular")
            };
            var technologiesMock = technologies.AsQueryable().BuildMockDbSet();            
            _dbContextMock.Object.Technologies = technologiesMock.Object;

            var response = _technologyRepository.GetTechnologyByNameAsync(queriedTechnologyThatDoesNotExist.TechnologyName);
            
            response.Result.Should().BeNull();
        }

        public void Dispose()
        {
            _dbContextMock.Object.Dispose();
        }
    }
}