using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using justfinditApi.Core.ConstantValues;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Repositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace justfinditApi.Tests.Repositories
{
    public class EmployeeRepositoryTests: IDisposable
    {
        private readonly DbContextOptions<ApplicationDbContext> _dbContextInMemoryOptions;
        private ApplicationDbContext _dbContextToArrange;
        private ApplicationDbContext _dbContextToAssert;
        
        public EmployeeRepositoryTests()
        {
            _dbContextInMemoryOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
        }
        
        [Fact]
        public void AddAsync_WhenCalled_EmployeeIsAdded()
        {
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepository = new EmployeeRepository(_dbContextToAssert);
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());

            
            Task.FromResult(employeeRepository.AddAsync(employee));
            _dbContextToAssert.SaveChanges();

            _dbContextToAssert.Employees.Count().Should().Be(1);
        }
        
        [Fact]
        public void AddAsync_WhenCalled_AddedEmployeeIsReturned()
        {
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepository = new EmployeeRepository(_dbContextToAssert);
                         
            var userId = Guid.NewGuid();
            var user = new User{Id = userId, Email = "test@test.com"};
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", user);

            var result = employeeRepository.AddAsync(employee).Result;
            _dbContextToAssert.SaveChanges();

            result.User.Id.Should().Be(userId);
        }

        [Fact]
        public void GetByEmailAsync_WhenEmployeeWithGivenEmailExist_ReturnThatEmployee()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToArrange = new EmployeeRepository(_dbContextToArrange);
            
            var queriedUserEmail = "test@test.com";
            var user = new User{Email = queriedUserEmail};
            var queriedEmployee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", user); 
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());

            Task.FromResult(employeeRepositoryToArrange.AddAsync(queriedEmployee));
            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();

            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            var result = employeeRepositoryToAssert.GetByEmailAsync(queriedUserEmail).Result;

            result.Should().BeEquivalentTo(queriedEmployee);
        }
        
        [Fact]
        public void GetByEmailAsync_WhenEmployeeWithGivenEmailExist_EnsureThatEmployeeContainAllDependentProperties()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToArrange = new EmployeeRepository(_dbContextToArrange);
            
            var queriedUserEmail = "test@test.com";
            var user = new User{Email = queriedUserEmail};
            var education = Education.Create("School", "2010", "2014", "IT", "Master", "speciality");
            var course1 = Course.Create("course1", "Microsoft", "10", "2015");
            var course2 = Course.Create("course2", "Oracle", "1", "2016");
            var queriedEmployee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", user); 
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            queriedEmployee.AddEducation(education);
            queriedEmployee.AddCourse(course1);
            queriedEmployee.AddCourse(course2);
            
            Task.FromResult(employeeRepositoryToArrange.AddAsync(queriedEmployee));
            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();

            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            var result = employeeRepositoryToAssert.GetByEmailAsync(queriedUserEmail).Result;

            result.Courses.Count.Should().Be(2);
            result.Courses.Should().Contain(x => x.CourseId == course1.CourseId);
            result.Courses.Should().Contain(x => x.CourseId == course2.CourseId);
            result.Education.Count.Should().Be(1);
            result.Education.Should().Contain(x => x.EducationId == education.EducationId);
        }
        
        [Fact]
        public void GetByEmailAsync_WhenEmployeeWithGivenEmailDoesNotExist_ReturnNull()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepository = new EmployeeRepository(_dbContextToArrange);
            
            var notExistedEmail = "test@test.com";
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User{Email = "test1@test.com"});

            Task.FromResult(employeeRepository.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();

            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            
            var result = employeeRepositoryToAssert.GetByEmailAsync(notExistedEmail).Result;

            result.Should().BeNull();
        }
        
        [Fact]
        public void GetByUserIdAsync_WhenEmployeeWithGivenIdExist_ReturnThatEmployee()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToArrange = new EmployeeRepository(_dbContextToArrange);
            
            var queriedUserId = Guid.NewGuid();
            var user = new User{Id = queriedUserId};
            
            var queriedEmployee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", user); 
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User{Id = Guid.NewGuid()});
            
            Task.FromResult(employeeRepositoryToArrange.AddAsync(queriedEmployee));
            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();
            
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            var result = employeeRepositoryToAssert.GetByUserIdAsync(queriedUserId).Result;

            result.Should().BeEquivalentTo(queriedEmployee);
        }
        
        [Fact]
        public void GetByUserIdAsync_WhenEmployeeWithGivenIdExist_EnsureThatEmployeeContainAllDependentProperties()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToArrange = new EmployeeRepository(_dbContextToArrange);
            
            var queriedUserId = Guid.NewGuid();
            var user = new User{Id = queriedUserId};
            var education = Education.Create("School", "2010", "2014", "IT", "Master", "speciality");
            var course1 = Course.Create("course1", "Microsoft", "10", "2015");
            var course2 = Course.Create("course2", "Oracle", "1", "2016");
            var queriedEmployee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", user); 
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User{Id = Guid.NewGuid()});
            queriedEmployee.AddEducation(education);
            queriedEmployee.AddCourse(course1);
            queriedEmployee.AddCourse(course2);

            Task.FromResult(employeeRepositoryToArrange.AddAsync(queriedEmployee));
            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();

            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            
            var result = employeeRepositoryToAssert.GetByUserIdAsync(queriedUserId).Result;

            result.Courses.Count.Should().Be(2);
            result.Courses.Should().Contain(x => x.CourseId == course1.CourseId);
            result.Courses.Should().Contain(x => x.CourseId == course2.CourseId);
            result.Education.Count.Should().Be(1);
            result.Education.Should().Contain(x => x.EducationId == education.EducationId);
        }

        [Fact]
        public void GetByUserIdAsync_WhenEmployeeWithGivenIdDoesNotExist_ReturnNull()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToArrange = new EmployeeRepository(_dbContextToArrange);
            
            var notExistedId = Guid.NewGuid();
            var employee1 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User{Id = Guid.NewGuid()});
            var employee2 = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User{Id = Guid.NewGuid()});

            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee1));
            Task.FromResult(employeeRepositoryToArrange.AddAsync(employee2));
            _dbContextToArrange.SaveChanges();
           
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employeeRepositoryToAssert = new EmployeeRepository(_dbContextToAssert);
            var result = employeeRepositoryToAssert.GetByUserIdAsync(notExistedId).Result;

            result.Should().BeNull();
        }

        public void Dispose()
        {
            _dbContextToArrange?.Dispose();
            _dbContextToAssert?.Dispose();
        }
    }
}