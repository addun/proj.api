using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Repositories;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Repositories
{
    public class UserRepositoryTests: IDisposable
    {
        private readonly UserRepository _userRepository;
        private readonly Mock<ApplicationDbContext> _dbContextMock;
        public UserRepositoryTests()
        {
            _dbContextMock =  new Mock<ApplicationDbContext>();
            _userRepository = new UserRepository(_dbContextMock.Object);
        }
        
        [Fact]
        public void GetUserByIdAsync_WhenUserWithGivenIdExist_ReturnThatUser()
        {
            var queriedUserId = Guid.NewGuid();
            var queriedUser = new User {Id = queriedUserId, Email = "test@test.com"};
            var users = new List<User>
            {
                new User {Id = Guid.NewGuid(), Email = "test1@test.com"},
                queriedUser,
                new User {Id = Guid.NewGuid(), Email = "test2@test.com"},
                new User {Id = Guid.NewGuid(), Email = "test3@test.com"},
            };
            var usersMock = users.AsQueryable().BuildMockDbSet();
            _dbContextMock.Object.Users = usersMock.Object;

            var response = _userRepository.GetUserByIdAsync(queriedUserId);

            response.Result.Should().Be(queriedUser);
        }
        
        [Fact]
        public void GetUserByIdAsync_WhenUserWithGivenIdNotExist_ReturnNull()
        {
            var noExistingUserId = Guid.NewGuid();
            var users = new List<User>
            {
                new User {Email = "test1@test.com"},
                new User {Email = "test2@test.com"},
                new User {Email = "test3@test.com"},
            };
            var usersMock = users.AsQueryable().BuildMockDbSet();
            _dbContextMock.Object.Users = usersMock.Object;

            var response = _userRepository.GetUserByIdAsync(noExistingUserId);

            response.Result.Should().BeNull();
        }

        [Fact]
        public void GetUserByEmailAsync_WhenUserWithGivenEmailExist_ReturnThatUser()
        {
            var queriedUserEmail = "existingEmail@test.com";
            var queriedUser = new User {Email = queriedUserEmail};
            var users = new List<User>
            {
                new User {Id = Guid.NewGuid(), Email = "test1@test.com"},
                queriedUser,
                new User {Id = Guid.NewGuid(), Email = "test2@test.com"},
                new User {Id = Guid.NewGuid(), Email = "test3@test.com"},
            };
            var usersMock = users.AsQueryable().BuildMockDbSet();
            _dbContextMock.Object.Users = usersMock.Object;

            var response = _userRepository.GetUserByEmailAsync(queriedUserEmail);

            response.Result.Should().Be(queriedUser);
        }
        
        [Fact]
        public void GetUserByEmailAsync_WhenUserWithGivenEmailNotExist_ReturnNull()
        {
            var noExistingUserEmail = "notExistingEmail@test.com";
            var users = new List<User>
            {
                new User {Id = Guid.NewGuid(), Email = "test1@test.com"},
                new User {Id = Guid.NewGuid(), Email = "test2@test.com"},
                new User {Id = Guid.NewGuid(), Email = "test3@test.com"},
            };
            var usersMock = users.AsQueryable().BuildMockDbSet();
            _dbContextMock.Object.Users = usersMock.Object;

            var response = _userRepository.GetUserByEmailAsync(noExistingUserEmail);

            response.Result.Should().BeNull();
        }

        [Fact]
        public void UpdateUser_WhenCalled_UpdateMethodIsCalledWithProperUser()
        {
            var userToUpdate = new User
            {
                Id = Guid.NewGuid(), Email = "test@test.com", PhoneNumber = "444555666"
            };
            var users = new List<User>
            {
                userToUpdate,
                new User {Id = Guid.NewGuid(), Email = "test1@test.com", PhoneNumber = "111222333"}
            };
            var usersMock = users.AsQueryable().BuildMockDbSet();
            User userToUpdatePassedToMethod = null;
            usersMock.Setup(x => x.Update(It.IsAny<User>()))
                .Callback<User>(user => userToUpdatePassedToMethod = user);     
            _dbContextMock.Object.Users = usersMock.Object;

            userToUpdate.Email = "changed@test.com";
            userToUpdate.PhoneNumber = "777777777";
            _userRepository.UpdateUser(userToUpdate);
            
            usersMock.Verify(x=>x.Update(It.IsAny<User>()), Times.Once);
            userToUpdatePassedToMethod.Should().Be(userToUpdate);
        }

        public void Dispose()
        {            
            _dbContextMock.Object.Dispose();        
        }
    }
}