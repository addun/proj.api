using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Repositories;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Repositories
{
    public class LanguageRepositoryTests
    {
        private readonly LanguageRepository _languageRepository;
        private readonly Mock<ApplicationDbContext> _dbContextMock;
        
        public LanguageRepositoryTests()
        {
            _dbContextMock = new Mock<ApplicationDbContext>();
            _languageRepository = new LanguageRepository(_dbContextMock.Object);
        }
        
        [Fact]
        public void GetLanguageByName_WhenLanguageWithGivenNameExist_ReturnThatLanguage()
        {
            var queriedLanguage= Language.Create("English");
            var languages = new List<Language>
            {
                queriedLanguage,
                Language.Create("Spanish"),
                Language.Create("German")
            };
            var languagesMock = languages.AsQueryable().BuildMockDbSet();                       
            _dbContextMock.Object.Languages = languagesMock.Object;

            var response = _languageRepository.GetLanguageByName(queriedLanguage.LanguageName);
            
            response.Result.Should().Be(queriedLanguage); 
        }
        
        [Fact]
        public void GetLanguageByName_WhenLanguageWithGivenNameDoesNotExist_ReturnNull()
        {
            var queriedLanguageThatDoesNotExist= Language.Create("English");
            var languages = new List<Language>
            {
                Language.Create("Spanish"),
                Language.Create("German")
            };
            var languagesMock = languages.AsQueryable().BuildMockDbSet();                       
            _dbContextMock.Object.Languages = languagesMock.Object;

            var response = _languageRepository.GetLanguageByName(queriedLanguageThatDoesNotExist.LanguageName);
            
            response.Result.Should().BeNull();
        }
    }
}