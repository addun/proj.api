using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.Data;
using justfinditApi.Infrastucture.Repositories;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Repositories
{
    public class EmployerRepositoryTests: IDisposable
    {
        private readonly DbContextOptions<ApplicationDbContext> _dbContextInMemoryOptions;
        private ApplicationDbContext _dbContextToArrange;
        private ApplicationDbContext _dbContextToAssert;
        public EmployerRepositoryTests()
        {
            _dbContextInMemoryOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
        }
        
        [Fact]
        public void AddAsync_WhenCalled_EmployerIsAdded()
        {
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepository = new EmployerRepository(_dbContextToAssert);
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var newEmployer = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, new User());
  
            Task.FromResult(employerRepository.AddAsync(newEmployer));
            _dbContextToAssert.SaveChanges();

            _dbContextToAssert.Employers.Count().Should().Be(1);
        }
        
        [Fact]
        public void AddAsync_WhenCalled_AddedEmployerIsReturned()
        {
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepository = new EmployerRepository(_dbContextToAssert);
            
            var userId = Guid.NewGuid();
            var user = new User{Id = userId};
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var newEmployer = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, user);

            var result = employerRepository.AddAsync(newEmployer).Result;
            _dbContextToAssert.SaveChanges();

            result.User.Id.Should().Be(userId);
        }

        [Fact]
        public void GetByEmailAsync_WhenCalledWithEmailThatDoesNotExist_ReturnNull()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToArrange = new EmployerRepository(_dbContextToArrange);
            
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");

            var notExistedEmployer = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, new User {Email = "test@test.com"});
            var employer2 = new Employer("Company name1", "example", "2001", "201", "0987654321", "example1.com", location, new User {Email = "test1@test.com"});

            Task.FromResult(employerRepositoryToArrange.AddAsync(employer2));
            _dbContextToArrange.SaveChanges();
          
            
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToAssert = new EmployerRepository(_dbContextToAssert);
            var result = employerRepositoryToAssert.GetByEmailAsync(notExistedEmployer.User.Email);

            result.Result.Should().BeNull();
        }
       
        [Fact]
        public void GetByEmailAsync_WhenCalledWithEmailThatExist_ReturnProperObjectWithIncludedUserObject()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToArrange = new EmployerRepository(_dbContextToArrange);

            var userId = Guid.NewGuid();
            var user = new User {Id = userId, Email = "test@test.com"};
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var queriedEmployer = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, user);
            var employer2 = new Employer("Company name1", "example", "2001", "201", "0987654321", "example1.com", location, new User {Email = "test1@test.com"});
            
            Task.FromResult(employerRepositoryToArrange.AddAsync(queriedEmployer));
            Task.FromResult(employerRepositoryToArrange.AddAsync(employer2));
            _dbContextToArrange.SaveChanges();

            
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToAssert = new EmployerRepository(_dbContextToAssert);
            var result = employerRepositoryToAssert.GetByEmailAsync(queriedEmployer.User.Email).Result;

            result.Should().BeEquivalentTo(queriedEmployer);
            result.User.Should().NotBeNull();
            result.User.Id.Should().Be(userId);
        }
        
        [Fact]
        public void GetUserByIdAsync_WhenCalledWithIdThatDoesNotExist_ReturnNull()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToArrange = new EmployerRepository(_dbContextToArrange);

            var notExistedId = Guid.NewGuid();
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var employer2 = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, new User{Id = Guid.NewGuid()});

            Task.FromResult(employerRepositoryToArrange.AddAsync(employer2));
            _dbContextToArrange.SaveChanges();
          
            
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToAssert = new EmployerRepository(_dbContextToAssert);
            var result = employerRepositoryToAssert.GetByUserIdAsync(notExistedId);

            result.Result.Should().BeNull();
        }
       
        [Fact]
        public void GetUserByIdAsync_WhenCalledWithIdThatExist_ReturnProperObjectWithIncludedUserObject()
        {
            _dbContextToArrange = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToArrange = new EmployerRepository(_dbContextToArrange);

            var userId = Guid.NewGuid();
            var user = new User {Id = userId};
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var queriedEmployer = new Employer("Company name", "", "2000", "200", "1234567890", "example.com", location, user);
            var employer2 = new Employer("Company name1", "example", "2001", "201", "0987654321", "example1.com", location, new User {Id = Guid.NewGuid()});


            Task.FromResult(employerRepositoryToArrange.AddAsync(queriedEmployer));
            Task.FromResult(employerRepositoryToArrange.AddAsync(employer2));
            _dbContextToArrange.SaveChanges();
          
            
            _dbContextToAssert = new ApplicationDbContext(_dbContextInMemoryOptions);
            var employerRepositoryToAssert = new EmployerRepository(_dbContextToAssert);
            var result = employerRepositoryToAssert.GetByUserIdAsync(userId).Result;

            result.Should().BeEquivalentTo(queriedEmployer);
            result.User.Should().NotBeNull();
            result.User.Id.Should().Be(userId);
        }

        public void Dispose()
        {
            _dbContextToArrange?.Dispose();
            _dbContextToAssert?.Dispose();
        }
    }
}