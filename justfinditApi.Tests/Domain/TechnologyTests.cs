using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class TechnologyTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Create_WhenCalledWithInvalidTechnologyName_ThrowException(string technologyName)
        {
            Action creation = () => Technology.Create(technologyName);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_WhenCalledWithValidArguments_CreateObject()
        {
            var technology = Language.Create("C#");

            technology.LanguageName.Should().Be("C#");
        }
    }
}