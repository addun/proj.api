using System;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class CourseTests
    {
        [Theory]
        [InlineData("CourseName","OrganizerName","1","2018")]
        public void Create_WhenCalledWithValidArguments_NotThrowingException(string courseName, string organizerName,
            string monthOfObtain, string yearOfObtain)
        {
            var exception = Record.Exception(() =>
            {
                Course.Create(courseName, organizerName, monthOfObtain, yearOfObtain);
            });

            Assert.Null(exception);
        }
        
        [Fact]
        public void Create_AfterCreation_ObjectStateIsValid()
        {
            string courseName = "CourseName";
            string courseOrganizer = "CourseOrganizer";
            string monthOfObtain = "2";
            string yearOfObtain = "2018";

            var course = Course.Create(courseName, courseOrganizer, monthOfObtain, yearOfObtain);
            
            Assert.Equal(course.CourseName,courseName);
            Assert.Equal(course.OrganizerName,courseOrganizer);
            Assert.Equal(course.DateOfObtain.Month.ToString().ToLower(), monthOfObtain.ToLower());
            Assert.Equal(course.DateOfObtain.Year.ToString().ToLower(), yearOfObtain.ToLower());
        }
        
        [Theory]
        [InlineData(null,"OrganizerName","10","2018")]
        [InlineData("","OrganizerName","10","2018")]
        public void Create_WhenCalledWithNullOrEmptyCourseName_ThrowingArgumentException(string courseName, string organizerName,
            string monthOfObtain, string yearOfObtain)
        {
            var exception = Record.Exception(() =>
            {
                Course.Create(courseName, organizerName, monthOfObtain, yearOfObtain);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("CourseName",null,"10","2018")]
        [InlineData("CourseName","","10","2018")]
        public void Create_WhenCalledWithNullOrEmptyOrganizerName_ThrowingArgumentException(string courseName, string organizerName,
            string monthOfObtain, string yearOfObtain)
        {
            var exception = Record.Exception(() =>
            {
                Course.Create(courseName, organizerName, monthOfObtain, yearOfObtain);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("CourseName","OrganizerName",null,"2018")]
        [InlineData("CourseName","OrganizerName","0","2018")]
        [InlineData("CourseName","OrganizerName","13","2018")]
        [InlineData("CourseName","OrganizerName","xyz","2018")]
        public void Create_WhenCalledWithNullOrInvalidMonthOfObtain_ThrowingArgumentException(string courseName, string organizerName,
            string monthOfObtain, string yearOfObtain)
        {
            var exception = Record.Exception(() =>
            {
                Course.Create(courseName, organizerName, monthOfObtain, yearOfObtain);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("CourseName","OrganizerName","10",null)]
        [InlineData("CourseName","OrganizerName","10", "")]
        [InlineData("CourseName","OrganizerName","10", "xyz")]
        public void Create_WhenCalledWithNullOrInvalidYearOfObtain_ThrowingArgumentException(string courseName, string organizerName,
            string monthOfObtain, string yearOfObtain)
        {
            var exception = Record.Exception(() =>
            {
                Course.Create(courseName, organizerName, monthOfObtain, yearOfObtain);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }       
    }
}