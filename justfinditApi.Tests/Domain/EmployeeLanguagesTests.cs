using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class EmployeeLanguagesTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(6)]
        public void Create_LevelIsInvalid_ThrowException(int level)
        {
            var language = Language.Create("English");

            Action creation = () => EmployeeLanguage.Create(language, level);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_LanguageIsNull_ThrowException()
        {
            Action creation = () => EmployeeLanguage.Create(null, 1);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_ArgumentsAreValid_CreateObject()
        {
            var language = Language.Create("English");
            var employeeLanguages = EmployeeLanguage.Create(language, 1);

            employeeLanguages.Language.Should().BeEquivalentTo(language);
            employeeLanguages.Level.Should().Be(1);
        }
    }
}