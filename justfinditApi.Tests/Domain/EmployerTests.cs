using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class EmployerTests
    {
        
        [Fact]
        public void CreateEmployer_WhenCalledWithValidArguments_CreatedEmployerHasCorrectState()
        {
            var location = EmployerLocation.Create("country", "city", "street", "24-667", "22", "2");
            Employer employer = null;
            Action create = () =>
            {
                employer = new Employer("companyName", "companyDesc", "2000", "20", "1234567890", "example.com",
                    location, new User());
            };
            
            create.Should().NotThrow();
            employer.Should().NotBeNull();
            employer.CompanyName.Should().Be("companyName");
            employer.CompanyDescription.Should().Be("companyDesc");
            employer.FoundedYear.Should().Be("2000");
            employer.CompanySize.Should().Be("20");
            employer.Nip.Should().Be("1234567890");
            employer.WebsiteUrl.Should().Be("example.com");
        }
        
        [Fact]
        public void CreateEmployer_WhenUserIsNull_ThrowServerException()
        {
            var location = EmployerLocation.Create("country", "city", "street", "24-667", "22", "2");
            Action create = () =>
            {
                var employer = new Employer("companyName", "companyDesc", "2000", "20", "1234567890", "example.com",
                    location, null);
            };
            create.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void CreateEmployer_WhenCompanyLocationIsNull_ThrowServerException()
        {
            
            Action create = () =>
            {
                var employer = new Employer("companyName", "companyDesc", "2000", "20", "1234567890", "example.com",
                    null, new User());
            };
            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData(null, "description", "2000", "10", "1234567890", "example.com")]
        [InlineData("", "description", "2000", "10", "1234567890", "example.com")]
        [InlineData(" ", "description", "2000", "10", "1234567890", "example.com")]
        public void CreateEmployer_WhenCompanyNameIsNullOrWhitespace_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            Action create = () =>
            {
                var employer = new Employer(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl,
                    location, user);
            };
            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", null, "10", "1234567890", "example.com")]
        [InlineData("company name", "description", "", "10", "1234567890", "example.com")]
        [InlineData("company name", "description", " ", "10", "1234567890", "example.com")]
        [InlineData("company name", "description", "xyz", "10", "1234567890", "example.com")]
        public void CreateEmployer_WhenFoundedYearIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            Action create = () =>
            {
                var employer = new Employer(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl,
                    location, user);
            };
            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", "2000", null, "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", " ", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "xyz", "1234567890", "example.com")]
        public void CreateEmployer_WhenCompanySizeIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            Action create = () =>
            {
                var employer = new Employer(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl,
                    location, user);
            };
            create.Should().Throw<ArgumentException>();
        }
                
        [Theory]
        [InlineData("company name", "description", "2000", "10", null, "example.com")]
        [InlineData("company name", "description", "2000", "10", "", "example.com")]
        [InlineData("company name", "description", "2000", "10", " ", "example.com")]
        [InlineData("company name", "description", "2000", "10", "xyz", "example.com")]
        public void CreateEmployer_WhenNipIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            Action create = () =>
            {
                var employer = new Employer(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl,
                    location, user);
            };
            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", "2000", "10", "1234567890", null)]
        [InlineData("company name", "description", "2000", "10", "1234567890", "")]
        [InlineData("company name", "description", "2000", "10", "1234567890", " ")]
        public void CreateEmployer_WhenWebsiteUrlIsNullOrWhitespace_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            Action create = () =>
            {
                var employer = new Employer(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl,
                    location, user);
            };
            create.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void EditCompanyData_WhenCalledWithValidArguments_EmployerHasCorrectState()
        {
            var location = EmployerLocation.Create("country", "city", "street", "24-667", "22", "2");
            Employer employer = new Employer("companyName", "companyDesc", "2000", "20", "1234567890", "example.com",
                location, new User());
            Action edit = () =>
            {
                employer.EditEmployerData("companyName1", "companyDesc1", "2001", "21", "0987654321", "example1.com");
            };
            
            edit.Should().NotThrow();
            employer.Should().NotBeNull();
            employer.CompanyName.Should().Be("companyName1");
            employer.CompanyDescription.Should().Be("companyDesc1");
            employer.FoundedYear.Should().Be("2001");
            employer.CompanySize.Should().Be("21");
            employer.Nip.Should().Be("0987654321");
            employer.WebsiteUrl.Should().Be("example1.com");
        }
        
        [Theory]
        [InlineData(null, "description", "2000", "10", "1234567890", "example.com")]
        [InlineData("", "description", "2000", "10", "1234567890", "example.com")]
        [InlineData(" ", "description", "2000", "10", "1234567890", "example.com")]
        public void EditCompanyData_WhenCompanyNameIsNullOrWhitespace_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", null, "10", "1234567890", "example.com")]
        [InlineData("company name", "description", "", "10", "1234567890", "example.com")]
        [InlineData("company name", "description", " ", "10", "1234567890", "example.com")]
        [InlineData("company name", "description", "xyz", "10", "1234567890", "example.com")]
        public void EditCompanyData_WhenFoundedYearIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", "2000", null, "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", " ", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "xyz", "1234567890", "example.com")]
        public void EditCompanyData_WhenCompanySizeIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", "2000", "10", null, "example.com")]
        [InlineData("company name", "description", "2000", "10", "", "example.com")]
        [InlineData("company name", "description", "2000", "10", " ", "example.com")]
        [InlineData("company name", "description", "2000", "10", "xyz", "example.com")]
        public void EditCompanyData_WhenNipIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("company name", "description", "2000", "10", "1234567890", null)]
        [InlineData("company name", "description", "2000", "10", "1234567890", "")]
        [InlineData("company name", "description", "2000", "10", "1234567890", " ")]
        public void EditCompanyData_WhenWebsiteUrlIsInvalid_ThrowServerException(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("", "description", "2000", "10", "1234567890", "example.com")]
        [InlineData("", "description", "", "10", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "", "1234567890", "example.com")]
        [InlineData("company name", "description", "2000", "10", "", "example.com")]
        [InlineData("company name", "description", "2000", "10", "1234567890", "")]
        public void EditCompanyData_WhenAnyFromArgumentsIsInvalid_InitialStateShouldNotChange(string companyName, string companyDescription, 
            string foundedYear, string companySize, string nip, string websiteUrl)

        {
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User();
            var employer = new Employer("company name1", "description1", "2001", "11", "0987654321", "example1.com",
                location, user);
            
            Action edit = () =>
            {
                employer.EditEmployerData(companyName, companyDescription, foundedYear, companySize, nip, websiteUrl);
            };
            
            edit.Should().Throw<ArgumentException>();
            employer.Should().NotBeNull();
            employer.CompanyName.Should().Be("company name1");
            employer.CompanyDescription.Should().Be("description1");
            employer.FoundedYear.Should().Be("2001");
            employer.CompanySize.Should().Be("11");
            employer.Nip.Should().Be("0987654321");
            employer.WebsiteUrl.Should().Be("example1.com");
        }
    }
}