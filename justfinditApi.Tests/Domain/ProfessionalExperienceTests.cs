using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class ProfessionalExperienceTests
    {
        [Theory]
        [InlineData("","Company name", "1", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData(null,"Company name", "1", "2010","1","2012","Poland","Cracow","Description")]
        public void Create_WhenCreateWithNullOrWhitespacePosition_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Position","", "1", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position",null, "1", "2010","1","2012","Poland","Cracow","Description")]
        public void Create_WhenCreateWithNullOrWhitespaceCompanyName_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Position","Company name", "1", "2010","1","2012","","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","1","2012",null,"Cracow","Description")]
        public void Create_WhenCreateWithNullOrWhitespaceCountry_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Position","Company name", "1", "2010","1","2012","Poland","","Description")]
        [InlineData("Position","Company name", "1", "2010","1","2012","Poland",null,"Description")]
        public void Create_WhenCreateWithNullOrWhitespaceCity_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Position","Company name", "0", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "13", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", null, "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "xyz", "2010","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "null","1","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "xyz","1","2012","Poland","Cracow","Description")]
        public void Create_WhenCreateWithInvalidStartDate_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Position","Company name", "1", "2010","0","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","13","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","xyz","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","","2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010",null,"2012","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","1","","Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","1",null,"Poland","Cracow","Description")]
        [InlineData("Position","Company name", "1", "2010","1","xyz","Poland","Cracow","Description")]
        public void Create_WhenCreateWithInvalidEndDate_ThrowException(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            Action creation = () => ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            creation.Should().Throw<ArgumentException>();
        }
        
        
        [Theory]
        [InlineData("Position","Company name", "1", "2010","1","2012","Poland","Cracow","Description")]
        public void Create_WhenCreateWithValidArguments_CreateObject(string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            var profExp = ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            profExp.Position.Should().Be(position);
            profExp.CompanyName.Should().Be(companyName);
            profExp.StartDate.Month.ToString().ToLower().Should().Be(startMonth);
            profExp.StartDate.Year.ToString().ToLower().Should().Be(startYear);
            profExp.EndDate.Value.Month.ToString().ToLower().Should().Be(endMonth);
            profExp.EndDate.Value.Year.ToString().ToLower().Should().Be(endYear);
            profExp.Country.Should().Be(country);
            profExp.City.Should().Be(city);
            profExp.JobDescription.Should().Be(jobDescription);
        }

        [Theory]
        [InlineData("Position", "Company name", "1", "2010", "", "", "Poland", "Cracow", "Description")]
        [InlineData("Position", "Company name", "1", "2010", null, null, "Poland", "Cracow", "Description")]
        public void Create_WhenCreateWithValidArgumentsAndNullOrEmptyEndDate_CreateObjectWithEndDateEqualNull(
            string position,
            string companyName, string startMonth, string startYear, string endMonth, string endYear,
            string country, string city, string jobDescription = "")
        {
            var profExp = ProfessionalExperience.Create(position, companyName, startMonth, startYear,
                endMonth, endYear, country, city, jobDescription);

            profExp.Position.Should().Be(position);
            profExp.CompanyName.Should().Be(companyName);
            profExp.StartDate.Month.ToString().ToLower().Should().Be(startMonth);
            profExp.StartDate.Year.ToString().ToLower().Should().Be(startYear);
            profExp.EndDate.Should().NotHaveValue();
            profExp.Country.Should().Be(country);
            profExp.City.Should().Be(city);
            profExp.JobDescription.Should().Be(jobDescription);
        }
    }
}