﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using justfinditApi.Core.ConstantValues;
using justfinditApi.Core.Domain;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class EmployeeTests
    {
        [Fact]
        public void AddCourse_CourseEqualsNull_CoursesCollectionDoesntChange()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            var courses = new List<Course>(employee.Courses);
            
            employee.AddCourse(null);

            employee.Courses.Should().BeEquivalentTo(courses);
        }
        
        [Fact]
        public void AddCourse_AfterCallWithValidObject_CoursesCollectionContainsAddedObject()
        {
            var course = new Mock<Course>();
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddCourse(course.Object);

            employee.Courses.Should().OnlyContain(x => x.CourseId == course.Object.CourseId);
        }
        
        [Fact]
        public void AddEducation_EducationEqualsNull_EducationCollectionDoesntChange()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            var educations = new List<Education>(employee.Education);
            
            employee.AddEducation(null);

            employee.Education.Should().BeEquivalentTo(educations);
        }
        
        [Fact]
        public void AddEducation_AfterCallWithValidObject_EducationCollectionContainsAddedObject()
        {
            var education = new Mock<Education>();
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddEducation(education.Object);

            Assert.Contains(education.Object, employee.Education);
            employee.Education.Should().OnlyContain(x => x.EducationId == education.Object.EducationId);
        }
        
        [Fact]
        public void AddLanguage_LanguageEqualsNull_LanguagesCollectionDoesntChange()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            var languages = new List<EmployeeLanguage>(employee.EmployeeLanguages);
            
            employee.AddLanguage(null, 1);

            employee.EmployeeLanguages.Should().BeEquivalentTo(languages);
        }

        [Fact]
        public void AddLanguage_AfterCallWithValidObject_LanguagesCollectionContainsAddedObject()
        {
            var language = Language.Create("English");
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());

            employee.AddLanguage(language, 1);

            employee.EmployeeLanguages.Should()
                .ContainSingle(x => x.Language.LanguageId == language.LanguageId);
        }
        
        [Fact]
        public void AddProfessionalExperience_ProfessionalExperienceEqualsNull_ProfessionalExperienceCollectionDoesntChange()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            var professionalExperience = new List<ProfessionalExperience>(employee.ProfessionalExperience);
            
            employee.AddProfessionalExperience(null);

            employee.ProfessionalExperience.Should().BeEquivalentTo(professionalExperience);
        }
        
        [Fact]
        public void AddProfessionalExperience_AfterCallWithValidObject_ProfessionalExperienceCollectionContainsAddedObject()
        {
            var professionalExperience = new Mock<ProfessionalExperience>();
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddProfessionalExperience(professionalExperience.Object);

            employee.ProfessionalExperience.Should().OnlyContain(x =>
                x.ProfessionalExperienceId == professionalExperience.Object.ProfessionalExperienceId);
        }
        
        [Fact]
        public void AddSkill_SkillEqualsNull_SkillsCollectionDoesntChange()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            var skills = new List<EmployeeSkill>(employee.EmployeeSkills);
            
            employee.AddSkill(null, 1);

            employee.EmployeeSkills.Should().BeEquivalentTo(skills);
        }
        
        [Fact]
        public void AddSkill_AfterCallWithValidObject_SkillsCollectionContainsAddedObject()
        {
            var technology = Technology.Create(".Net");
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddSkill(technology, 1);
  
            employee.EmployeeSkills.Should()
                .OnlyContain(x => x.Technology.TechnologyId == technology.TechnologyId);
        }

        [Fact]
        public void CreateEmployee_UserIsNull_ThrowException()
        {
            Action create = () =>
            {
                var employee = new Employee("name","surname","MAN","01-01-2000", "description", "555666777",
                    "example.com", "Poland", "Cracow", null);
            };
 
            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("", "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData(null, "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        public void CreateEmployee_NameIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Name", "", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData("Name", null, "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        public void CreateEmployee_SurnameIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Name", "Surname", "", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData("Name", "Surname", null, "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData("Name", "Surname", "xyz", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        public void CreateEmployee_SexIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Name", "Surname", "MAN", "", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData("Name", "Surname", "MAN", null, "description", "555666777",
            "example.com", "Poland", "Cracow")]
        [InlineData("Name", "Surname", "MAN", "invalid data", "description", "555666777",
            "example.com", "Poland", "Cracow")]
        public void CreateEmployee_BirthDateIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "", "Cracow")]
        [InlineData("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", null, "Cracow")]
        public void CreateEmployee_CountryIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }
        
        [Theory]
        [InlineData("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", "")]
        [InlineData("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
            "example.com", "Poland", null)]
        public void CreateEmployee_CityIsInvalid_ThrowException(string name, string surname, string sex,
            string birthDate, string about, string phone, string websiteUrl, string country,
            string city)
        {
            Employee employee;
            Action create = () => employee 
                = new Employee(name,surname,sex,birthDate,about,phone,websiteUrl,country,city, new User());

            create.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void DeleteCertificate_CertificateToDeleteNotExist_ThrowException()
        {
            var certificate1 = Course.Create("course1", "organizer1", "1", "2016");
            var certificate2 = Course.Create("course2", "organizer2", "2", "2017");
            var notExistedCertificate = Course.Create("course3", "organizer3", "3", "2018");
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddCourse(certificate1);
            employee.AddCourse(certificate2);
            
            Action delete = () => employee.DeleteCertificate(notExistedCertificate.CourseId);

            delete.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void DeleteCertificate_CertificateToDeleteExist_RemoveCertificate()
        {
            var certificate1 = Course.Create("course1", "organizer1", "1", "2016");
            var certificate2 = Course.Create("course2", "organizer2", "2", "2017");
            var certificateToDelete = Course.Create("course3", "organizer3", "3", "2018");
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddCourse(certificate1);
            employee.AddCourse(certificate2);
            employee.AddCourse(certificateToDelete);
            
            employee.DeleteCertificate(certificateToDelete.CourseId);

            var expectedLanguages = new List<Course>{certificate1, certificate2};
            employee.Courses.Should().BeEquivalentTo(expectedLanguages);
        }
        
        [Fact]
        public void DeleteEducation_EducationToDeleteNotExist_ThrowException()
        {
            var education1 = Education.Create("schoolName1","2015","2018","IT","Master");
            var education2 = Education.Create("schoolName2","2015","2018","IT","Master");
            var notExistedEducation = Education.Create("schoolName3","2015","2018","IT","Master");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddEducation(education1);
            employee.AddEducation(education2);
            
            Action delete = () => employee.DeleteEducation(notExistedEducation.EducationId);

            delete.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void DeleteEducation_EducationToDeleteExist_RemoveEducation()
        {
            var education1 = Education.Create("schoolName1","2015","2018","IT","Master");
            var education2 = Education.Create("schoolName2","2015","2018","IT","Master");
            var educationToDelete = Education.Create("schoolName3","2015","2018","IT","Master");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddEducation(education1);
            employee.AddEducation(education2);
            employee.AddEducation(educationToDelete);
            
            employee.DeleteEducation(educationToDelete.EducationId);

            var expectedEducationEntries = new List<Education>{education1, education2};
            employee.Education.Should().BeEquivalentTo(expectedEducationEntries);
        }
        
        [Fact]
        public void DeleteLanguage_LanguageToDeleteNotExist_ThrowException()
        {
            var language1 = Language.Create("English");
            var language2 = Language.Create("German");
            var notExistedLanguage = Language.Create("Spanish");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddLanguage(language1, 1);
            employee.AddLanguage(language2, 1);
            
            Action delete = () => employee.DeleteLanguage(notExistedLanguage.LanguageId);

            delete.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void DeleteLanguage_LanguageToDeleteExist_RemoveLanguage()
        {
            var language1 = Language.Create("English");
            var language2 = Language.Create("German");
            var languageToDelete = Language.Create("Spanish");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddLanguage(language1, 1);
            employee.AddLanguage(language2, 1);
            employee.AddLanguage(languageToDelete, 1);
            
            employee.DeleteLanguage(languageToDelete.LanguageId);

            employee.EmployeeLanguages.Should().NotContain(x => x.Language.LanguageId == languageToDelete.LanguageId);
        }
        
        [Fact]
        public void DeleteProfessionalExperience_ProfessionalExperienceToDeleteNotExist_ThrowException()
        {
            var professionalExperience1 = ProfessionalExperience.Create("Position1", "companyName1", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            var professionalExperience2 = ProfessionalExperience.Create("Position2", "companyName2", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            var notExistedProfessionalExperience = ProfessionalExperience.Create("Position3", "companyName3", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddProfessionalExperience(professionalExperience1);
            employee.AddProfessionalExperience(professionalExperience2);
            
            Action delete = () => employee.DeleteProfessionalExperience(notExistedProfessionalExperience.ProfessionalExperienceId);

            delete.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void DeleteProfessionalExperience_ProfessionalExperienceToDeleteExist_RemoveProfessionalExperience()
        {
            var professionalExperience1 = ProfessionalExperience.Create("Position1", "companyName1", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            var professionalExperience2 = ProfessionalExperience.Create("Position2", "companyName2", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            var professionalExperienceToDelete = ProfessionalExperience.Create("Position3", "companyName3", "1", "2010", "1",
                "2011", "Poland", "Cracow", "");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddProfessionalExperience(professionalExperience1);
            employee.AddProfessionalExperience(professionalExperience2);
            employee.AddProfessionalExperience(professionalExperienceToDelete);
            
            employee.DeleteProfessionalExperience(professionalExperienceToDelete.ProfessionalExperienceId);

            var expectedProfessionalExperiences = new List<ProfessionalExperience>
            {
                professionalExperience1,
                professionalExperience2
            };
            
            employee.ProfessionalExperience.Should().BeEquivalentTo(expectedProfessionalExperiences);
        }
        
        [Fact]
        public void DeleteSkill_SkillToDeleteNotExist_ThrowException()
        {
            var skill1 = Technology.Create("Angular");
            var skill2 = Technology.Create("C#");
            var notExistedSkill = Technology.Create("Java");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddSkill(skill1, 1);
            employee.AddSkill(skill2, 1);
            
            Action delete = () => employee.DeleteProfessionalExperience(notExistedSkill.TechnologyId);

            delete.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void DeleteSkill_SkillToDeleteExist_RemoveSkill()
        {
            var skill1 = Technology.Create("Angular");
            var skill2 = Technology.Create("C#");
            var skillToDelete = Technology.Create("Java");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.AddSkill(skill1, 1);
            employee.AddSkill(skill2, 1);
            employee.AddSkill(skillToDelete, 1);
            
            employee.DeleteSkill(skillToDelete.TechnologyId);
 
            employee.EmployeeSkills.Should()
                .NotContain(x => x.Technology.TechnologyId == skillToDelete.TechnologyId);
        }
        
        [Fact]
        public void EditCertificate_EditedCertificateParameterEqualsNull_CertificateIsNotEdited()
        {
            var certificate = Course.Create("name", "organizerName", "1", "2018");
            Course editedCertificate = null;
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddCourse(certificate);
            
            
            Action edit = () => employee.EditCertificate(certificate.CourseId, editedCertificate);

            edit.Should().Throw<ArgumentException>();
            certificate.CourseName.Should().Be("name");
            certificate.OrganizerName.Should().Be("organizerName");
            certificate.DateOfObtain.Month.ToString().ToLower().Should().Be("1");
            certificate.DateOfObtain.Year.ToString().ToLower().Should().Be("2018");
        }
        
        [Fact]
        public void EditCertificate_CertificateToEditDoesntExist_ThrowException()
        {
            var certificate = Course.Create("name1", "organizerName1", "1", "2018");
            var notExistedCertificate = Course.Create("name2", "organizerName2", "1", "2018");
            var editedCertificate = Course.Create("name3", "organizerName3", "1", "2018");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddCourse(certificate);
                    
            Action edit = () => employee.EditCertificate(notExistedCertificate.CourseId, editedCertificate);

            edit.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void EditCertificate_WhenCalledWithValidParameters_EditCertificate()
        {
            var certificate = Course.Create("name1", "organizerName1", "1", "2017");
            var editedCertificate = Course.Create("name2", "organizerName2", "2", "2018");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddCourse(certificate);
                    
            employee.EditCertificate(certificate.CourseId, editedCertificate);

            certificate.CourseName.Should().Be("name2");
            certificate.OrganizerName.Should().Be("organizerName2");
            certificate.DateOfObtain.Month.ToString().ToLower().Should().Be("2");
            certificate.DateOfObtain.Year.ToString().ToLower().Should().Be("2018");
        }
        
        [Fact]
        public void EditEducation_EditedEducationParameterEqualsNull_EducationIsNotEdited()
        {
            var education = Education.Create("schoolName1","2015","2018","IT","Master","");
            Education editedEducation = null;
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddEducation(education);
            
            
            Action edit = () => employee.EditEducation(education.EducationId, editedEducation);

            edit.Should().Throw<ArgumentException>();
            education.SchoolName.Should().Be("schoolName1");
            education.StartYear.Should().Be("2015");
            education.EndYear.Should().Be("2018");
            education.BranchOfStudy.Should().Be("IT");
            education.Degree.Should().Be("Master");
            education.Speciality.Should().Be("");
        }
        
        [Fact]
        public void EditEducation_EducationToEditDoesntExist_ThrowException()
        {
            var education = Education.Create("schoolName1","2015","2018","IT","Master","");
            var notExistedEducationEntry = Education.Create("schoolName2","2015","2018","IT","Master","");
            var editedEducation = Education.Create("schoolName3","2015","2018","IT","Master","");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddEducation(education);
            
            
            Action edit = () => employee.EditEducation(notExistedEducationEntry.EducationId, editedEducation);

            edit.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void EditEducation_WhenCalledWithValidParameters_EditEducation()
        {
            var education = Education.Create("schoolName1","2014","2017","Accounting","Master", "");
            var editedEducation = Education.Create("schoolName2","2015","2018","IT","Master", "Web Applications");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddEducation(education);
            
            employee.EditEducation(education.EducationId, editedEducation);

            education.SchoolName.Should().Be("schoolName2");
            education.StartYear.Should().Be("2015");
            education.EndYear.Should().Be("2018");
            education.BranchOfStudy.Should().Be("IT");
            education.Degree.Should().Be("Master");
            education.Speciality.Should().Be("Web Applications");
        }
        
        [Fact]
        public void EditLanguage_LanguageToEditDoesntExist_ThrowException()
        {
            var language = Language.Create("English");
            var notExistedLanguage = Language.Create("Spanish");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddLanguage(language, 1);
            
            
            Action edit = () => employee.EditLanguage(notExistedLanguage.LanguageId, 2);

            edit.Should().Throw<ArgumentException>();
            employee.EmployeeLanguages
                .Single(x => x.Language.LanguageId == language.LanguageId)
                .Level.Should().Be(1);
        }
        
        [Fact]
        public void EditLanguage_LanguageToEditExistAndLevelIsValid_EditLanguage()
        {
            var language = Language.Create("English");
            var newLevel = 4;
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddLanguage(language, 1);
            
            
            employee.EditLanguage(language.LanguageId, newLevel);

            employee.EmployeeLanguages
                .Single(x => x.Language.LanguageId == language.LanguageId)
                .Level.Should().Be(newLevel);
        }
        
        [Fact]
        public void EditPersonalData_AfterCallWithValidParameters_EditPersonalData()
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            employee.EditPersonalData("Name1", "Surname1", "about1", "WOMAN", "02-02-2001", "UK",
                "London", "777666555","example1.com");

            employee.Name.Should().Be("Name1");
            employee.Surname.Should().Be("Surname1");
            employee.About.Should().Be("about1");
            employee.Sex.Should().Be("WOMAN");
            employee.BirthDate.Should().Be(DateTime.Parse("02-02-2001"));
            employee.Country.Should().Be("UK");
            employee.City.Should().Be("London");
            employee.Phone.Should().Be("777666555");
            employee.WebsiteUrl.Should().Be("example1.com");
        }
        
        [Theory]
        [InlineData("", "Surname1", "about1", "kobieta", "02-02-2001", "UK",
            "London", "777666555","example1.com")]
        [InlineData("Name1", "", "about1", "kobieta", "02-02-2001", "UK",
            "London", "777666555","example1.com")]
        [InlineData("Name1", "Surname1", "about1", "xxx", "02-02-2001", "UK",
            "London", "777666555","example1.com")]
        [InlineData("Name1", "Surname1", "about1", "kobieta", "xyz", "UK",
            "London", "777666555","example1.com")]
        [InlineData("Name1", "Surname1", "about1", "kobieta", "02-02-2001", null,
            "London", "777666555","example1.com")]
        public void EditPersonalData_AfterCallWithInvalidParameters_PersonalDataAreNotEdited(string name,
            string surname, string about, string sex, string birthDate, 
            string country, string city, string phone, string websiteUrl)
        {
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            
            Action edit = () => employee.EditPersonalData(name, surname, about, sex, birthDate, country,
                city, phone, websiteUrl);

            edit.Should().Throw<ArgumentException>();
            employee.Name.Should().Be("Name");
            employee.Surname.Should().Be("Surname");
            employee.About.Should().Be("description");
            employee.Sex.Should().Be("MAN");
            employee.BirthDate.Should().Be(DateTime.Parse("01-01-2000"));
            employee.Country.Should().Be("Poland");
            employee.City.Should().Be("Cracow");
            employee.Phone.Should().Be("555666777");
            employee.WebsiteUrl.Should().Be("example.com");
        }
        
        [Fact]
        public void EditProfessionalExperience_EditedProfessionalExperienceParameterEqualsNull_ProfessionalExperienceIsNotEdited()
        {
            var professionalExperience = ProfessionalExperience
                .Create("Position1", "companyName1", "1", "2010", "1", "2011", "Poland", "Cracow", "");
            ProfessionalExperience editedProfessionalExperience = null;
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddProfessionalExperience(professionalExperience);
            
            
            Action edit = () => employee.EditProfessionalExperience
                (professionalExperience.ProfessionalExperienceId, editedProfessionalExperience);

            edit.Should().Throw<ArgumentException>();
            professionalExperience.Position.Should().Be("Position1");
            professionalExperience.CompanyName.Should().Be("companyName1");
            professionalExperience.StartDate.Month.ToString().ToLower().Should().Be("1");
            professionalExperience.StartDate.Year.ToString().ToLower().Should().Be("2010");
            professionalExperience.EndDate.Should().HaveValue();
            professionalExperience.EndDate.Value.Month.ToString().ToLower().Should().Be("1");
            professionalExperience.EndDate.Value.Year.ToString().ToLower().Should().Be("2011");
            professionalExperience.Country.Should().Be("Poland");
            professionalExperience.City.Should().Be("Cracow");
            professionalExperience.JobDescription.Should().Be("");
        }
        
        [Fact]
        public void EditProfessionalExperience_ProfessionalExperienceToEditDoesntExist_ThrowException()
        {
            var professionalExperience = ProfessionalExperience
                .Create("Position1", "companyName1", "1", "2010", "1", "2011", "Poland", "Cracow", "");
            var notExistedProfessionalExperience = ProfessionalExperience
                .Create("Position2", "companyName2", "1", "2010", "1", "2011", "Poland", "Cracow", "");
            var editedProfessionalExperience = ProfessionalExperience
                .Create("Position3", "companyName3", "1", "2010", "1", "2011", "Poland", "Cracow", "");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddProfessionalExperience(professionalExperience);
            
            
            Action edit = () => employee.EditProfessionalExperience
                (notExistedProfessionalExperience.ProfessionalExperienceId, editedProfessionalExperience);
            
            edit.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void EditProfessionalExperience_WhenCalledWithValidParameters_EditProfessionalExperience()
        {
            var professionalExperience = ProfessionalExperience
                .Create("Position1", "companyName1", "1", "2010", "1", "2011", "Poland", "Cracow", "");
            var editedProfessionalExperience = ProfessionalExperience
                .Create("Position2", "companyName2", "2", "2011", "2", "2012", "UK", "London", "AboutExp");

            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddProfessionalExperience(professionalExperience);
            
            
            employee.EditProfessionalExperience
                (professionalExperience.ProfessionalExperienceId, editedProfessionalExperience);
            
            professionalExperience.Position.Should().Be("Position2");
            professionalExperience.CompanyName.Should().Be("companyName2");
            professionalExperience.StartDate.Month.ToString().ToLower().Should().Be("2");
            professionalExperience.StartDate.Year.ToString().ToLower().Should().Be("2011");
            professionalExperience.EndDate.Should().HaveValue();
            professionalExperience.EndDate.Value.Month.ToString().ToLower().Should().Be("2");
            professionalExperience.EndDate.Value.Year.ToString().ToLower().Should().Be("2012");
            professionalExperience.Country.Should().Be("UK");
            professionalExperience.City.Should().Be("London");
            professionalExperience.JobDescription.Should().Be("AboutExp");
        }
        
        [Fact]
        public void EditSkill_SkillToEditDoesntExist_ThrowException()
        {
            var skill = Technology.Create("C#");
            var notExistedSkill = Technology.Create("Java");
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddSkill(skill, 1);
            
            
            Action edit = () => employee.EditSkill(notExistedSkill.TechnologyId, 2);

            edit.Should().Throw<ArgumentException>();
            employee.EmployeeSkills
                .Single(x => x.Technology.TechnologyId == skill.TechnologyId)
                .Level.Should().Be(1);
        }
        
        [Fact]
        public void EditSkill_SkillToEditExistAndLevelIsValid_EditSkill()
        {
            var skill = Technology.Create("C#");
            var newLevel = 4;
            
            var employee = new Employee("Name", "Surname", "MAN", "01-01-2000", "description", "555666777",
                "example.com", "Poland", "Cracow", new User());
            employee.AddSkill(skill, 1);
            
            
            employee.EditSkill(skill.TechnologyId, newLevel);

            employee.EmployeeSkills
                .Single(x => x.Technology.TechnologyId == skill.TechnologyId)
                .Level.Should().Be(newLevel);
        }
    }
}