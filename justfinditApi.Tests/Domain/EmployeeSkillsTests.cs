using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class EmployeeSkillsTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(6)]
        public void Create_LevelIsInvalid_ThrowException(int level)
        {
            var technology = Technology.Create("C#");

            Action creation = () => EmployeeSkill.Create(technology, level);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_TechnologyIsNull_ThrowException()
        {
            Action creation = () => EmployeeSkill.Create(null, 1);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_ArgumentsAreValid_CreateObject()
        {
            var technology = Technology.Create("C#");
            var employeeSkills = EmployeeSkill.Create(technology, 1);

            employeeSkills.Technology.Should().BeEquivalentTo(technology);
            employeeSkills.Level.Should().Be(1);
        }
    }
}