using System;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class EducationTests
    {
        [Theory]
        [InlineData("SchoolName", "2015", "2018", "branchOfStudy", "degree", "speciality")]
        public void Create_WhenCalledWithValidArguments_NotThrowingException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });

            Assert.Null(exception);
        }
        
        [Fact]
        public void Create_AfterCreation_ObjectStateIsValid()
        {
            string schoolName = "SchoolName";
            string startYear = "2016";
            string endYear = "2018";
            string branchOfStudy = "branchOfStudy";
            string degree = "degree";
            string speciality = "speciality";
            
            var education = Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);

            Assert.Equal(schoolName, education.SchoolName);
            Assert.Equal(startYear, education.StartYear);
            Assert.Equal(endYear, education.EndYear);
            Assert.Equal(branchOfStudy, education.BranchOfStudy);
            Assert.Equal(degree, education.Degree);
            Assert.Equal(speciality, education.Speciality);
        }
        
        [Theory]
        [InlineData(null, "2015", "2018", "branchOfStudy", "degree", "speciality")]
        [InlineData("", "2015", "2018", "branchOfStudy", "degree", "speciality")]
        public void Create_WhenCalledWithNullOrEmptySchoolName_ThrowingArgumentException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("SchoolName", "2015", "2018", null, "degree", "speciality")]
        [InlineData("SchoolName", "2015", "2018", "", "degree", "speciality")]
        public void Create_WhenCalledWithNullOrEmptyBranchOfStudy_ThrowingArgumentException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("SchoolName", "2015", "2018", "branchOfStudy", null, "speciality")]
        [InlineData("SchoolName", "2015", "2018", "branchOfStudy", "", "speciality")]
        public void Create_WhenCalledWithNullOrEmptyDegree_ThrowingArgumentException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });

            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("SchoolName", "2015", "2018", "branchOfStudy", "degree", "")]
        public void Create_WhenCalledWithNullSpecialityArgument_SpecialityPropertyEqualsEmptyString(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {
            var education = Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);

            Assert.Equal("", education.Speciality);
        }
        
        [Theory]
        [InlineData("SchoolName", null, "2018", "branchOfStudy", "degree", "speciality")]
        [InlineData("SchoolName", "", "2018", "branchOfStudy", "degree", "speciality")]
        [InlineData("SchoolName", "xyz", "2018", "branchOfStudy", "degree", "speciality")]
        public void Create_WhenCalledWithNullOrEmptyOrInvalidStartYear_ThrowingArgumentException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {          
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });
            
            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("SchoolName", "2016", "xyz", "branchOfStudy", "degree", "speciality")]
        public void Create_WhenCalledWithInvalidEndYear_ThrowingArgumentException(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {          
            var exception = Record.Exception(() =>
            {
                Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);
            });
            
            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }
        
        [Theory]
        [InlineData("SchoolName", "2016", "", "branchOfStudy", "degree", "speciality")]
        public void Create_WhenCalledWithNullOrEmptyEndYear_EndYearPropertyEqualsNull(string schoolName, string startYear,
            string endYear, string branchOfStudy, string degree, string speciality = "")
        {          
            var education = Education.Create(schoolName, startYear, endYear, branchOfStudy, degree, speciality);

            Assert.Null(education.EndYear);
        }
    }
}