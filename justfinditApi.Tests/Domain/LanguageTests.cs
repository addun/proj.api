using System;
using FluentAssertions;
using justfinditApi.Core.Domain;
using Xunit;

namespace justfinditApi.Tests.Domain
{
    public class LanguageTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Create_WhenCalledWithInvalidLanguageName_ThrowException(string languageName)
        {
            Action creation = () => Language.Create(languageName);

            creation.Should().Throw<ArgumentException>();
        }
        
        [Fact]
        public void Create_WhenCalledWithValidArguments_CreateObject()
        {
            var language = Language.Create("English");

            language.LanguageName.Should().Be("English");
        }
    }
}