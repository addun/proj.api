using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Infrastucture.DomainServices;
using justfinditApi.Infrastucture.Exceptions;
using justfinditApi.Infrastucture.Services;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;

namespace justfinditApi.Tests.DomainServices
{
    public class UserServiceTests: IDisposable
    {
        private readonly Mock<UserManager<User>> _userManagerMock;
        private readonly Mock<RoleManager<Role>> _roleManagerMock;
        private readonly Mock<IJwtProvider> _jwtProviderMock;
        
        public UserServiceTests()
        {
            var userStoreMock = new Mock<IUserStore<User>>();
            var roleStoreMock = new Mock<IRoleStore<Role>>();
            _userManagerMock = new Mock<UserManager<User>>(userStoreMock.Object,null,null,null,null,null,null,null,null);
            _roleManagerMock = new Mock<RoleManager<Role>>(roleStoreMock.Object,null,null,null,null);
            _jwtProviderMock = new Mock<IJwtProvider>(); 
        }
        
        [Fact]
        public void CreateUser_WhenCreationIsFail_ThrowServerException()
        {
            _userManagerMock.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    IdentityResult.Failed(
                        new IdentityError
                        {
                            Code = "1", Description = "Description1"
                        }, 
                        new IdentityError
                        {
                            Code = "2", Description = "Description2"
                        }
                    ))
                );
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
            
            Func<Task> creation = async () => await userService.CreateUser("test@test.com", "password");

            creation.Should().Throw<ServerException>();
        }       
        
        [Fact]
        public void CreateUser_WhenCreationIsSucceeded_ReturnCreatedUser()
        {    
            _userManagerMock.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns<User, string>((user, password) => Task.FromResult(IdentityResult.Success));
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
            
            var createdUser = userService.CreateUser("test@test.com", "password").Result;
 
            createdUser.Email.Should().Be("test@test.com");                 
        }

        [Fact]
        public void ConfirmEmail_WhenTokenIsNotValid_ThrowException()
        {
            _userManagerMock.Setup(x => x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));
                
            var user = new User{Email = "test@test.com" };
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
            
            
            Func<Task> conformation = async () => await userService.ConfirmEmail(user, "token");

            conformation.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void ConfirmEmail_WhenTokenIsNotEqualUserVerificationCode_ThrowException()
        {
            var userVerificationCode = "Qwerty";
            _userManagerMock.Setup(x => x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
                
            var user = new User{Email = "test@test.com", VerificationCode = userVerificationCode};
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
            
            
            Func<Task> conformation = async () => await userService.ConfirmEmail(user, "notQwerty");

            conformation.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void ConfirmEmail_WhenTokenIsValidAndEqualUserVerificationCode_ConfirmEmail()
        {
            var userVerificationCode = "Qwerty";
            _userManagerMock.Setup(x => x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
                
            var user = new User
            {
                Email = "test@test.com",
                VerificationCode = userVerificationCode,
                EmailConfirmed = false
            };
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
            
            
            Task.FromResult(userService.ConfirmEmail(user, "Qwerty"));

            user.EmailConfirmed.Should().BeTrue();
        }

        [Fact]
        public void GeneratePasswordResetTokenAsync_WhenUserEmailIsNotConfirmed_ThrowException()
        {
            var passwordResetToken = "Qwerty";
            var user = new User
            {
                Email = "test@test.com",
                EmailConfirmed = false
            };
            _userManagerMock.Setup(x=>x.GeneratePasswordResetTokenAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(passwordResetToken));       
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);
                      
            Func<Task> conformation = 
                async () => await userService.GeneratePasswordResetTokenAsync(user);

            conformation.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void GeneratePasswordResetTokenAsync_WhenUserEmailIsConfirmed_AssignTokenToUserLastGeneratedSecurityTokenProperty()
        {
            var passwordResetToken = "Qwerty";
            var user = new User{ Email = "test@test.com", EmailConfirmed = true };
            _userManagerMock.Setup(x=>x.GeneratePasswordResetTokenAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(passwordResetToken));       
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            var token = userService.GeneratePasswordResetTokenAsync(user).Result;

            user.LastGeneratedSecurityToken.Should().Be(token);
        }

        [Fact]
        public void GenerateEmailConfirmationTokenAsync_WhenCalled_AssignTokenToUserVerificationCodeProperty()
        {
            var userVerificationCode = "Qwerty";
            var user = new User{ Email = "test@test.com" };
            _userManagerMock.Setup(x=>x.GenerateEmailConfirmationTokenAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(userVerificationCode));       
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            var token = userService.GenerateEmailConfirmationTokenAsync(user).Result;

            user.VerificationCode.Should().Be(token);
        }
       
        [Fact]
        public void TryGenerateJwtTokenAsync_WhenPasswordIsValid_CreateJwtWithValidClaims()
        {
            IList<string> userRoles = new List<string> {"admin"};
            var userId = Guid.NewGuid();           
            var user = new User{ Id = userId, Email = "test@test.com" };
            
            _userManagerMock.Setup(x => x.CheckPasswordAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            _userManagerMock.Setup(x => x.GetRolesAsync(It.IsAny<User>()))
                .Returns(Task.FromResult(userRoles));

            IEnumerable<Claim> receivedPrincipalsParameter = null;
            _jwtProviderMock.Setup(x => x.CreateJwt(It.IsAny<IEnumerable<Claim>>()))
                .Callback<IEnumerable<Claim>>(principal => { receivedPrincipalsParameter = principal; })
                .Returns("token");
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Task.FromResult(userService.TryGenerateJwtTokenAsync(user, "valid_password"));


            var expectedReceivedPrincipal = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, "admin") 
            };
            
            _jwtProviderMock.Verify(x=>x.CreateJwt(It.IsAny<IEnumerable<Claim>>()), Times.Once);
            receivedPrincipalsParameter.Should().BeEquivalentTo(expectedReceivedPrincipal);
        }

        [Fact]
        public void ResetPassword_WhenTokenIsInvalid_ThrowException()
        {
            var invalidToken = "invalid_token";
            var user = new User{ Email = "test@test.com" };
            _userManagerMock.Setup(x =>
                    x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(Task.FromResult(false));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.ResetPassword(user, "newPassword", invalidToken);

            resettingPassword.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void ResetPassword_WhenTokenIsNotEqualLastGeneratedSecurityTokenUserProperty_ThrowException()
        {
            var validToken = "token";
            var user = new User{ Email = "test@test.com", LastGeneratedSecurityToken = "another_token"};
            _userManagerMock.Setup(x =>
                    x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            _userManagerMock.Setup(x =>
                    x.ResetPasswordAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.ResetPassword(user, "newPassword", validToken);

            resettingPassword.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void ResetPassword_WhenErrorsOccuredDuringResetingPassword_ThrowException()
        {
            var validToken = "token";
            var user = new User{ Email = "test@test.com", LastGeneratedSecurityToken = "token"};
            _userManagerMock.Setup(x =>
                    x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            _userManagerMock.Setup(x =>
                    x.ResetPasswordAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult
                    .Failed(new IdentityError{Code = "code", Description = "description"})));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.ResetPassword(user, "newPassword", validToken);

            resettingPassword.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void ResetPassword_WhenRessetingPasswordSucceed_ResetLastGeneratedSecurityToken()
        {
            var validToken = "token";
            var user = new User{ Email = "test@test.com", LastGeneratedSecurityToken = "token"};
            _userManagerMock.Setup(x =>
                    x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            _userManagerMock.Setup(x =>
                    x.ResetPasswordAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.ResetPassword(user, "newPassword", validToken);

            resettingPassword.Should().NotThrow<ServerException>();
            user.LastGeneratedSecurityToken.Should().BeNull();
        }
        
        [Fact]
        public void ResetPassword_WhenRessetingPasswordSucceed_ReturnTrue()
        {
            var validToken = "token";
            var user = new User{ Email = "test@test.com", LastGeneratedSecurityToken = "token"};
            _userManagerMock.Setup(x =>
                    x.VerifyUserTokenAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            
            _userManagerMock.Setup(x =>
                    x.ResetPasswordAsync(It.IsAny<User>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            bool result = false;
            Func<Task> resettingPassword = 
                async () => result = await userService.ResetPassword(user, "newPassword", validToken);

            resettingPassword.Should().NotThrow<ServerException>();
            result.Should().BeTrue();
        }

        [Fact]
        public void AddUserToRoleAsync_WhenRoleDoesNotExist_CreateRoleThenAddToRole()
        {
            var newRoleName = "admin";
            var user = new User {Email = "test@test.com"};
            _roleManagerMock.Setup(x => x.RoleExistsAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(false));
            _roleManagerMock.Setup(x => x.CreateAsync(It.IsAny<Role>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.AddUserToRoleAsync(user, newRoleName);
            
            resettingPassword.Should().NotThrow();
            _roleManagerMock.Verify(x=>x.CreateAsync(It.IsAny<Role>()), Times.Once);
            _userManagerMock.Verify(x=>x.AddToRoleAsync(It.IsAny<User>(),It.IsAny<string>()), Times.Once);
        }
        
        [Fact]
        public void AddUserToRoleAsync_WhenRoleIsCreated_PassProperArgument()
        {
            var newRoleName = "admin";
            var user = new User {Email = "test@test.com"};
            _roleManagerMock.Setup(x => x.RoleExistsAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(false));
            Role passedRole = null;
            _roleManagerMock.Setup(x => x.CreateAsync(It.IsAny<Role>()))
                .Callback<Role>(role => { passedRole = role; })
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.AddUserToRoleAsync(user, newRoleName);
            
            resettingPassword.Should().NotThrow();
            passedRole.Name.Should().Be(newRoleName);
        }
        
        [Fact]
        public void AddUserToRoleAsync_WhenRoleExist_AddToRole()
        {
            var newRoleName = "admin";
            var user = new User {Email = "test@test.com"};
            _roleManagerMock.Setup(x => x.RoleExistsAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            _roleManagerMock.Setup(x => x.CreateAsync(It.IsAny<Role>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.AddUserToRoleAsync(user, newRoleName);
            
            resettingPassword.Should().NotThrow();
            _roleManagerMock.Verify(x=>x.CreateAsync(It.IsAny<Role>()), Times.Never);
            _userManagerMock.Verify(x=>x.AddToRoleAsync(It.IsAny<User>(),It.IsAny<string>()), Times.Once);
        }
        
        [Fact]
        public void AddUserToRoleAsync_WhenAddingToRoleFail_ThrowException()
        {
            var newRoleName = "admin";
            var user = new User {Email = "test@test.com"};
            _roleManagerMock.Setup(x => x.RoleExistsAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            _roleManagerMock.Setup(x => x.CreateAsync(It.IsAny<Role>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _userManagerMock.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError
                    {Code = "errorCode", Description = "errorDescription"})));
            
            var userService = new UserService(_userManagerMock.Object, _roleManagerMock.Object, _jwtProviderMock.Object);

            Func<Task> resettingPassword = 
                async () => await userService.AddUserToRoleAsync(user, newRoleName);

            resettingPassword.Should().Throw<ServerException>();
        }

        public void Dispose()
        {
        }
    }
}