using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;
using justfinditApi.Infrastucture.Services;
using justfinditApi.Infrastucture.Validators;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Services
{
    public class EmployerServiceTests
    {
        private readonly Mock<IUnitOfWork> _dbMock;
        private readonly Mock<IValidatorService> _validatorServiceMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IPrincipalProvider> _principalProviderMock;

        public EmployerServiceTests()
        {
            _dbMock = new Mock<IUnitOfWork>();
            _validatorServiceMock = new Mock<IValidatorService>();
            _mapperMock = new Mock<IMapper>();
            _principalProviderMock = new Mock<IPrincipalProvider>();
        }
        
        [Fact]
        public void GetEmployerByUserId_WhenGuidIsEmpty_ThrowServerException()
        {
           var emptyGuid = Guid.Empty;

            var employerService
                = new EmployerService(_dbMock.Object, _validatorServiceMock.Object, _mapperMock.Object,
                    _principalProviderMock.Object);

            Func<Task<EmployerDto>> forgotPassword = async () => 
                await employerService.GetEmployerByUserId(emptyGuid);

            forgotPassword.Should().Throw<ServerException>();
        }
        
        [Fact]
        public void GetEmployerByUserId_WhenGuidIsNotEmpty_CallRepositoryMethod()
        {
            var userId = Guid.NewGuid();

            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Employer>(null));
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Func<Task<EmployerDto>> forgotPassword = async () => 
                await employerService.GetEmployerByUserId(userId);

            forgotPassword.Should().NotThrow();
            _dbMock.Verify(x=>x.Employers.GetByUserIdAsync(It.IsAny<Guid>()),Times.Once);
        }
        
        [Fact]
        public void GetEmployerByUserId_WhenGuidIsNotEmpty_MappedDtoIsReturned()
        {
            var userId = Guid.NewGuid();

            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Employer>(null));
            
            var employerDtoResult = new EmployerDto();
            _mapperMock.Setup(x => x.Map<EmployerDto>(It.IsAny<Employer>()))
                .Returns(employerDtoResult);
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Task<EmployerDto> result = null;
            Func<Task<EmployerDto>> forgotPassword = async () =>
            {
                result = employerService.GetEmployerByUserId(userId);
                return await result;
            };

            forgotPassword.Should().NotThrow();
            _dbMock.Verify(x=>x.Employers.GetByUserIdAsync(It.IsAny<Guid>()),Times.Once);
            _mapperMock.Verify(x => x.Map<EmployerDto>(It.IsAny<Employer>()),Times.Once);
            result.Result.Should().BeOfType<EmployerDto>();
            result.Result.Should().Be(employerDtoResult);
        }
        
        [Fact]
        public void EditCompanyDataAsync_WhenDtoIsInvalid_ThrowServerException()
        {
            var companyDataDto = new EmployerDataDto();
            var editCompanyDataDto = new EditEmployerDataDto
            {
              UserId = Guid.NewGuid(),
              EmployerData = companyDataDto
            };
            var validationErrors = 
                new Dictionary<string, List<string>> {{"error", new List<string> {"error"}}};
            var invalidValidatorResult = FluentValidationServiceResult.Create(validationErrors);

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<EmployerDataDto>()))
                .Returns(invalidValidatorResult);
            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Employer>(null));
            
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Func<Task> editCompanyData = async () => 
                await employerService.EditEmployerDataAsync(editCompanyDataDto);

            editCompanyData.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<EmployerDataDto>()), Times.Once);
            _dbMock.Verify(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()), Times.Never);
        }
        
        [Fact]
        public void EditCompanyDataAsync_WhenDtoIsValid_RepositoryMethodToFetchEmployerIsCalled()
        {
            var editCompanyDataDto = new EditEmployerDataDto
            {
                UserId = Guid.NewGuid(),
                EmployerData = new EmployerDataDto()
            };
            var successValidatorResult = FluentValidationServiceResult.Create(new Dictionary<string, List<string>>( ));

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<EmployerDataDto>()))
                .Returns(successValidatorResult);
            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Employer>(null));
            
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Task.FromResult(employerService.EditEmployerDataAsync(editCompanyDataDto));

            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<EmployerDataDto>()), Times.Once);
            _dbMock.Verify(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()), Times.Once);
        }
        
        [Fact]
        public void EditCompanyDataAsync_WhenEmployerWithIdGivenInDtoDoesNotExist_ThrowServerException()
        {
            var editCompanyDataDto = new EditEmployerDataDto
            {
                UserId = Guid.NewGuid(),
                EmployerData = new EmployerDataDto()
            };
            var successValidatorResult = FluentValidationServiceResult.Create(new Dictionary<string, List<string>>( ));

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<EmployerDataDto>()))
                .Returns(successValidatorResult);
            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Employer>(null));
            
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Func<Task> editCompanyData = async () => 
                await employerService.EditEmployerDataAsync(editCompanyDataDto);

            editCompanyData.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<EmployerDataDto>()), Times.Once);
            _dbMock.Verify(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()), Times.Once);
        }
        
        [Fact]
        public void EditCompanyDataAsync_WhenEmployerWithIdGivenInDtoExist_EditCompanyData()
        {
            var userId = Guid.NewGuid();
            var editCompanyDataDto = new EditEmployerDataDto
            {
                UserId = userId,
                EmployerData = new EmployerDataDto
                {
                    CompanyName = "company name1",
                    CompanyDescription = "description1",
                    Nip = "1234567890",
                    CompanySize = "10",
                    FoundedYear = "2000",
                    WebsiteUrl = "example1.com"
                }
            };
            
            var successValidatorResult = FluentValidationServiceResult.Create(new Dictionary<string, List<string>>( ));

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<EmployerDataDto>()))
                .Returns(successValidatorResult);
            
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User{Id = userId};
            var employerToReturn = new Employer("company name", "description", "2001", "11", "0987654321", "example1.com",
                location, user);
            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(employerToReturn));
            
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Func<Task> editCompanyData = async () => 
                await employerService.EditEmployerDataAsync(editCompanyDataDto);

            editCompanyData.Should().NotThrow();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<EmployerDataDto>()), Times.Once);
            _dbMock.Verify(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()), Times.Once);
            
            employerToReturn.CompanyName.Should().Be(editCompanyDataDto.EmployerData.CompanyName);
            employerToReturn.CompanyDescription.Should().Be(editCompanyDataDto.EmployerData.CompanyDescription);
            employerToReturn.CompanySize.Should().Be(editCompanyDataDto.EmployerData.CompanySize);
            employerToReturn.FoundedYear.Should().Be(editCompanyDataDto.EmployerData.FoundedYear);
            employerToReturn.WebsiteUrl.Should().Be(editCompanyDataDto.EmployerData.WebsiteUrl);
        }
        
        [Fact]
        public void EditCompanyDataAsync_WhenCompanyDataIsEdited_SaveInRepository()
        {     
            var userId = Guid.NewGuid();
            var editCompanyDataDto = new EditEmployerDataDto
            {
                UserId = userId,
                EmployerData = new EmployerDataDto
                {
                    CompanyName = "company name1",
                    CompanyDescription = "description1",
                    Nip = "1234567890",
                    CompanySize = "10",
                    FoundedYear = "2000",
                    WebsiteUrl = "example1.com"
                }
            };
            var successValidatorResult = FluentValidationServiceResult.Create(new Dictionary<string, List<string>>( ));

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<EmployerDataDto>()))
                .Returns(successValidatorResult);
            
            var location = EmployerLocation.Create("country", "city", "street", "postalCode", "1", "4");
            var user = new User{Id = userId};
            var employerToReturn = new Employer("company name", "description", "2001", "11", "0987654321", "example1.com",
                location, user);
            _dbMock.Setup(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(employerToReturn));
            _dbMock.Setup(x => x.Employers.UpdateEmployer(It.IsAny<Employer>()))
                .Returns(Task.CompletedTask);
            
            var employerService 
                = new EmployerService(_dbMock.Object,_validatorServiceMock.Object,_mapperMock.Object, _principalProviderMock.Object);

            Func<Task> editCompanyData = async () => 
                await employerService.EditEmployerDataAsync(editCompanyDataDto);

            editCompanyData.Should().NotThrow();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<EmployerDataDto>()), Times.Once);
            _dbMock.Verify(x => x.Employers.GetByUserIdAsync(It.IsAny<Guid>()), Times.Once);
            _dbMock.Verify(x => x.Employers.UpdateEmployer(It.IsAny<Employer>()), Times.Once);
        }
    }
}