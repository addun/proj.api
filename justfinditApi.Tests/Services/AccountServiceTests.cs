using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using justfinditApi.Core.Domain;
using justfinditApi.Core.DomainServices;
using justfinditApi.Core.Repositories;
using justfinditApi.Infrastucture.Dtos;
using justfinditApi.Infrastucture.Exceptions;
using justfinditApi.Infrastucture.Services;
using justfinditApi.Infrastucture.Validators;
using Moq;
using Xunit;

namespace justfinditApi.Tests.Services
{
    public class AccountServiceTests: IDisposable
    {
        private Mock<IValidatorService> _validatorServiceMock;
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private Mock<IUserService> _userServiceMock;
        private Mock<IEmailService> _emailServiceMock;
        private Mock<IPrincipalProvider> _principalProviderMock;
        private Mock<IMapper> _mapperMock;
        
        public AccountServiceTests()
        {
            _emailServiceMock = new Mock<IEmailService>();
            _validatorServiceMock = new Mock<IValidatorService>();
            _principalProviderMock = new Mock<IPrincipalProvider>();
            _mapperMock = new Mock<IMapper>();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _userServiceMock = new Mock<IUserService>();
        }
        
        [Fact]
        public void ConfirmEmail_WhenDtoValidationIsFailed_ThrowServerException()
        {
            var confirmEmailDto = new ConfirmEmailDto
            {
                UserId = Guid.NewGuid(),
                Token = "token"
            };
            var validatorErrors = new Dictionary<string, List<string>>
            {
                {"error", new List<string> {"error1", "error2"}}
            };
            var validatorResult = FluentValidationServiceResult.Create(validatorErrors);
            
            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<ConfirmEmailDto>()))
                .Returns(validatorResult);
            _userServiceMock.Setup(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()));
            
            var accountService = new AccountService(_unitOfWorkMock.Object, _userServiceMock.Object,
                _emailServiceMock.Object, _principalProviderMock.Object, _validatorServiceMock.Object,
                _mapperMock.Object);

            Func<Task> emailConformation = async () => await accountService.ConfirmEmail(confirmEmailDto);
            
            emailConformation.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<ConfirmEmailDto>()), Times.Once);
            _userServiceMock.Verify(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()), Times.Never);
        }                                                        
        
        [Fact]
        public void ConfirmEmail_WheUserToConfirmEmailDoesNotExist_ThrowServerException()
        {
            var confirmEmailDto = new ConfirmEmailDto
            {
                UserId = Guid.NewGuid(),
                Token = "token"
            };

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<ConfirmEmailDto>()))
                .Returns(FluentValidationServiceResult.Create(new Dictionary<string, List<string>>()));
            _unitOfWorkMock.Setup(x => x.Users.GetUserByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult<User>(null));
            _userServiceMock.Setup(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()));
            
            var accountService = new AccountService(_unitOfWorkMock.Object, _userServiceMock.Object,
                _emailServiceMock.Object, _principalProviderMock.Object, _validatorServiceMock.Object,
                _mapperMock.Object);

            Func<Task> emailConformation = async () => await accountService.ConfirmEmail(confirmEmailDto);
                        
            emailConformation.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<ConfirmEmailDto>()), Times.Once);
            _unitOfWorkMock.Verify(x=>x.Users.GetUserByIdAsync(It.IsAny<Guid>()),Times.Once);
            _userServiceMock.Verify(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()), Times.Never);
        }
        
        [Fact]
        public void ConfirmEmail_WhenCalled_UserIsUpdated()
        {
            var confirmEmailDto = new ConfirmEmailDto
            {
                UserId = Guid.NewGuid(),
                Token = "token"
            };

            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<ConfirmEmailDto>()))
                .Returns(FluentValidationServiceResult.Create(new Dictionary<string, List<string>>()));
            _unitOfWorkMock.Setup(x => x.Users.GetUserByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new User()));
            _userServiceMock.Setup(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.CompletedTask);
            _unitOfWorkMock.Setup(x => x.Users.UpdateUser(It.IsAny<User>()));
            
            var accountService = new AccountService(_unitOfWorkMock.Object, _userServiceMock.Object,
                _emailServiceMock.Object, _principalProviderMock.Object, _validatorServiceMock.Object,
                _mapperMock.Object);

            Func<Task> emailConformation = async () => await accountService.ConfirmEmail(confirmEmailDto);
            
            emailConformation.Should().NotThrow();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<ConfirmEmailDto>()), Times.Once);
            _unitOfWorkMock.Verify(x=>x.Users.GetUserByIdAsync(It.IsAny<Guid>()),Times.Once);
            _userServiceMock.Verify(x => x.ConfirmEmail(It.IsAny<User>(), It.IsAny<string>()), Times.Once);
            _unitOfWorkMock.Verify(x=>x.Users.UpdateUser(It.IsAny<User>()), Times.Once);
        }

        [Fact]
        public void ForgotPassword_WhenDtoValidationIsFailed_ThrowServerException()
        {
            var forgotPasswordDto = new ForgotPasswordDto
            {
                Email = "test@test.com"
            };
            var validatorErrors = new Dictionary<string, List<string>>
            {
                {"error", new List<string> {"error1", "error2"}}
            };
            var validatorResult = FluentValidationServiceResult.Create(validatorErrors);
            
            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<ForgotPasswordDto>()))
                .Returns(validatorResult);
            _userServiceMock.Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<User>()))
                .Returns(Task.FromResult("token"));
            
            var accountService = new AccountService(_unitOfWorkMock.Object, _userServiceMock.Object,
                _emailServiceMock.Object, _principalProviderMock.Object, _validatorServiceMock.Object,
                _mapperMock.Object);

            Func<Task> forgotPassword = async () => await accountService.ForgotPassword(forgotPasswordDto);

            forgotPassword.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<ForgotPasswordDto>()), Times.Once);
            _userServiceMock.Verify(x=>x.GeneratePasswordResetTokenAsync(It.IsAny<User>()),Times.Never);
        }
        
        [Fact]
        public void ForgotPassword_WheUserWithGivenEmailDoesNotExist_ThrowServerException()
        {
            var forgotPasswordDto = new ForgotPasswordDto
            {
                Email = "notexist@test.com"
            };
            var validatorResult = FluentValidationServiceResult.Create(new Dictionary<string, List<string>>( ));
            
            _validatorServiceMock.Setup(x => x.Validate(It.IsAny<ForgotPasswordDto>()))
                .Returns(validatorResult);
            _unitOfWorkMock.Setup(x => x.Users.GetUserByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<User>(null));
            _userServiceMock.Setup(x => x.GeneratePasswordResetTokenAsync(It.IsAny<User>()))
                .Returns(Task.FromResult("token"));
            
            var accountService = new AccountService(_unitOfWorkMock.Object, _userServiceMock.Object,
                _emailServiceMock.Object, _principalProviderMock.Object, _validatorServiceMock.Object,
                _mapperMock.Object);

            Func<Task> forgotPassword = async () => await accountService.ForgotPassword(forgotPasswordDto);
            
            forgotPassword.Should().Throw<ServerException>();
            _validatorServiceMock.Verify(x => x.Validate(It.IsAny<ForgotPasswordDto>()), Times.Once);
            _unitOfWorkMock.Verify(x=>x.Users.GetUserByEmailAsync(It.IsAny<string>()),Times.Once);
            _userServiceMock.Verify(x=>x.GeneratePasswordResetTokenAsync(It.IsAny<User>()),Times.Never);
        }
        
        public void ForgotPassword_WhenUserEmailIsNotConfirmed_ThrowServerException()
        {
            
        }
        
        public void ForgotPassword_WhenCalled_GeneratePasswordResetToken()
        {
            
        }
        
        public void ForgotPassword_WhenTokenWasGenerated_UpdateUser()
        {
            
        }
        
        public void ForgotPassword_WhenUserWasUpdated_SendResetPasswordLink()
        {
            
        }

        public void Dispose()
        {
        }
    }
}